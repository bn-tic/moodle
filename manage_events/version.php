<?php
$plugin->version   = 2018072200;
$plugin->requires  = 2015111600;
$plugin->component = 'block_manage_events';
$plugin->maturity  = MATURITY_STABLE;
$plugin->release   = 'Moodle 3.x+';
