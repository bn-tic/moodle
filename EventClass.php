<?php

/*
 * class event (calendar)
 */
define("admins", ['2', '5193', '5192', '1002', '5196', '8722', '6358', '1815', '7065', '7069', '7068', '9403', '10423', '10925', '15759', '4236']);

class Event {

    public function __construct() {
        
    }

    /**
     * get last access
     * @global type $DB
     * @param type $user
     * @param type $course
     * @return type
     */
    public static function get_lastaccess($user, $course) {
        global $DB;
        //get last access on course for logged user
        $lastaccess = $DB->get_records_sql("SELECT *FROM mco_user_lastaccess WHERE mco_user_lastaccess.userid = $user AND mco_user_lastaccess.courseid = $course "
                . "ORDER BY mco_user_lastaccess.id desc");

        return $lastaccess;
    }

    /**
     * Get lessons from a course
     * @global type $DB
     * @param type $course
     * @return type
     */
    public function get_lessons($course) {
        global $DB;
        //only count events on blocks "%Tema%"
        $lessons = $DB->get_records_sql("SELECT * FROM mco_course_sections WHERE course = {$course} "
                . "AND mco_course_sections.name LIKE '%tema%' AND mco_course_sections.summary != '' ORDER BY section asc");

        return $lessons;
    }

    /**
     * get summary and name module from a course
     * @global type $DB
     * @param type $course
     * @param type $instance
     * @return type
     */
    public function get_section_by_resource($course, $instance) {
        global $DB;

        $module = $DB->get_record_sql("SELECT mco_course_modules.id FROM mco_course_modules WHERE mco_course_modules.course = {$course} AND mco_course_modules.INSTANCE = {$instance}");


        $data_section = $DB->get_record_sql("SELECT mco_course_sections.name, mco_course_sections.summary FROM mco_course_sections WHERE course = {$course} 
                    AND mco_course_sections.sequence LIKE '%{$module->id}%'");

        return $data_section;
    }

    /**
     * insert events on first course access
     * @global type $DB
     * @param type $user
     * @param type $course
     */
    public function first_access($user, $course) {
        global $DB;
        //get last access on course for logged user
        $lastaccess = self::get_lastaccess($user, $course);

        //asign events
        if (count($lastaccess) == 0) {
            //get lessons
            $lessons = self::get_lessons($course);

            $days = round(160 / count($lessons));
            $now = date("Y-m-d H:i:s");
            $data = new stdClass();

            //LESSONS
            foreach ($lessons as $lesson) {
                //event summary
                $summary = $lesson->summary;

                //various events
                if (strpos($lesson->sequence, ',') !== false) {
                    $sequence = explode(",", $lesson->sequence);

                    for ($i = 0; $i < count($sequence); $i++) {
                        $resource = self::search_resource($course, $sequence[$i]);
                        $summary .= "<p class='headline headline-v1'><i class='fa fa-check-circle'></i> {$resource}</p> ";
                    }
                } else if ($lesson->sequence != "") {

                    $resource = self::search_resource($course, $lesson->sequence);
                    $summary .= "<p class='headline headline-v1'><i class='fa fa-check-circle'></i> {$resource}</p> ";
                }


                //insert data
                $data->name = $lesson->name;
                $data->description = $summary;
                $data->format = 1;
                //user event
                $data->courseid = 0;
                $data->groupid = 0;
                $data->userid = $user;
                $data->repeatid = 0;
                $data->modulename = 0;
                $data->instance = 0;
                $data->type = 0;
                $data->eventtype = 'user';
                $data->timestart = strtotime($now);
                $data->timeduration = (($days * 24) * 60) * 60;
                $data->timesort = NULL;
                $data->visible = 1;
                $data->uuid = '';
                $data->sequence = 1;
                $data->timemodified = strtotime($now);
                $data->subcriptionid = NULL;
                $data->priority = NULL;
                $data->course = $course;

                //insert data
                self::insert_event($data);
                $to = date('Y-m-d H:i:s', strtotime($now . ' + ' . $days . ' days'));
                $now = $to;
            }
        }
    }

    /**
     * insert event
     * @param type $data
     * @return type
     */
    public function insert_event($data) {
        global $DB;
        $event = new stdClass();

        try {
            $transaction = $DB->start_delegated_transaction();
            $event->event = $DB->insert_record('event', $data, $returnid = TRUE);
            $event->course = $data->course;

            $DB->insert_record('event_course', $event);
            $transaction->allow_commit();
        } catch (Exception $e) {
            $transaction->rollback($e);
        }
    }

    /**
     * delete events from a course
     * @param type $course
     * @param type $user
     */
    public function delete_events($course, $user) {
        global $DB;

        //select all events for a user in a course (table events join mco_event_course)
        $events = $DB->get_records_sql("SELECT mco_event.id
                    FROM mco_event
                    INNER JOIN mco_event_course ON mco_event_course.`event` = mco_event.id
                    WHERE mco_event_course.course = {$course} AND mco_event.userid = {$user}");


        //save events ids
        foreach ($events as $event) {
            //delete from table mco_event_course
            $DB->delete_records('event_course', array('event' => $event->id));
            //delete from mco_event
            $DB->delete_records('event', array('id' => $event->id));
        }
    }

    /**
     * SEARCH RESOURCES ON DB
     * @global type $DB
     * @param type $course
     * @param type $sequence
     * @return type
     */
    public function search_resource($course, $sequence) {
        global $DB;
        $types = ['resource', 'scorm', 'video', 'page', 'book', 'quiz'];

        //search resource
        $instance = $DB->get_record_sql("SELECT mco_course_modules.INSTANCE as instance, mco_modules.NAME as name
            FROM mco_course_modules
            INNER JOIN mco_modules ON mco_course_modules.module = mco_modules.id
            WHERE mco_course_modules.course = {$course}
            AND mco_course_modules.id  = {$sequence}");

        //search resource
        if (in_array($instance->name, $types)) {
            $resource_name = $DB->get_record_sql("SELECT mco_{$instance->name}.name
            FROM mco_{$instance->name}
            WHERE mco_{$instance->name}.id = {$instance->instance}");
            return $resource_name->name;
        }
    }

    /**
     * Return start license 
     * @param type $user
     * @param type $course
     * @return type
     */
    public function get_start_license($user, $course) {
        global $DB;

        $license_duration = $DB->get_record_sql("SELECT ue.timestart "
                . "FROM mco_user_enrolments ue "
                . "JOIN mco_enrol e ON e.id = ue.enrolid "
                . "WHERE e.courseid = {$course} AND ue.userid = {$user} AND ue.timeend != 0");

        $to = date('Y-m-d H:i:s', $license_duration->timestart);

        return $to;
    }

    /**
     * Get end user license
     * @param type $user
     * @param type $course
     * @return type3
     */
    public function get_end_license($user, $course) {
        global $DB;

        //get end license by user
        /* $license_duration = $DB->get_record_sql("SELECT mco_companylicense_users.issuedate ,mco_companylicense.validlength 
          FROM mco_companylicense_users
          LEFT JOIN mco_companylicense ON mco_companylicense.id = mco_companylicense_users.licenseid
          WHERE mco_companylicense_users.userid = {$user} AND mco_companylicense_users.licensecourseid = {$course} ORDER BY mco_companylicense_users.id DESC");

          $to = date('Y-m-d H:i:s', strtotime("+" . $license_duration->validlength . " day", $license_duration->issuedate)); */

        $license_duration = $DB->get_record_sql("SELECT ue.timeend "
                . "FROM mco_user_enrolments ue "
                . "JOIN mco_enrol e ON e.id = ue.enrolid "
                . "WHERE e.courseid = {$course} AND ue.userid = {$user} AND ue.timeend != 0");

        $to = date('Y-m-d H:i:s', $license_duration->timeend);

        return $to;
    }

    public function get_countdown($user, $course) {

        if (in_array($user, admins)) {
            $message = "Acceso ilimitado";
        } else if (self::get_end_license($user, $course) > date('Y-m-d H:i:s')) {
            $message = '<script>createCountDown("time' . $course . '", "' . self::get_end_license($user, $course) . '")</script>';
        } else
            $message = 'Licencia caducada';

        return $message;
    }

    /**
     * Planning recalc
     * @global type $DB
     * @param type $user
     * @param type $course
     * @param type $elements (rest elements to complete)
     */
    public function recalc($user, $course, $elements, $now, $diff, $event_id, $is_user_recalc, $finishtime) {
        global $DB;
        if (!$is_user_recalc)
            $to = self::get_end_license($user, $course);
        else
            $to = $finishtime;

        if ($diff == 0) {
            if ($to > $now) {

                $diff = (abs(strtotime($to) - strtotime($now))) / $elements;
            } else
                redirect(new moodle_url('/course/view.php?id=' . $course, ['redirect' => 0]), "Tu licencia ha caducado");
        }

        if (!$is_user_recalc)
            $DB->execute("UPDATE mco_event SET mco_event.timestart = " . strtotime($now) . " WHERE mco_event.id = {$event_id}");
        else
            $DB->execute("UPDATE mco_event SET mco_event.timestart = " . strtotime($now) . ", mco_event.timeduration = '{$diff}' WHERE mco_event.id = {$event_id}");

        return $diff;
    }

    /**
     * info events (calendar)
     * @global type $DB
     * @param type $course
     * @param type $user
     * @return type
     */
    public function get_info_events($course, $user) {
        global $DB;

        $info = $DB->get_records_sql("SELECT mco_event.*
            FROM mco_event
            INNER JOIN mco_event_course ON mco_event_course.`event` = mco_event.id
            WHERE mco_event_course.course = {$course} AND mco_event.userid = {$user}");

        return $info;
    }
    
    /**
     * get iomadprogress id
     * @param type $parentcontextid
     */
    public function get_iomadbar_byparentcontext($parentcontextid) {
        global $DB;

        $block = $DB->get_record_sql("SELECT mco_block_instances.id "
                . "FROM mco_block_instances "
                . "WHERE mco_block_instances.parentcontextid = {$parentcontextid} AND blockname = 'iomad_progress'");

        return $block->id;
    }

    /**
     * Get all completion activities by course and user
     * @global type $DB
     * @param type $courseid
     * @param type $userid
     * @return type
     */
    public function get_all_completion_resources($courseid, $userid) {
        global $DB;

        $info = $DB->get_records_sql("SELECT m.name AS type, 
            CASE 
                WHEN m.name = 'assign'  THEN (SELECT name FROM mco_assign WHERE id = cm.instance) 
                WHEN m.name = 'assignment'  THEN (SELECT name FROM mco_assignment WHERE id = cm.instance)
                WHEN m.name = 'book'  THEN (SELECT name FROM mco_book WHERE id = cm.instance)
                WHEN m.name = 'chat'  THEN (SELECT name FROM mco_chat WHERE id = cm.instance)
                WHEN m.name = 'choice'  THEN (SELECT name FROM mco_choice WHERE id = cm.instance)
                WHEN m.name = 'data'  THEN (SELECT name FROM mco_data WHERE id = cm.instance)
                WHEN m.name = 'feedback'  THEN (SELECT name FROM mco_feedback WHERE id = cm.instance)
                WHEN m.name = 'folder'  THEN (SELECT name FROM mco_folder WHERE id = cm.instance)
                WHEN m.name = 'forum' THEN (SELECT name FROM mco_forum WHERE id = cm.instance)
                WHEN m.name = 'glossary' THEN (SELECT name FROM mco_glossary WHERE id = cm.instance)
                WHEN m.name = 'imscp' THEN (SELECT name FROM mco_imscp WHERE id = cm.instance)
                WHEN m.name = 'label'  THEN (SELECT name FROM mco_label WHERE id = cm.instance)
                WHEN m.name = 'lesson'  THEN (SELECT name FROM mco_lesson WHERE id = cm.instance)
                WHEN m.name = 'lti'  THEN (SELECT name FROM mco_lti  WHERE id = cm.instance)
                WHEN m.name = 'page'  THEN (SELECT name FROM mco_page WHERE id = cm.instance)
                WHEN m.name = 'quiz'  THEN (SELECT name FROM mco_quiz WHERE id = cm.instance)
                WHEN m.name = 'resource'  THEN (SELECT name FROM mco_resource WHERE id = cm.instance)
                WHEN m.name = 'scorm'  THEN (SELECT name FROM mco_scorm WHERE id = cm.instance)
                WHEN m.name = 'survey'  THEN (SELECT name FROM mco_survey WHERE id = cm.instance)
                WHEN m.name = 'url'  THEN (SELECT name FROM mco_url  WHERE id = cm.instance)
                WHEN m.name = 'wiki' THEN (SELECT name FROM mco_wiki  WHERE id = cm.instance)
                WHEN m.name = 'workshop' THEN (SELECT name FROM mco_workshop  WHERE id = cm.instance)
            ELSE 'Otra actividad'
            END AS activity,
            CASE
               WHEN cmc.completionstate = 0 THEN 'En progreso'
               WHEN cmc.completionstate = 1 THEN 'Completada'
               WHEN cmc.completionstate = 2 THEN 'Completada correctamente'
               WHEN cmc.completionstate = 3 THEN 'Completada con fallos'
               ELSE 'Desconocido'
            END AS 'progress',
            DATE_FORMAT(FROM_UNIXTIME(cmc.timemodified), '%Y-%m-%d %H:%i') AS 'whenf'
            FROM mco_course_modules_completion cmc
            JOIN mco_user u ON cmc.userid = u.id
            JOIN mco_course_modules cm ON cmc.coursemoduleid = cm.id
            JOIN mco_course c ON cm.course = c.id
            JOIN mco_modules m ON cm.module = m.id
            WHERE u.id = {$userid} AND c.id = {$courseid} ORDER BY whenf desc");

        return $info;
    }

}
