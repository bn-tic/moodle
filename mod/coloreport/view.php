<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Page module version information
 *
 * @package mod_coloreport
 * @copyright  2009 Petr Skoda (http://skodak.org)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require('../../config.php');
require_once($CFG->dirroot . '/mod/coloreport/lib.php');
require_once($CFG->dirroot . '/mod/coloreport/locallib.php');
require_once($CFG->libdir . '/completionlib.php');

$id = optional_param('id', 0, PARAM_INT); // Course Module ID
$p = optional_param('p', 0, PARAM_INT);  // Page instance ID
$inpopup = optional_param('inpopup', 0, PARAM_BOOL);

if ($p) {
    if (!$coloreport = $DB->get_record('coloreport', array('id' => $p))) {
        print_error('invalidaccessparameter');
    }
    $cm = get_coursemodule_from_instance('coloreport', $coloreport->id, $coloreport->course, false, MUST_EXIST);
} else {
    if (!$cm = get_coursemodule_from_id('coloreport', $id)) {
        print_error('invalidcoursemodule');
    }
    $coloreport = $DB->get_record('coloreport', array('id' => $cm->instance), '*', MUST_EXIST);
}

$course = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);

require_course_login($course, true, $cm);
$context = context_module::instance($cm->id);
require_capability('mod/coloreport:view', $context);

// Completion and trigger events.
coloreport_view($coloreport, $course, $cm, $context);

$PAGE->set_url('/mod/coloreport/view.php', array('id' => $cm->id));

$options = empty($coloreport->displayoptions) ? array() : unserialize($coloreport->displayoptions);

if ($inpopup and $coloreport->display == RESOURCELIB_DISPLAY_POPUP) {
    $PAGE->set_coloreportlayout('popup');
    $PAGE->set_title($course->shortname . ': ' . $coloreport->name);
    $PAGE->set_heading($course->fullname);
} else {
    $PAGE->set_title($course->shortname . ': ' . $coloreport->name);
    $PAGE->set_heading($course->fullname);
    $PAGE->set_activity_record($coloreport);
}
echo $OUTPUT->header();

$quizs = $DB->get_records_sql("SELECT id FROM mco_quiz WHERE mco_quiz.course = {$course->id}");
$tests = new stdClass();


$html = '';

switch($coloreport->content){
    case 1: 
        $html .= "<p class='headline headline-easy alert-success'><i class='fa fa-tachometer'></i> Preguntas de dificultad fácil</p>";
        break;
    case 2:
        $html .= "<p class='headline headline-medium alert alert-warning'><i class='fa fa-tachometer'></i> Preguntas de dificultad media</p>";
        break;
    case 3:
        $html .= "<p class='headline headline-hard alert-danger'><i class='fa fa-tachometer'></i> Preguntas de dificultad elevada</p>";
        break;
}



$html .= '<div class="table-wrapper-scroll-y custom-scrollbar"><table class="table table-striped table-condensed">
  <tr>
    <th class="text-center">Cuestionario</th>
    <th class="text-center">Pregunta</th>
    <th class="text-center">Respuesta Correcta</th>
  </tr>';

foreach ($quizs as $quiz) {
    $tests = $DB->get_recordset_sql("SELECT mco_quiz.name, mco_question_attempts.questionsummary, mco_question_attempts.rightanswer, ROUND(1-avg(case mco_question_attempt_steps.state when 'gradedright' then 1 else 0 end),2) as difficultylevel, mco_question_attempts.questionid
        FROM mco_quiz_attempts 
        JOIN mco_quiz ON mco_quiz.id = mco_quiz_attempts.quiz
        JOIN mco_question_usages ON mco_question_usages.id = mco_quiz_attempts.uniqueid
        JOIN mco_question_attempts ON mco_question_attempts.questionusageid = mco_question_usages.id
        JOIN mco_question_attempt_steps ON mco_question_attempt_steps.questionattemptid = mco_question_attempts.id
        LEFT JOIN mco_question_attempt_step_data ON mco_question_attempt_step_data.attemptstepid = mco_question_attempt_steps.id
        WHERE mco_quiz_attempts.quiz = {$quiz->id}
        AND (mco_question_attempt_steps.state = 'gradedright' OR mco_question_attempt_steps.state = 'gradedwrong' OR mco_question_attempt_steps.state='gradedpartial')
        GROUP BY mco_question_attempts.questionid");
        
        foreach($tests as $test){
            if($coloreport->content == 1){
                if($test->difficultylevel <= 0.3){
                    $html .= "<tr><td>{$test->name}</td>";
                    $html .= "<td>{$test->questionsummary}</td>";
                    $html .= "<td>{$test->rightanswer}</td></tr>";
                }
            }else if($coloreport->content == 2){
                if($test->difficultylevel  < 0.5 && $test->difficultylevel > 0.3){
                    $html .= "<tr><td>{$test->name}</td>";
                    $html .= "<td>{$test->questionsummary}</td>";
                    $html .= "<td>{$test->rightanswer}</td></tr>";
                }
            }else if($coloreport->content == 3){
                if($test->difficultylevel  >= 0.5){
                    $html .= "<tr><td>{$test->name}</td>";
                    $html .= "<td>{$test->questionsummary}</td>";
                    $html .= "<td>{$test->rightanswer}</td></tr>";
                }                
            }
        }
}

$html .= '</table></div>';

echo $html;

echo $OUTPUT->footer();
