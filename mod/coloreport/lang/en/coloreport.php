<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'coloreport', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   mod_coloreport
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['configdisplayoptions'] = 'Select all options that should be available, existing settings are not modified. Hold CTRL key to select multiple fields.';
$string['content'] = 'Categoría';
$string['contentheader'] = 'Categoría de preguntas';
$string['createcoloreport'] = 'Create a new coloreport resource';
$string['displayoptions'] = 'Available display options';
$string['displayselect'] = 'Display';
$string['displayselectexplain'] = 'Select display type.';
$string['legacyfiles'] = 'Migration of old course file';
$string['legacyfilesactive'] = 'Active';
$string['legacyfilesdone'] = 'Finished';
$string['modulename'] = 'Informe de Colores';
$string['modulename_help'] = 'Informe personalizado según la categoría de preguntas seleccionada';
$string['modulename_link'] = 'mod/coloreport/view';
$string['modulenameplural'] = 'coloreports';
$string['optionsheader'] = 'Display options';
$string['coloreport-mod-coloreport-x'] = 'Any coloreport module coloreport';
$string['coloreport:addinstance'] = 'Add a new coloreport resource';
$string['coloreport:view'] = 'View coloreport content';
$string['pluginadministration'] = 'coloreport module administration';
$string['pluginname'] = 'Informes de colores';
$string['popupheight'] = 'Pop-up height (in pixels)';
$string['popupheightexplain'] = 'Specifies default height of popup windows.';
$string['popupwidth'] = 'Pop-up width (in pixels)';
$string['popupwidthexplain'] = 'Specifies default width of popup windows.';
$string['printheading'] = 'Display coloreport name';
$string['printheadingexplain'] = 'Display coloreport name above content?';
$string['printintro'] = 'Display coloreport description';
$string['printintroexplain'] = 'Display coloreport description above content?';
$string['privacy:metadata'] = 'The mod_coloreport plugin does not store any personal data.';
$string['search:activity'] = 'Informes de Colores';
$string['category'] = 'Categoría';
$string['green'] = 'Verde (Preguntas de dificultad: fácil)';
$string['orange'] = 'Naranja (Preguntas de dificultad: media)';
$string['red'] = 'Rojo (Preguntas de dificultad: elevada)';
$string['blank'] = '';
