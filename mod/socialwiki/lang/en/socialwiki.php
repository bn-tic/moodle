<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains all en_utf8 translations in alphabetical order.
 *
 * @package mod_socialwiki
 * @copyright NMAI-lab
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 */

$string['addcomment'] = 'Añadir comentario';
$string['addedbegins'] = 'Añadido inicio';
$string['addedends'] = 'Añadido fin';
$string['admin'] = 'Administración';
$string['adminmenu'] = 'Admin menú';
$string['attachmentattach'] = 'Añadir adjunto';
$string['attachmentimage'] = 'Añadir como imagen';
$string['attachmentlink'] = 'Añadir como enlace';
$string['attachments'] = 'Adjuntos';
$string['backcomments'] = 'Volver a comentarios';
$string['backversions'] = 'Volver a versiones';
$string['backoldversion'] = 'Volver a una versión anterior';
$string['backpage'] = 'Volver a la página';
$string['backtomapmenu'] = 'Volver al menú';
$string['changerate'] = '¿Desea cambiarlo?';
$string['cannoteditpage'] = 'No puedes editar esta página.';
$string['cannotfollow'] = 'No puedes seguirte a ti mismo';
$string['cannotlike'] = 'Debe ser un usuario inscrito';
$string['deniedfollow'] = 'No tienes permisos para seguir a nadie';
$string['cannotmanagefiles'] = 'No tienes permisos para admnistrar wikis.';
$string['cannotviewfiles'] = 'No tienes permisos para ver ficheros wiki.';
$string['cannotviewpage'] = 'No puedes ver esta página.';
$string['classic'] = 'Clásico';
$string['comparesel'] = 'Comparar seleccionado';
$string['comments'] = 'Comentarios';
$string['commentnewest'] = 'El más nuevo el primero';
$string['commentoldest'] = 'El más antiguo el primero';
$string['commentscount'] = 'Comentarios ({$a})';
$string['comparewith'] = 'Comparar página {$a->old} con {$a->new}';
$string['userpages'] = 'Mis páginas';
$string['userpages_help'] = 'Lista de tus páginas creadas.';
$string['createcomment'] = 'Creando comentario';
$string['creating'] = 'Creando página';
$string['createpage'] = 'Crear página';
$string['createddate'] = 'Creado: {$a->date} por {$a->username}';
$string['creole'] = 'Creole';
$string['defaultformat'] = 'Formato por defecto';
$string['defaultformat_help'] = 'Esta configuración determina el formato predeterminado utilizado al editar páginas.
* HTML: el editor de HTML está disponible
* Creole: un lenguaje de marcado wiki común para el que está disponible una pequeña barra de herramientas de edición
* Nwiki: lenguaje de marcado similar a Mediawiki utilizado en el módulo Nwiki contribuido';
$string['follow'] = 'Seguir';
$string['follow_tip'] = 'Click para seguir a este usuario';
$string['unfollow'] = 'Dejar de seguir';
$string['unfollow_tip'] = 'Click para dejar de seguir a este usuario';
$string['like'] = 'Like';
$string['likes'] = 'Likes';
$string['like_tip'] = 'Like/Unlike esta página';

$string['deletecomment'] = 'Eliminando comentario';
$string['deletecommentcheck'] = 'Eliminar comentario';
$string['deletecommentcheckfull'] = '¿Está seguro de eliminar este comentario?';
$string['deleteupload'] = 'Eliminar';
$string['deletedbegins'] = 'Eliminar inicio';
$string['deletedends'] = 'Eliminar fin';
$string['deleteversions'] = 'Eliminar versiones';
$string['distance0'] = 'No está en tu red';
$string['distance1'] = 'Seguido';
$string['distance2'] = 'Segunda conexión';
$string['distance3'] = 'Conexión distante';
$string['diff'] = 'Diff';
$string['diff_help'] = 'Diff entre páginas seleccionadas';
$string['edit'] = 'Editar';
$string['editcomment'] = 'Editar comentario';
$string['editblocks'] = 'Activar bloques de edición';
$string['editfiles'] = 'Editar archivos';
$string['editing'] = 'Editar página';
$string['editingcomment'] = 'Editar comentario';
$string['editingpage'] = 'Editando la página \'{$a}\'';
$string['editsection'] = 'editar';
$string['editwarning'] = 'La edición creará una nueva versión de la página, no modificará esta versión.';
$string['files'] = 'Archivos';
$string['filenotuploadederror'] = 'El archivo \'{$a}\' no ha podido subirse correctamente.';
$string['filtername'] = 'Autoenlazando';
$string['forceformat'] = 'Forzar formato';
$string['forceformat_help'] = 'Si se fuerza el formato (checkbox tickado), no habrá elección de cambiar el formato cuando se esté editando una página.';
$string['format'] = 'Formato';
$string['format_help'] = '* HTML: el editor HTML está disponible
* Creole: un lenguaje de marcado wiki común para el que está disponible una pequeña barra de herramientas de edición
* Nwiki: lenguaje de marcado similar a Mediawiki utilizado en el módulo Nwiki contribuido';
$string['formathtml'] = 'formato HTML';
$string['formathtml_help'] = 'El editor HTML puede usarse para formatear contenido.

Para crear una nueva página, escriba el nombre de la nueva página entre corchetes dobles, por ejemplo [[Página 2]].
Para vincular a una página, copie la url de la página entre corchetes dobles con un título de página existente dentro de los enlaces a una búsqueda de esa página.';
$string['formatcreole'] = 'Formato creole';
$string['formatcreole_help'] = 'Creole es un lenguaje de marcado wiki común con una barra de herramientas de edición para insertar el marcado apropiado.

Para crear una nueva página, escriba el nombre de la nueva página entre corchetes dobles, por ejemplo [[Página 2]].
Para vincular a una página, copie la URL de la página, entre corchetes dobles con un título de página existente dentro de los enlaces para buscar esa página.';
$string['formatcreole_link'] = 'mod/socialwiki/creole';
$string['formatnwiki'] = 'Formato NWiki';
$string['formatnwiki_help'] = 'Nwiki es el lenguaje de marcado similar a Mediawiki utilizado en el módulo Nwiki contribuido.

Para crear una nueva página, escriba el nombre de la nueva página entre corchetes dobles, por ejemplo [[Página 2]].
Para vincular a una página, copie la url de la página entre corchetes dobles con un título de página existente dentro de los enlaces a una búsqueda de esa página.';
$string['formatnwiki_link'] = 'mod/socialwiki/nwiki';

$string['help'] = 'Ayuda';
$string['help_admin'] = 'Como profesor o administrador, tiene acceso a la página de administración para corregir cualquier contenido no deseado. </br>
El ícono de ajustes junto al nombre de una página editará la página especificada.
El ícono X eliminará la página y la redireccionará a la página principal si la página actual es la que se está eliminando. </br>
El botón debajo de la tabla cambiará la vista entre mostrar páginas relacionadas (otras versiones) y enumerar todas las páginas en la wiki. </br>
</br>
Para llegar a esta vista, vaya a la pestaña de administrador de cualquier página.';
$string['help_create'] = 'La vista de creación le permitirá crear una nueva página para la wiki. </br>
Todo lo que se requiere es el título de la página, aunque también puede existir la opción de elegir un formato. </br>
Si el campo de formato está disponible, habrá 3 opciones:
<ul> <li> Formato HTML: el formato predeterminado y la mejor opción para la mayoría de los usuarios, incluye la posibilidad de cambiar el contenido HTML directamente. </li>
    <li> Formato criollo: un lenguaje de marcado wiki ligero para usuarios avanzados. </li>
    <li> Formato Nwiki: interfaz similar al formato criollo con un conjunto diferente de marcas. </li> </ul>
El formato se elige para todas las versiones de esta página. Todas las versiones futuras deberán usar el formato seleccionado. </br>
</br>
Haga clic en el botón Crear página para comenzar a trabajar en la primera versión de esta página. En este punto, se crea la página y no puede deshacer esta acción, debe guardar la primera versión o se creará una página en blanco con el título especificado.';
$string['help_diff'] = 'La vista de diff compara dos páginas seleccionadas de un árbol. </br>
Las diferencias se muestran en color, con las adiciones en verde y las partes faltantes en amarillo. </br>
Navegue a través de diferentes versiones para comparar usando los enlaces de página debajo de las páginas comparadas. </br>
</br>
Para acceder a esta vista, vaya a la <a href="#versions"> pestaña de versiones </a> de una página o realice una <a href="#search"> búsqueda </a> para ver un árbol y seleccione Dos versiones para comparar.';
$string['help_edit'] = 'La vista de edición le permite modificar una página y crear una nueva versión. </br>
El editor que se eligió en la página de creación se mostrará con el contenido anterior, lo que le permitirá cambiar cualquier cosa a su gusto. </br>
</br>
El formato HTML mostrará el editor normal que se encuentra en otro lugar con opciones de formato en la parte superior. </br>
Los editores de formato criollo y Nwiki son similares, aunque no tienen tantas opciones y las etiquetas de formato se mostrarán en el contenido. </br>
Con estos dos formatos también es importante adjuntar todos los archivos que va a usar antes de agregar cualquier contenido o tendrá que comenzar de nuevo. </br>
</br>
Hay enlaces especiales a páginas en socialwiki y aquí hay 3 formas diferentes de agregarlos:
<ul> <li> [[nombre de página]] Enlaces a un nombre de página de búsqueda. </li>
    <li> [[nombre de página @ pageid]] Enlaces a una versión específica de una página. </li>
    <li> [[nombre de la página @.]] Enlaces a la versión más reciente de esa página. </li> </ul>
Una vez que haya terminado de editar, puede obtener una vista previa de la página usando el botón de vista previa en la parte inferior de la página y cuando esté listo, puede usar el botón Guardar para publicar su nueva versión de la página. </br>
Ahora que se creó la versión de la página, no puede eliminarla, solo un maestro tiene la opción de eliminar contenido de la wiki.';
$string['help_home'] = 'La vista de inicio le ofrece una visión general de la wiki, lo que permite un fácil acceso para ver todas las páginas y colaboradores. </br>
Hay 4 pestañas para mirar:
<ul> <li> Explorar: vea todas las versiones de página nuevas, las versiones de página de sus seguidores o explore todas las versiones de página disponibles </li>
    <li> Páginas: eche un vistazo a todas las páginas de la wiki, al hacer clic en una se redirigirá a una <a href="#search"> vista de búsqueda </a> de todas las versiones creadas. </li>
    <li> Administrar: vea todos sus Favoritos (su versión más reciente de una página), sus Me gusta y las versiones de página que ha creado. </li>
    <li> Personas: vea una lista de sus seguidores y los usuarios que lo siguen. También puede navegar a través de todos los usuarios que han contribuido. </li> </ul>';
$string['help_search'] = 'La vista de búsqueda buscará su consulta tanto en el título como en el contenido de las páginas y mostrará los resultados. </br>
Dejar el cuadro de búsqueda vacío o escribir * mostrará todas las páginas disponibles. </br>
Para obtener más información, consulte <a href="#versions"> Las versiones ayudan </a>, ya que estas vistas funcionan exactamente igual. </br>
</br>
Para llegar a esta vista, use el cuadro de búsqueda en la parte superior derecha de cualquier página para buscar en el wiki actual.';
$string['help_viewuserpages'] = 'La vista de usuario le mostrará el contenido de otro usuario. </br>
Desde aquí puede ver la similitud entre usted y el usuario especificado en los puntajes de pares. </br>
También existe la opción de seguir al usuario si está interesado en su contenido y desea recibir actualizaciones sobre lo que publica.';
$string['help_versions'] = 'Hay 3 vistas de búsqueda diferentes:
<ul> <li> Vista de árbol: muestra los resultados de búsqueda en una vista de árbol para que las versiones de la página se puedan ver fácilmente. </li>
    <li> Vista de lista: muestra una lista de páginas ordenadas usando sus Me gusta y las personas que está siguiendo para ordenar las páginas. </li>
    <li> Vista popular: ordena las páginas con las páginas con más me gusta que aparecen en la parte superior. </li> </ul>
Use el botón en la parte superior derecha de cada nodo para minimizarlo u ocultar una rama completa. </br>
Para comparar dos versiones, seleccione las páginas con los botones de opción en la parte inferior de un nodo y luego presione Comparar seleccionados en la parte inferior.';
$string['html'] = 'HTML';
$string['incorrectdeleteversions'] = 'Las versiones de página proporcionadas para su eliminación son incorrectas.';
$string['insertcomment'] = 'Añadir comentario';
$string['insertimage'] = 'Añadir imagen...';
$string['insertimage_help'] = 'Esta lista desplegable insertará una imagen en el editor wiki. Si necesita agregar imágenes, haga clic en el enlace "Adjuntar archivos" arriba.';
$string['invalidcoursemoduleid'] = 'Parámetros incorrectos';
$string['invalidparameters'] = 'Course Module ID incorrecto.';
$string['invalidsection'] = 'Intentando acceder a una sección incorrecta.';
$string['invalidsesskey'] = 'Sesskey incorrecta. Por favor, reenvía de nuevo';
$string['invalidsubwikiid'] = 'Invalid Subwiki ID has been given.';
$string['invalidwikiid'] = 'Invalid Wiki ID has been given.';
$string['individualpagedoesnotexist'] = 'Página no existe';
$string['list'] = 'Vista de la lista';
$string['listall'] = 'Listar todo';
$string['listrelated'] = 'Lista relacionada';
$string['home'] = 'Home';
$string['homemenu'] = 'Home menu';
$string['hometitle'] = 'Inicio';
$string['makepage'] = 'Nueva página';
$string['migrationfinished'] = 'La migración finalizó con éxito.';
$string['migrationfinishednowikis'] = 'La migración terminó, no se migraron wikis';
$string['missingpages'] = 'Páginas sin contenido';
$string['modern'] = 'Moderno';
$string['modified'] = 'Modificado';
$string['modulename'] = 'LIKEs';
$string['modulename_help'] = 'El módulo de actividades de Likes permite a los participantes agregar y editar una colección de páginas web. Se crea una nueva página wiki para cada edición que alguien hace en una página. Las páginas solo existen mientras a alguien le gusta, si una página tiene 0 me gusta, se elimina.

Los participantes pueden seguirse entre sí y dar me gusta a las páginas para que los resultados de búsqueda se adapten especialmente a cada participante.
Se mantiene una versión de las versiones anteriores de cada página en la wiki, que enumera las ediciones realizadas por cada participante.

Los wikis sociales tienen muchos usos, como

* Para apuntes grupales o guías de estudio
* Para los miembros de una facultad para planificar un esquema de trabajo o agenda de reunión juntos
* Para que los estudiantes creen en colaboración un libro en línea, creando contenido sobre un tema establecido por su tutor
* Para la narración colaborativa o la creación de poesía, donde cada participante escribe una línea o verso.';
$string['create'] = 'Crear';
$string['modulename_link'] = 'mod/socialwiki/view';
$string['modulenameplural'] = 'LIKESs';
$string['navigation'] = 'Navegación';
$string['navigationfrom'] = 'Esta página proviene de';
$string['navigationfrom_help'] = 'Las páginas wiki que enlazan con esta página';
$string['navigationto'] = 'Esta página va a';
$string['navigationto_help'] = 'Enlaces a otras páginas';
$string['newpage'] = 'Nueva';
$string['newpagehdr'] = 'Nueva página';
$string['newpagetitle'] = 'Nuevo título de página';
$string['noattachments'] = '<strong>Sin ficheros adjuntos</strong>';
$string['nocontent'] = 'No hay contenido para esta página.';
$string['nocontribs'] = 'No tienes contribuciones en este wiki.';
$string['nocomments'] = 'No hay comentarios';
$string['nocreatepermission'] = 'Crear permiso de página necesario';
$string['noeditcommentpermission'] = 'Se necesita permiso para editar comentarios';
$string['noeditpermission'] = 'Editar permiso de página necesario';
$string['nofrompages'] = 'No hay enlaces a esta página.';
$string['noversions'] = 'No hay versiones para esta página.';
$string['nomanagecommentpermission'] = 'Administrar comentarios - permiso necesario';
$string['nomanagewikipermission'] = 'Administrar el permiso wiki necesario';
$string['noorphanedpages'] = 'No hay páginas huérfanas.';
$string['norecentactivity'] = 'No hay actividad reciente';
$string['norated'] = 'Esta página no ha sido calificada todavía, ¡sé el primero!';
$string['norating'] = 'Sin evaluar';
$string['noteditblocks'] = 'Desactivar bloques de edición';
$string['notingroup'] = 'No en grupo';
$string['notopages'] = 'Esta página no enlaza con otras páginas.';
$string['notmigrated'] = 'Este wiki aún no se ha migrado. Por favor contacte a su administrador.';
$string['nonode'] = 'El nodo padre no existe';
$string['noupdatedpages'] = 'No hay páginas actualizadas.';
$string['nouser'] = 'Se debe pasar un pageid o userid a follow.php';
$string['noviewcommentpermission'] = 'Ver comentarios permiso necesario';
$string['noviewpagepermission'] = 'Ver permiso de página necesario';
$string['nwiki'] = 'NWiki';
$string['oldversion'] = 'Versión antigua';
$string['orphaned'] = 'Páginas huérfanas';
$string['orphaned_help'] = 'Lista de páginas que no están vinculadas desde otra página.';
$string['page-mod-socialwiki-x'] = 'Cualquier página de módulo wiki social';
$string['page-mod-socialwiki-view'] = 'Página principal del módulo Wiki social';
$string['page-mod-socialwiki-comments'] = 'Página de comentarios del módulo Wiki social';
$string['page-mod-socialwiki-versions'] = 'Página de versiones';
$string['pagecontributors'] = 'Participantes en esta página:';
$string['pageexists'] = 'Esta página ya existe. Redirigiendo a ella.';
$string['pageindex'] = 'Índice de página';
$string['pageindex_help'] = 'El árbol de páginas disponible';
$string['pagelist'] = 'Lista de páginas';
$string['pagelist_help'] = 'Lista de páginas clasificadas por orden alfabético';
$string['pagename'] = 'Nombre de la página';
$string['peerscores'] = 'PERFIL';
$string['pluginadministration'] = 'Administración';
$string['pluginname'] = 'LIKES';
$string['popular'] = 'Vista popular';
$string['prettyprint'] = 'Version para imprimir';
$string['previewwarning'] = 'Esto es una vista previa. Los cambios aún no se han guardado.';
$string['rated'] = 'Calificó esta página como {$a}';
$string['rating'] = 'Clasificación';
$string['ratingmode'] = 'Modo de calificación';
$string['recommended'] = 'páginas recomendadas';
$string['reparsetimeout'] = 'Reparando el tiempo de espera predeterminado';
$string['repeatedsection'] = 'Error: el nombre de la sección no puede repetirse \'{$a}\'';
$string['restore'] = 'Restaurar';
$string['removeallwikitags'] = 'Eliminar todas las etiquetas';
$string['removepages'] = 'Eliminar páginas';
$string['restoreconfirm'] = '¿Estás seguro de que quieres restaurar la versión #{$a}?';
$string['restoreerror'] = 'Version #{$a} no pudo ser restaurada';
$string['restorethis'] = 'Restaurar esta versión';
$string['restoreversion'] = 'Restaurar versión anterior';
$string['restoring'] = 'Restaurando versión #{$a}';
$string['return'] = 'Volver';
$string['save'] = 'Guardar';
$string['saving'] = 'Guardando';
$string['savingerror'] = 'Error al guardar';
$string['searchcontent'] = 'Buscar contenido en la página';
$string['search'] = 'Buscar';
$string['searchresult'] = 'Buscar resultados relacionados: ';
$string['searchresults_empty'] = 'Sin resultados';
$string['searchterms'] = 'Buscar términos';
$string['searchviews'] = 'Visitas: ';
$string['special'] = 'Especial';
$string['style'] = 'Seleccionar estilo para la actividad';
$string['tableofcontents'] = 'Tabla de contenidos';
$string['tableview'] = 'Vista de tabla';
$string['tableview_help'] = 'Vista de tabla de todas las páginas';
$string['tagsdeleted'] = 'Las etiquetas han sido eliminadas';
$string['tagtitle'] = 'Mira la etiqueta "{$a}"';
$string['timesrating'] = 'Esta página ha sido calificada  {$a->c} veces con un promedio de: {$a->s}';
$string['tree'] = 'Vista de árbol';
$string['updatedpages'] = 'Páginas actualizadas';
$string['updatedpages_help'] = 'Páginas actualizadas recientemente';
$string['updatedwikipages'] = 'Páginas actualizadas';
$string['upload'] = 'Subir y borrar';
$string['makepage'] = 'Hacer una nueva página';
$string['unknownfirstname'] = 'Desconocido';
$string['unknownlastname'] = 'Usuario';
$string['uploadname'] = 'Nombre del archivo';
$string['uploadactions'] = 'Acciones';
$string['uploadtitle'] = 'Adjuntar archivos';
$string['uploadfiletitle'] = 'Archivos adjuntos';
$string['userdata'] = 'Datos del usuario';
$string['userdata_help'] = 'Los datos del usuario incluyen autores de páginas, me gusta y seguidores.';
$string['versionerror'] = 'No existe el ID de esta versión';
$string['versions'] = 'Versiones';
$string['versions_help'] = 'Las versiones enumeran enlaces a versiones anteriores de la página.';
$string['view'] = 'Vista';
$string['viewallversions'] = 'Vista de todas la versiones';
$string['viewcurrent'] = 'Versión actual';
$string['viewuserpages'] = 'Ver las páginas del usuario';
$string['socialwiki'] = 'Likes';
$string['wikiattachments'] = 'Adjuntos';
$string['wikiboldtext'] = 'Texto en negrita';
$string['wikiexternalurl'] = 'External URL';
$string['wikifiles'] = 'Archivos';
$string['wikiheader'] = 'Nivel {$a} cabecera';
$string['wikihr'] = 'Regla horizontal';
$string['wikiimage'] = 'Imagen';
$string['wikiinternalurl'] = 'Enlace interno';
$string['wikiintro'] = 'Descripción';
$string['wikiitalictext'] = 'Texto en cursiva';
$string['wikimode'] = 'Modo Wiki';
$string['wikimode_help'] = 'El modo wiki determina si todos pueden editar el wiki, un wiki colaborativo, o si todos tienen su propio wiki, que solo ellos pueden editar, un wiki individual.';
$string['wikimodecollaborative'] = 'Wiki colaborativo';
$string['wikimodeindividual'] = 'Wiki individual';
$string['wikiname'] = 'Nombre';
$string['wikinowikitext'] = 'Pre-formateado';
$string['wikiorderedlist'] = 'Lista ordenada';
$string['wikisettings'] = 'Opciones';
$string['wikiunorderedlist'] = 'Lista desordenada';
$string['socialwiki:addinstance'] = 'Agregar una nueva Wiki';
$string['socialwiki:createpage'] = 'Crear nueva página';
$string['socialwiki:editcomment'] = 'Añadir comentarios a la página';
$string['socialwiki:editpage'] = 'Guardar página';
$string['socialwiki:managecomment'] = 'Administrar comentarios';
$string['socialwiki:manage_socialwiki'] = 'Configuración';
$string['socialwiki:managefiles'] = 'Administrar archivos';
$string['socialwiki:viewcomment'] = 'Ver comentarios de la página';
$string['socialwiki:viewpage'] = 'Ver páginas';
$string['socialwikipages'] = 'Social páginas';
$string['wrongversionsave'] = 'Otro usuario ha creado una versión mientras estaba editando y ha sobrescrito sus cambios, verifique las versiones de la página.';
$string['socialwiki:managewiki'] = 'Administrar';

/*
 * All table related strings.
 */

// Headers.
$string['title'] = 'Título';
$string['title_help'] = 'El título de la versión de la página..';
$string['contributors'] = 'Participantes';
$string['contributors_help'] = 'Los usuarios que han contribuido a esta página.';
$string['updated'] = 'Actualizado';
$string['updated_help'] = 'Hace cuánto tiempo se creó la página.';
$string['likes'] = 'Likes';
$string['likes_help'] = 'Cuántos likes tiene esta página.';
$string['views'] = 'Visitas';
$string['views_help'] = 'Cuántas visitas tiene la página.';
/* Title versions is already declared above. */
$string['versions_help'] = 'Cuántas versiones diferentes tiene este tema.';
$string['name'] = 'Nombre';
$string['name_help'] = 'Nombre del usuario.';
$string['favourite'] = 'Favoritos';
$string['favourite_help'] = 'Un favorito es el más reciente como en un tema.';
$string['popularity'] = 'Seguidores';
$string['popularity_help'] = 'Cuántos seguidores tiene este usuario.';
$string['likesim'] = 'Likes similares';
$string['likesim_help'] = 'Qué tan similares son sus gustos.';
$string['followsim'] = 'Seguidores similares';
$string['followsim_help'] = 'How similar your follows are to the user.';
$string['networkdistance'] = '';
$string['networkdistance_help'] = 'A cuántos pasos está el usuario en su red de seguidores.';

// Each table type and what is shown when the table is empty.
$string['pagesfollowed'] = 'De los usuarios que sigues';
$string['pagesfollowed_empty'] = 'No hay versiones de página no vistas que les gusten a los usuarios que sigues.';
$string['newpages'] = 'Nuevas versiones de página';
$string['newpages_empty'] = 'No hay nuevas versiones de página.';
$string['allpages'] = 'Todas las versiones de página';
$string['allpages_empty'] = 'No hay versiones de página.';
$string['alltopics'] = 'Todas las páginas';
$string['alltopics_empty'] = 'No hay páginas';
$string['myfaves'] = 'Mis favoritos';
$string['myfaves_empty'] = 'No tienes páginas favoritas.';
$string['mylikes'] = 'Mis Likes';
$string['mylikes_empty'] = 'No tienes likes.';
$string['mypages'] = 'Mis páginas';
$string['mypages_empty'] = 'No has creado páginas.';
$string['followers'] = 'Seguidores';
$string['followers_empty'] = 'No tienes seguidores.';
$string['followedusers'] = 'Siguiendo';
$string['followedusers_empty'] = 'No sigues a nadie.';
$string['allusers'] = 'Todos los usuarios activos';
$string['allusers_empty'] = 'No hay otros usuarios.';
$string['userfaves'] = 'Páginas favoritas';
$string['userfaves_empty'] = 'Este usuario no tiene páginas favoritas.';
$string['userpages'] = 'Páginas creadas';
$string['userpages_empty'] = 'Este usuario no ha creado ninguna página.';