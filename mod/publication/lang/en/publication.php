<?php
// This file is part of mod_publication for Moodle - http://moodle.org/
//
// It is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// It is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'publication', language 'en'
 *
 * @package       mod_publication
 * @author        Philipp Hager
 * @author        Andreas Windbichler
 * @copyright     2014 Academic Moodle Cooperation {@link http://www.academic-moodle-cooperation.org}
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['modulename'] = 'Área de intercambio';
$string['pluginname'] = 'Área de intercambio';
$string['modulename_help'] = 'El área de intercambio ofrece las siguientes características:

* Los participantes pueden cargar documentos que están disponibles para otros participantes inmediatamente o después de que haya revisado los documentos y haya dado su consentimiento.
* Se puede elegir una asignación como base para la carpeta de un estudiante. El profesor puede decidir qué documentos de la tarea son visibles para todos los participantes. Los maestros también pueden dejar que los participantes decidan si sus documentos deben ser visibles para otros.';
$string['modulenameplural'] = 'Áreas de intercambios';
$string['pluginadministration'] = 'Área de intercambio (administración)';
$string['publication:addinstance'] = 'Añadir una nueva área de intercambio';
$string['publication:view'] = 'Ver área de intercambio';
$string['publication:upload'] = 'Subir archivos al área de intercambio';
$string['publication:approve'] = 'Decidir si los archivos deben ser visibles para cada estudiante';
$string['publication:grantextension'] = 'Grant extension';
$string['search:activity'] = 'Área de intercambio - información de la actividad';

$string['name'] = 'Nombre del área de intercambio';
$string['obtainstudentapproval'] = 'Obtener aprobación';
$string['saveapproval'] = 'Guardar aprobación';
$string['configobtainstudentapproval'] = 'Los documentos son visibles después del consentimiento del alumno.';
$string['hideidnumberfromstudents'] = 'Ocultar ID-Number';
$string['hideidnumberfromstudents_desc'] = 'Ocultar columna ID-Number en "Public Files" tabla de estudiantes';
$string['obtainteacherapproval'] = 'Aprobado por defecto';
$string['configobtainteacherapproval'] = 'Los documentos de los estudiantes son visibles por defecto para todos los demás participantes.';
$string['maxfiles'] = 'Número máximo de adjuntos';
$string['configmaxfiles'] = 'Número máximo predeterminado de archivos adjuntos permitidos por usuario.';
$string['maxbytes'] = 'Tamaño máximo del archivo adjunto';
$string['configmaxbytes'] = 'Tamaño máximo predeterminado para todos los archivos en la carpeta del estudiante.';

$string['reset_userdata'] = 'Toda la información';

// Strings from the File  mod_form.
$string['autoimport'] = 'Sincronizar automáticamente con la asignación';
$string['autoimport_help'] = 'Si está activado, los nuevos envíos en la Asignación correspondiente se importarán automáticamente en el módulo de carpeta del estudiante. (Opcional) La aprobación del estudiante debe obtenerse nuevamente para los nuevos archivos.';
$string['configautoimport'] = 'Si prefiere que las presentaciones de los alumnos se importen automáticamente en las instancias de la carpeta de alumnos. Esta función se puede habilitar / deshabilitar para cada instancia de carpeta de estudiante por separado.';
$string['availability'] = 'Ranura de tiempo para subir / aprobar';

$string['allowsubmissionsfromdate'] = 'desde';
$string['allowsubmissionsfromdateh'] = 'Ranura de tiempo para subir / aprobar';
$string['allowsubmissionsfromdateh_help'] = 'Puede determinar el período de tiempo durante el cual los estudiantes pueden cargar archivos o dar su aprobación para la publicación de archivos. Durante este período de tiempo, los estudiantes pueden editar sus archivos y también pueden retirar su aprobación para publicación.';
$string['allowsubmissionsfromdatesummary'] = 'Esta asignación aceptará envíos de <strong>{$a}</strong>';
$string['allowsubmissionsanddescriptionfromdatesummary'] = 'Los detalles de la tarea y el formulario de envío estarán disponibles en <strong>{$a}</strong>';
$string['alwaysshowdescription'] = 'Mostrar siempre la descripción';
$string['alwaysshowdescription_help'] = 'Si está deshabilitada, la descripción de asignación anterior solo será visible para los estudiantes en la fecha "Permitir envíos desde".';

$string['duedate'] = 'a';
$string['duedate_help'] = 'Cuando la tarea es debida. Las presentaciones aún se permitirán después de esta fecha, pero las asignaciones enviadas después de esta fecha se marcarán como retrasadas. Para evitar envíos después de una fecha determinada, establezca la fecha de corte de la asignación.';
$string['duedatevalidation'] = 'Due date must be after the allow submissions from date.';

$string['cutoffdate'] = 'Fecha de corte';
$string['cutoffdate_help'] = 'Si se establece, la asignación no aceptará envíos después de esta fecha sin una extensión.';
$string['cutoffdatevalidation'] = 'La fecha de corte no puede ser anterior a la fecha de vencimiento.';
$string['cutoffdatefromdatevalidation'] = 'La fecha de corte debe ser posterior a la fecha de permiso de envío.';

$string['mode'] = 'Modo';
$string['mode_help'] = 'Elija si los estudiantes pueden cargar documentos en la carpeta o los documentos de una tarea son la fuente de la misma.';
$string['modeupload'] = 'los estudiantes pueden subir documentos';
$string['modeimport'] = 'tomar documentos de una tarea';

$string['courseuploadlimit'] = 'Límite de carga del curso';
$string['allowedfiletypes'] = 'Tipos de archivo permitidos (;)';
$string['allowedfiletypes_help'] = 'Los tipos de archivos aceptados se pueden restringir ingresando una lista de tipos MIME separados por punto y coma, e.j. \'video/mp4; audio/mp3; image/png; image/jpeg\', o extensiones de archivo incluyendo un punto, e.j. \'.png; .jpg\'. Si el campo se deja vacío, se permiten todos los tipos de archivos.';
$string['allowedfiletypes_err'] = 'Compruebe la entrada! Extensiones de archivo o separadores no válidos';
$string['obtainteacherapproval_help'] = 'Decida si los archivos se harán visibles inmediatamente después de subirlos o no: <br><ul><li> sí, todos los archivos serán visibles para todos de inmediato</li><li> no - los archivos serán publicados solo después de que el profesor los haya aprobado</li></ul>';
$string['assignment'] = 'Asignación';
$string['assignment_help'] = 'Elija la asignación para importar archivos desde. En el momento en que las asignaciones de grupo no son compatibles y, por lo tanto, no son seleccionables.';
$string['obtainstudentapproval_help'] = 'Decidir si se obtendrá la aprobación de los estudiantes: <br><ul><li> sí, los archivos serán visibles para todos solo después de que el alumno los apruebe. El maestro puede seleccionar estudiantes / archivos individuales para pedir la aprobación.</li><li> no - la aprobación del estudiante no se obtendrá a través de Moodle. La visibilidad del archivo es únicamente la decisión del profesor.</li></ul>';
$string['choose'] = 'por favor elige ...';
$string['importfrom_err'] = 'Tienes que elegir una tarea de la que quieras importar.';
$string['nonexistentfiletypes'] = 'Los siguientes tipos de archivos no fueron reconocidos: {$a}';

$string['groupapprovalmode'] = 'Modo de aprobación de grupo';
$string['groupapprovalmode_help'] = 'Aquí usted decide si se requiere la aprobación de todos los miembros del grupo o solo la aprobación de al menos un miembro del grupo para que los archivos estén visibles. Los archivos solo serán visibles después de la aprobación de todos los miembros del grupo o al menos un miembro del grupo.';
$string['groupapprovalmode_all'] = '<strong>TODOS</strong> los miembros del grupo tienen que aprobar.';
$string['groupapprovalmode_single'] = 'Al menos <strong>UN</strong> miembro tiene que aprobar';

$string['warning_changefromobtainteacherapproval'] = 'Después de activar esta configuración, todos los archivos subidos serán visibles para otros participantes. Todo lo subido se hará visible. Puedes hacer manualmente archivos invisibles para ciertos estudiantes.';
$string['warning_changetoobtainteacherapproval'] = 'Después de desactivar esta configuración, los archivos cargados no serán visibles para otros participantes automáticamente. Tendrás que determinar qué archivos son visibles. Los archivos ya visibles se volverán invisibles.';

$string['warning_changefromobtainstudentapproval'] = 'Si realiza este cambio, solo usted puede decidir qué archivos son visibles para todos los estudiantes. A los estudiantes no se les pide su aprobación. Todos los archivos marcados como aprobados serán visibles para todos los estudiantes independientemente de las decisiones de los estudiantes.';
$string['warning_changetoobtainstudentapproval'] = 'Si realiza este cambio, se solicita a los estudiantes que aprueben todos los archivos marcados como visibles. Los archivos solo serán visibles después de la aprobación de los estudiantes.';

// Strings from the File  mod_publication_grantextension_form.php.
$string['extensionduedate'] = 'Fecha de vencimiento de la extensión';
$string['extensionnotafterduedate'] = 'La fecha de extensión debe ser posterior a la fecha de vencimiento.';
$string['extensionnotafterfromdate'] = 'La fecha de extensión debe ser posterior a la fecha permitida para las presentaciones.';

// Strings from the File  index.php.
$string['nopublicationsincourse'] = 'No hay instancias de carpeta de estudiantes en este curso.';

// Strings from the File  view.php.
$string['allowsubmissionsfromdate_upload'] = 'Posibilidad de subir desde';
$string['allowsubmissionsfromdate_import'] = 'Aprobación de';
$string['duedate_upload'] = 'Posibilidad de subir a';
$string['duedate_import'] = 'Aprobación para';
$string['cutoffdate_upload'] = 'Posibilidad de ultima subida a';
$string['cutoffdate_import'] = 'Última aprobación para';
$string['extensionto'] = 'Extensión a';
$string['filedetails'] = 'Detalles';
$string['assignment_notfound'] = 'La asignación desde la cual se importaron los archivos, ya no se pudo encontrar.';
$string['assignment_notset'] = 'No se ha elegido ninguna asignación.';
$string['updatefiles'] = 'Actualizar archivos';
$string['updatefileswarning'] = 'Los archivos de un estudiante individual en la carpeta del estudiante se actualizarán con su envío de la tarea. Los archivos ya visibles de los estudiantes también se reemplazarán, si se eliminan o actualizan, la configuración del estudiante en cuanto a la visibilidad no se cambiará.';
$string['myfiles'] = 'Archivos propios';
$string['mygroupfiles'] = 'Los archivos de mi grupo';
$string['add_uploads'] = 'Agregar archivos';
$string['edit_uploads'] = 'editar / subir archivos';
$string['edit_timeover'] = 'Los archivos solo se pueden editar durante el período de cambio.';
$string['approval_timeover'] = 'Solo puede cambiar su aprobación durante el período de cambio.';
$string['noentries'] = 'No hay entradas';
$string['nofiles'] = 'No hay archivos disponibles';
$string['nothing_to_show_users'] = 'nada para mostrar - no hay estudiantes disponibles';
$string['nothing_to_show_groups'] = 'nada que mostrar - no hay grupo disponible';
$string['notice'] = 'Aviso:';
$string['notice_uploadrequireapproval'] = 'Todos los archivos cargados se harán visibles solo después de la revisión del maestro';
$string['notice_uploadnoapproval'] = 'Todos los archivos serán visibles inmediatamente para todos al subirlos. El profesor se reserva el derecho de ocultar los archivos publicados en cualquier momento.';
$string['notice_groupimportrequireallapproval'] = 'Decida si sus archivos están disponibles para todos. Todos los miembros del grupo deben dar su aprobación antes de que el archivo sea visible.';
$string['notice_groupimportrequireoneapproval'] = 'Decida si sus archivos están disponibles para todos. La aprobación de un solo miembro del grupo es suficiente para que el archivo sea visible. ¡Discuta el grupo internamente si su archivo debe estar visible o no antes de aprobarlo!';
$string['notice_importrequireapproval'] = 'Decida si sus archivos están disponibles para todos.';
$string['notice_importnoapproval'] = 'Los siguientes archivos son visibles para todos.';
$string['teacher_pending'] = 'confirmación pendiente';
$string['teacher_approved'] = 'visible (aprobado)';
$string['teacher_rejected'] = 'rechazado';
$string['approved'] = 'Aprobado';
$string['show_details'] = 'Mostrar detalles';
$string['student_approve'] = 'aprobado';
$string['student_approved'] = 'aprobado';
$string['student_pending'] = 'no visible (no aprobado)';
$string['pending'] = 'Pendiente';
$string['student_reject'] = 'rechazado';
$string['student_rejected'] = 'rechazado';
$string['rejected'] = 'Rechazado';
$string['visible'] = 'visible';
$string['hidden'] = 'oculto';

$string['allfiles'] = 'Todos los archivos';
$string['publicfiles'] = 'Archivos públicos';
$string['downloadall'] = 'Descargar todos los archivos como ZIP';
$string['optionalsettings'] = 'Opciones';
$string['entiresperpage'] = 'Participantes mostrados por página';
$string['nothingtodisplay'] = 'No hay entradas para mostrar';
$string['nofilestozip'] = 'No hay archivos a zip';
$string['status'] = 'Estado';
$string['studentapproval'] = 'Estado'; // Previous 'Student approval'.
$string['studentapproval_help'] = 'El estado de la columna representa la respuesta de los estudiantes de la aprobación:

* ? - Aprobación pendiente
* ✓ - Aprobación dada
* ✖ - Aprobación rechazada';
$string['teacherapproval'] = 'Aprobación';
$string['visibility'] = 'visible para todos';
$string['visibleforstudents'] = 'visible para todos';
$string['visibleforstudents_yes'] = 'Los estudiantes pueden ver este archivo';
$string['visibleforstudents_no'] = 'Este archivo NO es visible para los estudiantes';
$string['resetstudentapproval'] = 'Reset estado'; // Previous 'Reset student approval'.
$string['savestudentapprovalwarning'] = '¿Estás seguro de que quieres guardar estos cambios? No se puede cambiar el estado una vez que se establece.';

$string['go'] = 'Ir';
$string['withselected'] = 'Con seleccionado...';
$string['zipusers'] = "Descargar como zip";
$string['approveusers'] = "visible para todos";
$string['rejectusers'] = "invisible para todos";
/*el profesor puede otorgar una extesión de tiempo al alumnno*/
$string['grantextension'] = 'grant extension';
$string['saveteacherapproval'] = 'Guardar aprobación';
$string['reset'] = 'Revertir';

// Strings from the File  upload.php.
$string['filesofthesetypes'] = 'Se pueden agregar archivos de estos tipos:';
$string['guideline'] = 'visible para todos:';
$string['published_immediately'] = 'Sí, de inmediato, sin la aprobación de un profesor.';
$string['published_aftercheck'] = 'No, solo después de la aprobación de un profesor.';
$string['save_changes'] = 'Guardar cambios';

// Strings for JS...
$string['total'] = 'total';
$string['details'] = 'Detalles';

// Deprecated since Moodle 2.9!
$string['requiremodintro'] = 'Requerir descripción de actividad';
$string['configrequiremodintro'] = 'Desactive esta opción si no desea forzar a los usuarios a ingresar una descripción de cada actividad.';
