<?php
require_once('config.php');
require_login();

echo $OUTPUT->header();

//manually configurable report 
$report = new stdClass();


//get all courses premium & excellence
$courses = $DB->get_records_sql("SELECT mco_course.id FROM mco_course WHERE mco_course.visible = 1 AND mco_course.fullname LIKE '%premium%' OR mco_course.fullname LIKE '%excellence%'");

foreach ($courses as $course){
    
    $report->courseid = $course->id;
    $report->ownerid = 2;
    $report->visible = 1;
    $report->name = "Mi Resumen Digital";
    $report->summary = '';
    $report->summaryformat = 1;
    $report->type = 'sql';
    $report->pagination = 0;
    $report->components = 'a:5:{s:9:"customsql";a:1:{s:6:"config";O:8:"stdClass":6:{s:8:"querysql";s:702:"SELECT+%0D%0A+CONCAT%28s.NAME%2C%22%3CBR%3E%22%2Cparent%2C%22%3CBR%3E%3Csmall%3E%3Cb%3E%22%2Ctitle%2C%22%3CBR%3E%3C%2Fb%3E%3C%2Fsmall%3E%22%29+as+Info%2CFROM_UNIXTIME%28msst.timemodified%29+as+Fecha%2CVALUE+as+Resumen%0D%0A+FROM++prefix_scorm_scoes_track++AS+msst+%0D%0A%09%09%09%09%09INNER+JOIN+prefix_user+AS+u+ON+u.id%3D+msst.userid%0D%0A%09%09%09%09%09INNER+JOIN+prefix_scorm+AS+s+ON+s.id+%3Dmsst.scormid%0D%0A%09%09%09%09%09INNER+JOIN+prefix_scorm_scoes+AS+ss++ON+ss.id+%3Dmsst.scoid%0D%0A%09%09%09%09%09INNER+JOIN+prefix_course+AS+c+ON+c.id+%3D+s.course%0D%0A%09%09%09%09%09%0D%0AWHERE++element+%3D%22cmi.comments%22+%0D%0Aand+u.id+%3D+%25%25USERID%25%25+++and+c.id+%3D+%25%25COURSEID%25%25%0D%0A";s:8:"courseid";i:531;s:12:"submitbutton";s:15:"Guardar+cambios";s:16:"reportcategories";s:1:"0";s:17:"reportsincategory";s:1:"0";s:14:"remotequerysql";s:0:"";}}s:7:"filters";a:1:{s:8:"elements";a:0:{}}s:11:"permissions";a:2:{s:6:"config";O:8:"stdClass":1:{s:13:"conditionexpr";s:0:"";}s:8:"elements";a:1:{i:0;a:5:{s:2:"id";s:15:"0bc5A7xjVbbtkkx";s:8:"formdata";O:8:"stdClass":0:{}s:10:"pluginname";s:6:"anyone";s:14:"pluginfullname";s:10:"Cualquiera";s:7:"summary";s:53:"Cualquier+usuario+en+el+Campus+podra+ver+este+informe";}}}s:8:"template";a:1:{s:6:"config";O:8:"stdClass":5:{s:7:"enabled";s:1:"0";s:6:"header";a:2:{s:4:"text";s:0:"";s:6:"format";s:1:"1";}s:6:"record";a:2:{s:4:"text";s:0:"";s:6:"format";s:1:"1";}s:6:"footer";a:2:{s:4:"text";s:0:"";s:6:"format";s:1:"1";}s:12:"submitbutton";s:15:"Guardar+cambios";}}s:4:"plot";a:1:{s:8:"elements";a:0:{}}}';
    $report->export = '';
    $report->jsordering = 1;
    $report->global = 0;
    $report->lastexecutiontime = 1;
    $report->cron = 0;
    $report->remote = 0;
    
    //delete if have report
    $exist = $DB->get_record_sql("SELECT mco_block_configurable_reports.id FROM mco_block_configurable_reports "
            . "WHERE mco_block_configurable_reports.courseid = {$course->id} AND mco_block_configurable_reports.NAME LIKE '%{$report->name}%'");
    if(count($exist) > 0)
        $DB->execute("DELETE FROM mco_block_configurable_reports WHERE mco_block_configurable_reports.id = {$exist->id}");
            
    //insert report
    $insert = $DB->insert_record('block_configurable_reports', $report);
    
    if($insert)
        echo "<div class='prom-box prom-box-info'><h3>Informe duplicado para el curso {$course->id}</h3></div>";
    else
        echo "<div class='prom-box prom-box-danger'><h3>Error al duplicar el informe para el curso {$course->id}</h3></div>";
    
}

echo $OUTPUT->footer();