<?php

require_once(dirname(__FILE__) . '/../../course/lib.php');

class courses {

    public function get() {
        global $DB;
        $sql = "SELECT course.id, course.shortname, course.visible, course.ean, CONCAT('" . BASE_URI . "course/get/', course.id) link FROM mco_course course JOIN mco_company_course com on com.courseid = course.id and com.companyid = ? ORDER BY course.shortname asc";

        $courses = $DB->get_records_sql($sql, array(COMPANY_ID));

        print(json_encode(array_values($courses)));
        exit();
    }

}

class course {

    public function get($id, $subaction) {
        global $DB;

        switch ($subaction) {
            case 'licenses':
                $sql = "SELECT l.* FROM mco_company_course c JOIN mco_companylicense l on l.companyid = c.companyid JOIN mco_companylicense_courses lc on lc.licenseid = l.id and lc.courseid = c.courseid WHERE c.companyid = ? AND c.courseid = ?";
                $licenses = $DB->get_records_sql($sql, array(COMPANY_ID, $id));
                print(json_encode(array_values($licenses)));
                break;
            default:
                $sql = "SELECT course.* FROM mco_course course JOIN mco_company_course com on com.courseid = course.id AND com.companyid = ? WHERE course.id = ?";
                $course = $DB->get_records_sql($sql, array(COMPANY_ID, $id));
                print(json_encode(array_values($course)));
        }
        exit();
    }

    public function getbyname() {
        global $DB;

        $data = json_decode(file_get_contents("php://input"));
        if (!$data || !isset($data->name)) {
            print("[]");
            exit();
        }
        $sql = "SELECT course.* FROM mco_course course JOIN mco_company_course com on com.courseid = course.id AND com.companyid = ? WHERE course.shortname like ?";
        $course = $DB->get_records_sql($sql, array(COMPANY_ID, "%" . $data->name . "%"));
        print(json_encode(array_values($course)));
        exit();
    }

    // Es necesario hacer las uniones mediante un left para poder obtener los cursos que nunca hubieran tenido alumnos
    public function getstudents($id) {
        global $DB;

        $sql = "SELECT ue.id iduserenrol, e.enrol, DATE_FORMAT(FROM_UNIXTIME(ue.timestart), '%d-%m-%Y') comienza, DATE_FORMAT(FROM_UNIXTIME(ue.timeend), '%d-%m-%Y') termina, 
						   u.id, u.username, u.firstname nombre, u.lastname apellidos, u.email
					FROM mco_course c
					JOIN mco_company co ON co.id = ?
					JOIN mco_company_course mc ON mc.courseid = c.id AND mc.companyid = co.id
					LEFT JOIN mco_enrol e ON e.courseid = c.id AND e.status = 0
					LEFT JOIN mco_role r ON r.id = e.roleid
					LEFT JOIN mco_user_enrolments ue ON ue.enrolid = e.id
					LEFT JOIN mco_user u ON u.id = ue.userid
					LEFT JOIN mco_company_users cu ON cu.companyid = co.id AND cu.userid = u.id AND cu.educator = 0 AND cu.managertype = 0
					WHERE c.id = ?";
        $users = $DB->get_records_sql($sql, array(COMPANY_ID, $id));
        print(json_encode(array_values($users)));
        exit();
    }

    // Esta función extrae los alumnos reales en un curso
    public function getrealstudents($id) {
        global $DB;

        $sql = "SELECT DISTINCT ue.id iduserenrol, e.enrol, DATE_FORMAT(FROM_UNIXTIME(ue.timestart), '%d-%m-%Y') comienza, DATE_FORMAT(FROM_UNIXTIME(ue.timeend), '%d-%m-%Y') termina, 
						   u.id, u.username, u.firstname nombre, u.lastname apellidos, u.email
					FROM mco_course c
					JOIN mco_company co ON co.id = ?
					JOIN mco_company_course mc ON mc.courseid = c.id AND mc.companyid = co.id
					JOIN mco_enrol e ON e.courseid = c.id AND e.status = 0
					JOIN mco_role r ON r.id = e.roleid
					JOIN mco_user_enrolments ue ON ue.enrolid = e.id
					JOIN mco_user u ON u.id = ue.userid and u.suspended = 0 and deleted = 0 and lower(firstname) <> 'demo'
					JOIN mco_company_users cu ON cu.companyid = co.id AND cu.userid = u.id AND cu.educator = 0 AND cu.managertype = 0
					JOIN mco_companylicense_users clu ON clu.userid = u.ID AND clu.licenseid <> 25 AND clu.licensecourseid = c.id
					JOIN mco_groups_members gm ON gm.userid = clu.userid
					JOIN mco_groups g ON g.courseid = c.id AND g.id = gm.groupid
					WHERE c.id = ? AND ((LOWER(c.shortname) NOT LIKE ('%básico%')) OR (LOWER(c.shortname) NOT LIKE ('%basico%')) OR (g.name = 'Curso'))";
        $users = $DB->get_records_sql($sql, array(COMPANY_ID, $id));
        print(json_encode(array_values($users)));
        exit();
    }

    public function getcourses_students() {
        global $DB;

        $sql = "SELECT a.id, 
						   a.shortname, 
						   a.fullname, 
						   a.ean, 
						   SUM(numalumnosvivos) numalumnosvivos, 
						   SUM(numalumnosmuertos) numalumnosmuertos,
						   (SELECT GROUP_CONCAT(DISTINCT CONCAT(u.firstname, ' ', u.lastname)) nombretutor
							FROM mco_course c 
							JOIN mco_company co ON co.id = ?
							JOIN mco_company_course mc ON mc.courseid = c.id AND mc.companyid = co.id
							JOIN mco_enrol e ON e.courseid = c.id
							JOIN mco_role r ON r.id = e.roleid
							JOIN mco_user_enrolments ue ON ue.enrolid = e.id
							JOIN mco_user u ON u.id = ue.userid
							JOIN mco_company_users cu ON cu.companyid = co.id AND cu.userid = u.id AND cu.educator = 1
							WHERE c.id = a.id) tutor
					FROM(
						SELECT c.id, c.shortname, c.fullname, c.ean, COUNT(DISTINCT ue.id) numalumnosvivos, 0 numalumnosmuertos
						FROM mco_course c
						JOIN mco_company co ON co.id = ?
						JOIN mco_company_course mc ON mc.courseid = c.id AND mc.companyid = co.id
						LEFT JOIN mco_enrol e ON e.courseid = c.id AND e.status = 0
						LEFT JOIN mco_role r ON r.id = e.roleid
						LEFT JOIN mco_user_enrolments ue ON ue.enrolid = e.id AND ue.timeend IS NOT NULL AND FROM_UNIXTIME(ue.timeend) >= DATE(SYSDATE())
						LEFT JOIN mco_user u ON u.id = ue.userid
						LEFT JOIN mco_company_users cu ON cu.companyid = co.id AND cu.userid = u.id AND cu.educator = 0 AND cu.managertype = 0
						GROUP BY c.id, c.fullname

						UNION

						SELECT c.id, c.shortname, c.fullname, c.ean, 0 numalumnosvivos, COUNT(DISTINCT ue.id) numalumnosmuertos
						FROM mco_course c
						JOIN mco_company co ON co.id = ?
						JOIN mco_company_course mc ON mc.courseid = c.id AND mc.companyid = co.id
						LEFT JOIN mco_enrol e ON e.courseid = c.id AND e.status = 0 
						LEFT JOIN mco_role r ON r.id = e.roleid
						LEFT JOIN mco_user_enrolments ue ON ue.enrolid = e.id AND ue.timeend IS NOT NULL AND FROM_UNIXTIME(ue.timeend) < DATE(SYSDATE())
						LEFT JOIN mco_user u ON u.id = ue.userid
						LEFT JOIN mco_company_users cu ON cu.companyid = co.id AND cu.userid = u.id AND cu.educator = 0 AND cu.managertype = 0
						GROUP BY c.id, c.fullname
					) a
					GROUP BY a.id, a.shortname, a.fullname, a.ean
					";
        $courses_students = $DB->get_records_sql($sql, array(COMPANY_ID, COMPANY_ID, COMPANY_ID));
        print(json_encode(array_values($courses_students)));
        exit();
    }

    public function get_coursesbyteacher($id) {
        global $DB;

        $sql = "SELECT DISTINCT c.id, 
									c.shortname,
									c.fullname,
									(SELECT COUNT(DISTINCT u2.id) numalumnos
									FROM mco_course c2 
									JOIN mco_company co2 ON co2.id = ?
									JOIN mco_company_course mc2 ON mc2.courseid = c2.id AND mc2.companyid = co2.id
									JOIN mco_enrol e2 ON e2.courseid = c2.id and e2.status = 0
									JOIN mco_role r2 ON r2.id = e2.roleid
									JOIN mco_user_enrolments ue2 ON ue2.enrolid = e2.id AND ue2.timeend IS NOT NULL AND FROM_UNIXTIME(ue2.timeend) >= DATE(SYSDATE())
									JOIN mco_user u2 ON u2.id = ue2.userid and u2.suspended = 0 and u2.deleted = 0
									JOIN mco_company_users cu2 ON cu2.companyid = co2.id AND cu2.userid = u2.id AND cu2.educator = 0 AND cu2.managertype = 0
									JOIN mco_companylicense_users clu ON clu.userid = u2.ID AND clu.licenseid <> 25 AND clu.licensecourseid = c2.id
									WHERE c2.ID = c.id
									) numalumnos,
									c.ean
					FROM mco_course c 
					JOIN mco_company co ON co.id = ?
					JOIN mco_company_course mc ON mc.courseid = c.id AND mc.companyid = co.id
					JOIN mco_enrol e ON e.courseid = c.id and e.status = 0
					JOIN mco_role r ON r.id = e.roleid
					JOIN mco_user_enrolments ue ON ue.enrolid = e.id AND ue.timeend IS NOT NULL AND ue.status = 0
					JOIN mco_user u ON u.id = ue.userid AND u.suspended = 0 AND u.deleted = 0 and lower(firstname) <> 'demo'
					JOIN mco_company_users cu ON cu.companyid = co.id AND cu.userid = u.id AND cu.educator = 1
					JOIN mco_companylicense_users clu ON clu.userid = u.ID AND clu.licenseid <> 25 AND clu.licensecourseid = c.id
					WHERE c.visible = 1 AND e.status = 0 AND u.ID = ?";
        $courses = $DB->get_records_sql($sql, array(COMPANY_ID, COMPANY_ID, $id));
        print(json_encode(array_values($courses)));
        exit();
    }

    public function get_namecoursesbyteacher($id) {
        global $DB;

        $sql = "SELECT DISTINCT c.id, 
									c.shortname,
									c.fullname,
									c.ean
					FROM mco_course c 
					JOIN mco_company co ON co.id = ?
					JOIN mco_company_course mc ON mc.courseid = c.id AND mc.companyid = co.id
					JOIN mco_enrol e ON e.courseid = c.id and e.status = 0
					JOIN mco_role r ON r.id = e.roleid
					JOIN mco_user_enrolments ue ON ue.enrolid = e.id AND ue.timeend IS NOT NULL AND FROM_UNIXTIME(ue.timeend) >= DATE(SYSDATE()) AND ue.status = 0
					JOIN mco_user u ON u.id = ue.userid AND u.suspended = 0 and lower(firstname) <> 'demo'
					JOIN mco_company_users cu ON cu.companyid = co.id AND cu.userid = u.id AND cu.educator = 1
					JOIN mco_companylicense_users clu ON clu.userid = u.ID AND clu.licenseid <> 25 AND clu.licensecourseid = c.ID
					WHERE c.visible = 1 AND e.status = 0 AND u.ID = ?";
        $courses = $DB->get_records_sql($sql, array(COMPANY_ID, $id));
        print(json_encode(array_values($courses)));
        exit();
    }

    public function get_studentsbyteacher($id) {
        global $DB;

        $sql = "SELECT DISTINCT ue.id iduserenrol, 
									e.enrol, 
									DATE_FORMAT(FROM_UNIXTIME(ue.timestart), '%d-%m-%Y') comienza, 
									DATE_FORMAT(FROM_UNIXTIME(ue.timeend), '%d-%m-%Y') termina, 
									u.id, 
									u.username, 
									u.firstname nombre, 
									u.lastname apellidos, 
									u.email
					FROM mco_course c
					JOIN mco_company co ON co.id = ?
					JOIN mco_company_course mc ON mc.courseid = c.id AND mc.companyid = co.id
					JOIN mco_enrol e ON e.courseid = c.id AND e.status = 0
					JOIN mco_role r ON r.id = e.roleid
					JOIN mco_user_enrolments ue ON ue.enrolid = e.id
					JOIN mco_user u ON u.id = ue.userid and u.suspended = 0 and u.deleted = 0 and lower(firstname) <> 'demo'
					JOIN mco_company_users cu ON cu.companyid = co.id AND cu.userid = u.id AND cu.educator = 0 AND cu.managertype = 0
					JOIN mco_companylicense_users clu ON clu.userid = u.ID AND clu.licenseid <> 25 AND clu.licensecourseid = c.ID
					WHERE c.ID IN (SELECT DISTINCT c.id
										FROM mco_course c
										JOIN mco_company_course cc ON cc.courseid = c.id
										JOIN mco_companylicense_courses clc ON clc.courseid = c.id
										JOIN mco_companylicense_users clu ON clu.licensecourseid = c.id AND clu.licenseid <> 25 AND clu.licensecourseid = c.ID
										JOIN mco_groups_members gm ON gm.userid = clu.userid
										JOIN mco_groups g ON g.courseid = c.id AND g.id = gm.groupid
										WHERE c.visible = 1
										  AND ((LOWER(c.shortname) NOT LIKE ('%básico%')) OR (LOWER(c.shortname) NOT LIKE ('%basico%')) OR (g.name = 'Curso'))
										  AND cc.companyid = ?
										  AND clu.userid IN (SELECT cu.userid 
																	  FROM mco_company_users cu
																	 WHERE cu.educator = 1 
																	   AND cu.userid = ?))";
        $users = $DB->get_records_sql($sql, array(COMPANY_ID, COMPANY_ID, $id));
        print(json_encode(array_values($users)));
        exit();
    }

    public function create() {
        global $DB;
        global $CFG;

        $CFG->debug = (E_ALL | E_STRICT);
        $CFG->debugdisplay = 1;

        $str = file_get_contents("php://input");
        //file_put_contents("log/create_course.log", $str, FILE_APPEND);

        $data = json_decode($str);


        $editoroptions = array('maxfiles' => EDITOR_UNLIMITED_FILES, 'maxbytes' => $CFG->maxbytes, 'trusttext' => false, 'noclean' => true);

        $isupdate = false;

        if (isset($data->id)) {
            $coursedata = get_course($data->id);

            if ($coursedata)
                $isupdate = true;
        }
        if (!$coursedata) {
            $coursedata = new stdClass;
            $isupdate = false;
            $coursedata->timecreated = time();
            $courseconfig = get_config('moodlecourse');
            $coursedata = (object) array_merge((array) $courseconfig, (array) $coursedata);
            $coursedata->restrictmodules = 1;
            $coursedata->summaryformat = 1;
            $coursedata->format = "buttons";
        }

        $coursedata->fullname = $data->fullname;
        $coursedata->shortname = $data->shortname;
        $coursedata->ean = $data->ean;
        $coursedata->timemodified = time();
        $coursedata->enddate = $coursedata->startdate = 0;

        $company = $DB->get_record('company', array('id' => COMPANY_ID));

        if (!empty($company->category)) {
            $coursedata->category = $company->category;
        } else {
            $coursedata->category = $CFG->defaultrequestcategory;
        }


        if (!$isupdate) {
            if (!$course = create_course($coursedata, $editoroptions)) {
                //error al insertar curso
                print(json_encode(array("error" => true, "errstr" => "Error al crear el curso")));
                die();
            }
        } else {
            //print(json_encode($coursedata)); exit();
            //print(json_encode($coursedata)); exit();
            update_course($coursedata);
            $course = $coursedata;
        }

        //para acceder al curso hace falta una licencia:
        if ($enrolls = $DB->get_records('enrol', array('courseid' => $course->id))) {
            foreach ($enrolls as $enrol) {
                $update = (array) $enrol;
                if ($enrol->enrol == 'self') {
                    $update['status'] = 1;
                } else if ($enrol->enrol == 'license') {
                    $update['status'] = 0;
                } else if ($enrol->enrol == 'manual') {
                    $update['status'] = 1;
                }
                $DB->update_record('enrol', $update);
            }
        }

        //asociar el curso con la empresa:
        $company = new company(COMPANY_ID);
        $company->add_course($course, 0, false, true);

        print(json_encode($course));
    }

    public function addlicenses() {
        global $DB;
        global $CFG;

        $data = json_decode(file_get_contents("php://input"));



        if ($data && isset($data->id) && isset($data->licenses) && !empty($data->licenses)) {
            if ($data->delete_existing) {
                $DB->delete_records('companylicense_courses', array('courseid' => $data->id));
            }

            $lics = explode(":", $data->licenses);

            if (is_array($lics)) {
                foreach ($lics as $lic) {
                    $DB->insert_record('companylicense_courses', array('licenseid' => $lic, 'courseid' => $data->id));
                }
            }

            print(json_encode(array("done" => true)));
        }
    }

    public function removelicenses() {
        global $DB;
        global $CFG;

        $data = json_decode(file_get_contents("php://input"));


        if ($data && isset($data->id) && isset($data->licenses) && !empty($data->licenses)) {
            $lics = explode(":", $data->licenses);

            if (is_array($lics)) {
                foreach ($lics as $lic) {
                    $DB->delete_records('companylicense_courses', array('licenseid' => $lic, 'courseid' => $data->id));
                }
            }

            print(json_encode(array("done" => true)));
        }
    }

    /**
     * Get teacher by course
     * @global type $DB
     * @param type $course
     */
    public function get_teachersbycourse($course) {
        global $DB;

        $sql = "SELECT mco_user.id
                            FROM mco_user_enrolments
                            JOIN mco_user ON mco_user_enrolments.userid = mco_user.id
                            JOIN mco_company_users ON mco_company_users.userid = mco_user_enrolments.userid
                            JOIN mco_enrol ON mco_enrol.id = mco_user_enrolments.enrolid
                            WHERE mco_company_users.educator = 1 AND mco_enrol.courseid = ?";
        $users = $DB->get_records_sql($sql, array($course));
        print(json_encode(array_values($users)));
        exit();
    }

    /*
     * Get all courses and teachers
     */

    public function get_coursesandteachers() {
        global $DB;

        $sql = "SELECT DISTINCT mco_course.id AS COURSE, mco_user.id AS TEACHER
            FROM mco_user_enrolments
            JOIN mco_user ON mco_user_enrolments.userid = mco_user.id
            JOIN mco_company_users ON mco_company_users.userid = mco_user_enrolments.userid
            JOIN mco_enrol ON mco_enrol.id = mco_user_enrolments.enrolid
            JOIN mco_course ON mco_course.id = mco_enrol.courseid
            WHERE mco_company_users.educator = 1 ";
        $data = $DB->get_records_sql($sql, array());
        print(json_encode(array_values($data)));
        exit();
    }

    /**
     * Assign teacher on a course
     * @param type $course
     * @param type $teacher
     */
    public function assign_teacher() {
        global $DB;

        $data = json_decode(file_get_contents("php://input"));

        //comprobamos si existe la matriculación manual
        //existe manual
        $enrol = $DB->get_record_sql("SELECT mco_enrol.id FROM mco_enrol WHERE mco_enrol.courseid = {$data->courseid} AND enrol = 'manual'");

        if (empty($enrol)) {
            $manual = new stdClass();

            $manual->enrol = '';
            $manual->status = '';
            $manual->courseid = '';
            $manual->sortorder = '';
            $manual->enrolperiod = '';
            $manual->enrolstartdate = '';
            $manual->enrolenddate = '';
            $manual->expirynotify = '';
            $manual->expirythreshold = '';
            $manual->notifyall = '';
            $manual->roleid = 5;
            $manual->timecreated = time();
            $manual->timemodified = time();

            try {
                $transaction = $DB->start_delegated_transaction();

                $DB->insert_record('mco_enrol', $manual);
                $transaction->allow_commit();
            } catch (Exception $e) {
                $transaction->rollback($e);
            }
        } else {
            //enroled teacher
            $enrols = $DB->get_records_sql("SELECT * "
                    . "FROM mco_user_enrolments "
                    . "JOIN mco_enrol ON mco_enrol.id = mco_user_enrolments.enrolid "
                    . "JOIN mco_company_users ON mco_company_users.userid = mco_user_enrolments.userid "
                    . "WHERE mco_enrol.courseid = {$data->courseid} AND mco_user_enrolments.userid = {$data->userid}");

            if (count($enrols) == 0) {
                //assign teacher
                $info = new stdClass();

                $info->status = 0;
                $info->enrolid = $enrol->id;
                $info->userid = $data->userid;
                $info->timestart = time();
                $info->timeend = 0;
                $info->modifierid = 2;
                $info->timecreated = time();
                $info->timemodified = time();

                try {
                    $transaction = $DB->start_delegated_transaction();

                    $DB->insert_record('user_enrolments', $info);
                    $transaction->allow_commit();
                } catch (Exception $e) {
                    $transaction->rollback($e);
                }
            }

            //necesitamos el id del context 50 (curso)
            $context = $DB->get_record_sql("SELECT * FROM mco_context WHERE mco_context.instanceid = {$data->courseid} AND mco_context.contextlevel = 50");

            //si no está matriculado la insertamos con el rol de profesor
            $role_ass = $DB->get_records_sql("SELECT * FROM mco_role_assignments WHERE mco_role_assignments.contextid = {$context->id} AND mco_role_assignments.userid = {$data->userid}");

            if (count($role_ass) == 0) {
                //insert en mco_role_assignments
                $role_assigments = new stdClass();

                $role_assigments->roleid = 3;
                $role_assigments->contextid = $context->id;
                $role_assigments->userid = $data->userid;
                $role_assigments->timemodified = time();
                $role_assigments->modifierid = 2;
                $role_assigments->component = '';
                $role_assigments->itemid = 0;
                $role_assigments->sortorder = 0;

                try {
                    $transaction = $DB->start_delegated_transaction();

                    $DB->insert_record('role_assignments', $role_assigments);
                    $transaction->allow_commit();
                } catch (Exception $e) {
                    $transaction->rollback($e);
                }
            }
        }
    }

    /**
     * remove teacher from a course
     * @global type $DB
     */
    public function remove_teacher() {
        global $DB;

        $data = json_decode(file_get_contents("php://input"));

        //necesitamos el id del context 50 (curso)
        $context = $DB->get_record_sql("SELECT * FROM mco_context WHERE mco_context.instanceid = {$data->courseid} AND mco_context.contextlevel = 50");
        //nos traemos el enrol
        $enrol = $DB->get_record_sql("SELECT mco_enrol.id FROM mco_enrol WHERE mco_enrol.courseid = {$data->courseid} AND enrol = 'manual'");

        if ($data && isset($data->userid) && isset($data->courseid)) {
            //eliminamos de la tabla role_assignments
            $DB->delete_records('role_assignments', array('contextid' => $context->id, 'userid' => $data->userid));

            //eliminamos de user_enrolments
            $DB->delete_records('user_enrolments', array('enrolid' => $enrol->id, 'userid' => $data->userid));

            print(json_encode(array("done" => true)));
        }
    }

    public function insert_teacher() {
        global $DB;

        $data = json_decode(file_get_contents("php://input"));

        print(json_encode($data));

        $user = new stdClass();
        $company_user = new stdClass();

        //user
        $user->auth = "manual";
        $user->confirmed = 1;
        $user->policyagreed = 0;
        $user->deleted = 0;
        $user->suspended = 0;
        $user->mnethostid = 1;
        $user->username = $data->email;
        $user->password = '$2y$10$w59j/lccTgSs9wHGlf3HmOmX8cS.qrD/Ex3KlixbwWbQDmhxu7Kx.'; //Editorialcep_2019
        $user->idnumber = "";
        $user->firstname = $data->firstname;
        $user->lastname = $data->lastname;
        $user->email = $data->email;
        $user->emailstop = 0;
        $user->country = "ES";
        $user->lang = "es";
        $user->calendartype = "gregorian";
        $user->timezone = 99;
        $user->descriptionformat = 1;
        $user->mailformat = 1;
        $user->maildigest = 0;
        $user->maildisplay = 0;
        $user->autosubscribe = 1;
        $user->trackforums = 0;
        $user->trustbitmask = 0;
        $user->idcloudia = $data->idcloudia;

        try {
            $transaction = $DB->start_delegated_transaction();
            $userid = $DB->insert_record('user', $user, $returnid = TRUE);

            //company_user
            $company_user->companyid = 11; //editorial cep
            $company_user->userid = $userid;
            $company_user->managertype = 0;
            $company_user->departmentid = 17; //cep
            $company_user->suspended = 0;
            $company_user->educator = 1;

            $DB->insert_record('company_users', $company_user);
            $transaction->allow_commit();
        } catch (Exception $e) {
            $transaction->rollback($e);
        }
    }

    /**
     * remove teacher from moodle
     * @global type $DB
     */
    public function remove_teacher_moodle() {
        global $DB;

        $data = json_decode(file_get_contents("php://input"));
        print(json_encode($data));
        
        if ($data && isset($data->userid)) {
            //nos traemos todos los cursos de ese usuario
            $enrolments = $DB->get_records_sql("SELECT * FROM mco_user_enrolments WHERE userid = {$data->userid}");
            $role_assigments = $DB->get_records_sql("SELECT * FROM mco_role_assignments WHERE userid = {$data->userid}");
            
            //roles en las matriculaciones
            if (count($role_assigments) > 0) {
                foreach ($role_assigments as $role_assigment) {
                    $DB->execute("DELETE FROM mco_role_assignments WHERE mco_role_assignments.id = {$role_assigment->id}");
                }
            }
            
            //matriculaciones
            if (count($enrolments) > 0) {
                foreach ($enrolments as $enrolment) {
                    $DB->execute("DELETE FROM mco_user_enrolments WHERE mco_user_enrolments.id = {$enrolment->id}");
                }
            }

            //eliminamos de la tabla company_users
            $DB->delete_records('company_users', array('userid' => $data->userid));

            //eliminamos de la tabla user
            $DB->delete_records('user', array('id' => $data->userid));

            print(json_encode(array("done" => true)));
        }
    }

    public function getteacherbyid($teacher) {
        global $DB;

        $user = $DB->get_record_sql("SELECT mco_user.id FROM mco_user WHERE mco_user.idcloudia = {$teacher}");

        print(json_encode($user));
        exit();
    }

}

?>
