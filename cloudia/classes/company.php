<?php
	class compania{
		function get_by_cif($cif){
			global $DB;
			$company = $DB->get_record('company', array('shortname'=>$cif));
			if($company){
				print(json_encode(array("id"=>(int)$company->id)));
			}else{
				print(json_encode(array("id"=>null)));
			}
		}
		
		//funcion para comprobar si una compañia (parametro id) tiene una licencia relacionada con otra licencia de cep (parametro licenseid)
		function get_related_license($id, $licenseid, $tutorization){
			global $DB;
			$lic = $DB->get_record('companylicense', array('companyid' => $id, 'ceplicense' => $licenseid, 'tutorizacion' => $tutorization));
			if($lic){
				print(json_encode(array("id"=>(int)$lic->id)));
			}else{
				print(json_encode(array("id"=>null)));
			}
		}
		
		
		public function add_related_license_and_course(){
			global $DB;
			global $CFG;
			
			$data = json_decode(file_get_contents("php://input"));
			
			//comprueba si ya existe una licencia relacionada para esta compañia:
			$rel = $DB->get_record("companylicense", array("companyid"=>$data->companyid, "ceplicense"=>$data->orig_licenseid));
			//si no existe, la creamos:
			if(!$rel){
				//buscamos la informacion de la licencia original:
				$orig = $DB->get_record("companylicense", array("id"=>$data->orig_licenseid));
				//creamos la licencia relacionada:
				$licenseid = $DB->insert_record("companylicense", array(
					"name"=>$orig->name, 
					"allocation"=>$orig->allocation, 
					"validlength"=>$orig->validlength, 
					"startdate"=>time(), 
					"expirydate"=>time()+86400*3660, 
					"used"=>0, 
					"companyid"=>$data->companyid, 
					"parentid"=>0, 
					"type"=>0, 
					"program"=>0, 
					"reference"=>$orig->reference, 
					"instant"=>1, 
					"ceplicense"=>$orig->id, 
					"tutorizacion"=>$orig->tutorizacion
				));
			}else{
				$licenseid = $rel->id;
			}
			
			//miramos si el curso existe en la compañia:
			$dep = $DB->get_record("department", array("company"=>$data->companyid));
			$exists = $DB->get_record('company_course', array("companyid"=>$data->companyid, "courseid"=>$data->courseid));
			if(!$exists){
				//si no existe, lo asignamos a la compañia:
				$DB->insert_record('company_course', array("companyid"=>$data->companyid, "courseid"=>$data->courseid, "departmentid"=>isset($dep)?$dep->id:null, "autoenrol"=>0));
			}else{
				
			}
			if($licenseid){
				//creamos la relacion entre compañia, licencia y curso:
				$info = array("licenseid"=>$licenseid, "courseid"=>$data->courseid);
				$rec = $DB->get_record('companylicense_courses', $info);
				var_dump($rec);
				if(!$rec || !isset($rec->id)){
					$DB->insert_record('companylicense_courses', $info);
				}
			}
			
			print(json_encode(array("done"=>true)));
		}
		
		
		function companycourse_add_slots(){
			
			$done = false;
			$data = json_decode(file_get_contents("php://input"));
			
			
			global $DB;

			$clc = $DB->get_record('company_course_slots', array('companyid' => $data->companyid, 'courseid' => $data->courseid, 'licenseid' => $data->licenseid, 'tutorized' => $data->tutorized));
			
			if($clc){
				$clc->slots = (int)$clc->slots + (int)$data->slots;
				$DB->update_record('company_course_slots', $clc);
			}else{
				$clc = new stdClass;
				$clc->companyid = $data->companyid;
				$clc->courseid = $data->courseid;
				$clc->licenseid = $data->licenseid;
				$clc->tutorized = $data->tutorized;
				$clc->slots = (int)$data->slots;
				$DB->insert_record('company_course_slots', (array)$clc);
			}
			print(json_encode(array("done"=>true)));
		}
		
		// funcion para comprobar si una licencia perteneciente a determinada compania esta asociada a un curso:
		function companycourse_license_is_valid($companyid, $licenseid, $courseid){
			global $DB;
			$sql = "SELECT COUNT(clc.id) c from mco_companylicense cl JOIN mco_companylicense_courses clc ON clc.licenseid = cl.id WHERE cl.companyid = ? AND clc.courseid = ? AND clc.licenseid = ?";
			$rel = $DB->get_record_sql($sql, array($companyid, $courseid, $licenseid));
			
			if($rel && $rel->c > 0){
				$ret = true;
			}else{
				$ret = false;
			}
			
			print(json_encode(array("valid"=>$ret)));
		}
		
		// funcion para comprobar los cursos a liquidar
		public function getstudentsbycompany($id, $fechadesde, $fechahasta){
			global $DB;
			$sql = "SELECT DISTINCT CONCAT(u.id,c.id,cl2.id) id, c.id courseid, c.fullname, u.username, u.email, cl2.id licenseid, 
						   FROM_UNIXTIME(MAX(lsl.timecreated), '%d/%m/%Y') fechaevento, FROM_UNIXTIME(ue.timestart, '%d/%m/%Y') fechainicio, FROM_UNIXTIME(ue.timeend, '%d/%m/%Y') fechafin 
					FROM mco_course c 
					JOIN mco_company_course mc ON mc.courseid = c.id AND mc.companyid = ?
					JOIN mco_enrol e ON e.courseid = c.id and e.status = 0
					JOIN mco_role r ON r.id = e.roleid
					JOIN mco_user_enrolments ue ON ue.enrolid = e.id AND ue.timeend IS NOT NULL 
					JOIN mco_user u ON u.id = ue.userid AND u.suspended = 0 AND u.deleted = 0
					JOIN mco_company_users cu ON cu.companyid = mc.companyid AND cu.userid = u.id
					JOIN mco_companylicense_users clu ON clu.userid = u.ID AND clu.licensecourseid = c.id
					JOIN mco_companylicense cl ON cl.id = clu.licenseid
					JOIN mco_companylicense cl2 ON cl2.id = cl.ceplicense
					JOIN mco_logstore_standard_log lsl ON lsl.userid = u.id AND lsl.courseid = c.id
					WHERE lsl.timecreated between ? and ?
					GROUP BY u.id, c.id, cl2.id, c.fullname, u.username
					ORDER BY c.id, u.id, lsl.timecreated";
			$students = $DB->get_records_sql($sql, array($id, $fechadesde, $fechahasta));
			print(json_encode(array_values($students)));
			exit();
		}
		
		// todos los clientes-empresas
		public function getcompanies() {
			global $DB;
			$sql = "SELECT DISTINCT id, shortname cif, name FROM mco_company WHERE suspended = 0";
			$companies = $DB->get_records_sql($sql);
			print(json_encode(array_values($companies)));
			exit();
		}
	}
?>