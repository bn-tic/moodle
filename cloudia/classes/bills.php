<?php

class bills {

    public function get_rm_courses($id, $start, $end) {
        global $DB;
        
        $condition = "";
                
        switch ($id){
			//marquero , rgarcia y fran sevilla
            case 5194:
            case 5195:
            case 13809:
                $condition = "AND(
                    #MES COMPLETO
                     (mco_user_enrolments.timestart < $start
                    AND mco_user_enrolments.timeend  >= $end)

                    #FINALIZADO ANTES DE FINAL DE MES
                    OR (mco_user_enrolments.timeend > $start
                    AND mco_user_enrolments.timeend  <= $end)

                    #INICIADO ANTES DE FINAL DE MES
                    OR (mco_user_enrolments.timestart > $start
                    AND mco_user_enrolments.timestart  < $end)
                )";
                break;
            
            //Paco correos
            case 6659:
                $condition = "AND mco_user_enrolments.timestart >= $start "
                    . "AND mco_user_enrolments.timestart  <= $end";
                break;
        }

        $sql = "SELECT DISTINCT mco_user.id as IDALUMNO, mco_course.ean as EAN, mco_course.id as IDCURSO, mco_course.fullname AS CURSO, mco_user.email AS EMAIL, mco_user.firstname AS NOMBRE, mco_user.lastname AS APELLIDOS, FROM_UNIXTIME(mco_user_enrolments.timestart) AS INICIO, 
                FROM_UNIXTIME(mco_user_enrolments.timeend) AS FIN, mco_companylicense_users.licenseid AS IDLICENCIAALUMNO
                FROM mco_course
                INNER JOIN mco_enrol ON mco_enrol.courseid = mco_course.id
                INNER JOIN mco_user_enrolments ON mco_enrol.id = mco_user_enrolments.enrolid
                INNER JOIN mco_user ON mco_user.id = mco_user_enrolments.userid
                INNER JOIN mco_company_users ON mco_company_users.userid = mco_user.id AND mco_company_users.companyid = ?
                INNER JOIN mco_companylicense_users ON mco_companylicense_users.userid = mco_user.id
                INNER JOIN mco_groups_members ON mco_groups_members.userid = mco_companylicense_users.userid
                INNER JOIN mco_groups ON mco_groups.courseid = mco_course.id AND mco_groups.id = mco_groups_members.groupid
                INNER JOIN mco_companylicense ON mco_companylicense.id = mco_companylicense_users.licenseid
                WHERE mco_course.visible = 1
                AND mco_companylicense_users.licensecourseid = mco_course.id
                AND mco_companylicense_users.licenseid NOT IN (25, 31, 38)
                AND mco_enrol.status = 0
                AND mco_user.deleted = 0
                #AND mco_course.fullname NOT LIKE '%basico.%'
                AND LOWER(mco_user.email) NOT LIKE ('%orbeformacion%') AND LOWER(mco_user.email) NOT LIKE ('%editorialcep%')
                $condition
                AND mco_course.id IN (SELECT mco_course.id
		FROM mco_course
		INNER JOIN mco_enrol ON mco_enrol.courseid = mco_course.id
		INNER JOIN mco_user_enrolments ON mco_enrol.id = mco_user_enrolments.enrolid
		INNER JOIN mco_user ON mco_user.id = mco_user_enrolments.userid
		WHERE mco_course.visible = 1
		AND mco_user.id = $id)
                AND mco_company_users.educator = 0
                ORDER BY mco_course.fullname, inicio";
        //print($sql);
        $courses = $DB->get_records_sql($sql, array(COMPANY_ID, $id, $start, $end));
        print(json_encode(array_values($courses)));
        exit();
    }

}

?>