<?php
	class users {
		public function get(){
			global $DB;
			
			$sql = "SELECT u.id, u.confirmed, u.policyagreed, u.deleted, u.suspended, u.firstname, u.lastname, u.username, u.email, u.idcloudia FROM mco_user u JOIN mco_company_users cu ON cu.companyid = ? AND cu.userid = u.id ORDER BY u.id ASC";
			
			$users = $DB->get_records_sql($sql, array(COMPANY_ID));
			
			print(json_encode(array_values($users)));
			exit();
		}
                
                public function last_enrolments(){
                    global $DB;
                    
                    $users = $DB->get_records_sql("SELECT u.id, u.firstname, u.lastname, u.username, u.email, FROM_UNIXTIME(ue.timestart) AS timestart, FROM_UNIXTIME(ue.timeend) AS timeend, c.fullname AS course
                        FROM mco_companylicense_users clu 
                        JOIN mco_user u ON u.id = clu.userid 
                        JOIN mco_company_users cu on cu.userid = u.id 
                        JOIN mco_companylicense_courses clc ON clc.licenseid = clu.licenseid and clc.courseid = clu.licensecourseid 
                        JOIN mco_user_enrolments ue ON ue.userid = u.id
                        JOIN mco_enrol e ON e.id = ue.enrolid AND e.courseid = clc.courseid
                        JOIN mco_course c ON c.id = clc.courseid 
                        WHERE cu.educator != 1  AND cu.companyid = ? AND ue.timestart >= UNIX_TIMESTAMP(date_add(NOW(), INTERVAL -30 DAY)) AND e.status <> 1 ORDER BY timestart DESC;" , array(COMPANY_ID));
			
                    print(json_encode(array_values($users)));
                    exit();
                }
	}
	
	class user {
		private $stdfields = array('id', 'firstname', 'lastname', 'username', 'email',
        'city', 'country', 'lang', 'auth', 'timezone', 'mailformat',
        'maildisplay', 'maildigest', 'htmleditor', 'ajax', 'autosubscribe',
        'mnethostid', 'institution', 'department', 'idnumber', 'skype',
        'msn', 'aim', 'yahoo', 'icq', 'phone1', 'phone2', 'address',
        'url', 'description', 'descriptionformat', 'oldusername', 'deleted',
        'password', 'temppassword', 'suspended');
		
		public function get($id, $subaction){
			global $DB;
			
			switch($subaction){
				case 'enrols':
					$sql = "SELECT e.id enrolid, u.id userid, c.id courseid, clu.licenseid, ue.timestart timestart, ue.timeend
								FROM mco_companylicense_users clu 
								JOIN mco_user u ON u.id = clu.userid 
								JOIN mco_company_users cu on cu.userid = u.id 
								JOIN mco_companylicense_courses clc ON clc.licenseid = clu.licenseid and clc.courseid = clu.licensecourseid 
								JOIN mco_user_enrolments ue ON ue.userid = u.id
								JOIN mco_enrol e ON e.id = ue.enrolid AND e.courseid = clc.courseid
								JOIN mco_course c ON c.id = clc.courseid 
							WHERE cu.companyid = ? AND u.id = ? AND e.status <> 1 ";
					$licenses = $DB->get_records_sql($sql, array(COMPANY_ID, $id));
					print(json_encode(array_values($licenses)));
					break;
				default:
					$sql = "SELECT u.id, u.confirmed, u.policyagreed, u.deleted, u.suspended, u.firstname, u.lastname, u.username, u.email, u.idcloudia FROM mco_user u JOIN mco_company_users cu ON cu.companyid = ? AND cu.userid = u.id WHERE u.id = ?";
					$user = $DB->get_records_sql($sql, array(COMPANY_ID, $id));
					
					print(json_encode(array_values($user)));
					exit();
			}
		}
		
		public function getbyusername($username){
			global $DB;
			global $CFG;
			$existinguser = $DB->get_record('user', array('username' => $username, 'mnethostid' => $CFG->mnet_localhost_id));
			print(json_encode($existinguser));
			exit();
		}
		
		public function getlastaccess($iduser, $idcourse) {
			global $DB;
			$sql = "SELECT DATE_FORMAT(FROM_UNIXTIME(timeaccess), '%d-%m-%Y') ultimoacceso, 
						   userid, 
						   courseid 
					FROM mco_user_lastaccess
					WHERE userid = ? AND courseid = ?";
			$lastaccess = $DB->get_records_sql($sql, array($iduser, $idcourse));
			print(json_encode(array_values($lastaccess)));
			exit();
		}
		
		public function create(){
			global $DB;
			global $CFG;
			
			$data = json_decode(file_get_contents("php://input"));
			
			if($data){
				$company = new company(COMPANY_ID);
				
				$user = new stdClass;
				$user->mnethostid = $CFG->mnet_localhost_id;
				$user->username = $data->username;
				$user->firstname = $data->firstname;
				$user->lastname = $data->lastname;
				$user->password = $data->password;
				$user->idcloudia = $data->idcloudia;
				$user->email = $data->email;
				
				$user->username = clean_param($user->username, PARAM_USERNAME);
				
				$existinguser = $DB->get_record('user', array('username' => $user->username, 'mnethostid' => $user->mnethostid));
				
				if($existinguser){
					$user->id = $existinguser->id;
					$user = (object) array_merge((array)$defaults, (array)$user);
					$user->password = hash_internal_user_password($user->password);
					$DB->update_record('user', $user);
				}else{
					$user->confirmed = 1;
					$user->timemodified = time();
					$user->timecreated = time();
					$user->password = hash_internal_user_password($user->password);
					
					$defaults = $company->get_user_defaults();
					$user = (object) array_merge((array)$defaults, (array)$user);
					
					$user->id = $DB->insert_record('user', $user);
				}
				
				$DB->set_field('user', 'idcloudia', $user->idcloudia, array("id"=>$user->id));
				
				set_user_preference('auth_forcepasswordchange', 0, $user->id);
				
				profile_save_data($user);
				
				$company->assign_user_to_company($user->id);
				
				\core\event\user_created::create_from_userid($user->id)->trigger();
				
				print(json_encode($user));
			}
			exit();
		}
		
		public function enrol(){
			global $DB;
			global $CFG;
			
			$data = json_decode(file_get_contents("php://input"));
			/*
			$data = new stdClass;
			$data->licenseid = 14;	//suscripcion 1 mes
			$data->courseid = 13;	//SAS Premium
			$data->userid = 5176;	//mrpotato
			*/
			
			if($data){
				if(!isset($data->licenseid)){
					header("HTTP/1.1 400 Bad Request");
					print(json_encode(array("error"=>true, "errcode"=>400, "errstr"=>"Licencia no especificada. Datos recibidos: " . json_encode($data))));
					exit();
					//license not specified
				}
				if(!isset($data->courseid)){
					header("HTTP/1.1 400 Bad Request");
					print(json_encode(array("error"=>true, "errcode"=>400, "errstr"=>"Curso no especificado. Datos recibidos: " . json_encode($data))));
					exit();
					//course not specified
				}
				if(!isset($data->userid)){
					header("HTTP/1.1 400 Bad Request");
					print(json_encode(array("error"=>true, "errcode"=>400, "errstr"=>"Usuario no especificado. Datos recibidos: " . json_encode($data))));
					exit();
					//user not specified
				}
				
				if(isset($data->userid)){
					try{
						$company = new company(COMPANY_ID);
						$company->assign_user_to_company($data->userid);
					}catch(Exception $e){;}
				}
				
				$license = $DB->get_record('companylicense', array("id"=>$data->licenseid));
				
				//$courses = company::get_courses_by_license($license->id);
				
				//si hay licencias disponibles:
				if($license->allocation - $license->used > 0){
					
					$recordarray = array(
						'licensecourseid' => $data->courseid,
						'userid' => $data->userid,
						'licenseid' => $data->licenseid
					);
					
					if($DB->get_record('companylicense_users', $recordarray)){
						$DB->delete_records('companylicense_users', $recordarray);
					}
					
					if(!$DB->get_record('companylicense_users', $recordarray)){
						$recordarray['issuedate'] = time();
						$recordarray['id'] = $DB->insert_record('companylicense_users', $recordarray);
						
						$eventother = array('licenseid' => $license->id, 'duedate' => 0);
						$event = \block_iomad_company_admin\event\user_license_assigned::create(array(
							'context' => context_course::instance($data->courseid),
							'objectid' => $recordarray['id'],
							'courseid' => $data->courseid,
							'userid' => $data->userid,
							'other' => $eventother
						));
						
						if(!isset($data->group) || is_null($data->group)){ $data->group = 'CURSO'; }
						
						try{
							if(isset($data->group) && in_array($data->group, array("PACK","CURSO","CENTRO"))){
								$group = array("courseid"=>$data->courseid, "descriptionformat"=>1, "hidepicture"=>1, "timecreated"=>time(), "timemodified"=>time());
								switch($data->group){
									case 'CURSO':
										$group["name"]="Curso"; $group['description']="<p>Alumnos de curso<br></p>";
										break;
									case 'PACK':
										$group["name"]="Pack"; $group['description']="<p>Alumnos de pack<br></p>";
										break;
									case 'CENTRO':
										$group["name"]="Centro"; $group['description']="<p>Alumnos de centros<br></p>";
										break;
								}
								//buscamos si el grupo ya esta creado:
								$exists = $DB->get_record('groups', array("name"=>$group['name'], "courseid"=>$group['courseid']));
								//si existe, cogemos el id
								if($exists){
									$groupid = $exists->id;
								}else{
									//si no existe, lo creamos:
									$groupid = $DB->insert_record('groups', $group);
								}
								
								//ahora que ya tenemos el grupo, asociamos el usuario al grupo:
								if(!$DB->get_record('groups_members', array("groupid"=>$groupid, "userid"=>$data->userid))){
									$DB->insert_record('groups_members', array("groupid"=>$groupid, "userid"=>$data->userid, "timeadded"=>time(), "component"=>"", "itemid"=>0));
								}
							}
						}catch(Exception $ex){
						}
						
						$event->trigger();
						/*
						$enrols = $DB->get_records_sql('SELECT e.id from mco_user_enrolments ue join mco_enrol e on e.id = ue.enrolid where ue.userid = ? and e.status = 1', array($data->userid));
						
						if($enrols){
							foreach($enrols as $enr){
								$DB->delete_records('user_enrolments', array('enrolid'=>$enr->id));
								$DB->delete_records('enrol', array('id'=>$enr->id));
							}
						}
						*/
					}else{
						
						header("HTTP/1.1 400 Bad Request");
						print(json_encode(array("error"=>true, "errcode"=>400, "errstr"=>"El usuario ya estaba matriculado. Datos recibidos: " . json_encode($data))));
						exit();
						//el usuario ya esta matriculado
						;
					}
					
					$DB->execute("UPDATE mco_companylicense_users SET isusing = 0 WHERE userid = ? AND isusing = 1", array($data->userid));
					
				}else{
					header("HTTP/1.1 418 I'm a teapot");
					print(json_encode(array("error"=>true, "errcode"=>400, "errstr"=>"La licencia no tiene slots disponibles. Datos recibidos: " . json_encode($data))));
					exit();
					//no license slots available
					;
				}
			}
		}
	}
?>