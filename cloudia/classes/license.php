<?php
	class licenses{
		public function get(){
			global $DB;
			$sql = "SELECT l.* FROM mco_companylicense l where l.companyid = ?";
			$licenses = $DB->get_records_sql($sql, array(COMPANY_ID));
			print(json_encode(array_values($licenses)));
		}
	}
	
	class license{
		public function create(){
			global $DB;
			global $CFG;
			
			$data = json_decode(file_get_contents("php://input"));
			
			$license = new stdClass;
			$license->name = $data->name;
			$license->allocation = (int)$data->allocation;
			$license->validlength = (int)$data->validlength;
			$license->startdate = strtotime($data->startdate);
			$license->expirydate = strtotime($data->expirydate);
			$license->used = 0;
			$license->companyid = COMPANY_ID;
			$license->parentid = 0;
			$license->type = 0;
			$license->program = (int)$data->program;
			$license->reference = $data->reference;
			$license->instant = (int)$data->instant;
			
			$license->id = $DB->insert_record('companylicense', (array)$license);
			
			print(json_encode($license));
		}
		
		public function update(){
			global $DB;
			
			$data = json_decode(file_get_contents("php://input"));
			
			if($data && isset($data->id)){
				$license = $DB->get_record('companylicense', array('id'=>$data->id));
				if($license){
					$license->name = $data->name;
					$license->allocation = (int)$data->allocation;
					$license->validlength = (int)$data->validlength;
					$license->startdate = strtotime($data->startdate);
					$license->expirydate = strtotime($data->expirydate);
					$license->program = (int)$data->program;
					$license->reference = $data->reference;
					$license->instant = (int)$data->instant;
					$DB->update_record('companylicense', $license);
				}
				
				print(json_encode((object)$license));
			}
		}
		
		public function add_slot_to_license(){
			global $DB;
			
			$done = false;
			
			$data = json_decode(file_get_contents("php://input"));
			
			if($data && isset($data->companyid) && isset($data->licenseid)){
				
				//buscamos la licencia relacionada con la que me pasan (de CEP) para la compania recibida:
				$license = $DB->get_record('companylicense', array('ceplicense'=>$data->licenseid, 'companyid'=>$data->companyid));
				
				if($license){
					$nslots = (int)(isset($data->slots)?$data->slots:1);
					$license->allocation+=$nslots;
					$DB->update_record('companylicense', $license);
					$done = true;
				}
			}
			
			print(json_encode(array("done"=>$done)));
		}
		
		public function delete(){
			global $DB;
			global $CFG;
			
			$data = json_decode(file_get_contents("php://input"));
			
			if($data && isset($data->id) && (int)$data->id > 0){
				$DB->delete_records('companylicense_courses', array('licenseid'=>$data->id));
				$DB->delete_records('companylicense_users', array('licenseid'=>$data->id));
				$DB->delete_records('companylicense', array('companyid'=>COMPANY_ID, 'id'=>$data->id));
				
				print(json_encode(array("done"=>true)));
			}
		}
	}
?>