<?php
	define('AJAX_SCRIPT', true);
	define('COMPANY_ID', 11);
	define('BASE_URI', 'https://www.aulacursosoposiciones.com/cloudia/api/');
	header("Content-Type: application/json");
	
	require_once(dirname(__FILE__) . '/../config.php');
	require_once($CFG->libdir.'/adminlib.php');
	
	require_once("classes/course.php");
	require_once("classes/user.php");
	require_once("classes/license.php");
	require_once("classes/company.php");
	require_once("classes/bills.php");
	
	$ctl = isset($_GET['ctl'])?$_GET['ctl']:null;
	if($ctl == 'company'){ $ctl = 'compania'; }
	$action = isset($_GET['action'])&&!empty($_GET['action'])?$_GET['action']:'main';
	$params = isset($_GET['paramstr'])&&!empty($_GET['paramstr'])?$_GET['paramstr']:'';
	
	if(!$ctl){
		header("HTTP/1.1 404 Not Found");
		print(json_encode(array("error"=>true, "errcode"=>404, "errstr"=>"Not Found")));
		exit();
	}
	
	if(!class_exists($ctl)){
		header("HTTP/1.1 503 Service unavailable");
		print(json_encode(array("error"=>true, "errcode"=>503, "errstr"=>"Service unavailable")));
		exit();
	}
	
	
	$service = new $ctl();
	
	if(!method_exists($service, $action)){
		header("HTTP/1.1 501 Not Implemented");
		print(json_encode(array("error"=>true, "errcode"=>501, "errstr"=>"Method `$action` not found in service $cls")));
		exit();
	}
	
	call_user_func_array(array($service, $action), explode("/",$params));
?>