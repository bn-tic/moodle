<?php
	ini_set('display_errors', 1);
	error_reporting(-1);
	
	require_once(dirname(__FILE__) . '/../config.php');
	require_once($CFG->libdir.'/adminlib.php');
	require_once($CFG->libdir.'/csvlib.class.php');
	require_once($CFG->dirroot.'/user/profile/lib.php');
	
	function increment_username($username, $mnethostid) {
		global $DB;

		if (!preg_match_all('/(.*?)([0-9]+)$/', $username, $matches)) {
			$username = $username.'2';
		} else {
			$username = $matches[1][0].($matches[2][0] + 1);
		}

		if ($DB->record_exists('user', array('username' => $username, 'mnethostid' => $mnethostid))) {
			return increment_username($username, $mnethostid);
		} else {
			return $username;
		}
	}
	
	$stdfields = array('id', 'firstname', 'lastname', 'username', 'email',
        'city', 'country', 'lang', 'auth', 'timezone', 'mailformat',
        'maildisplay', 'maildigest', 'htmleditor', 'ajax', 'autosubscribe',
        'mnethostid', 'institution', 'department', 'idnumber', 'skype',
        'msn', 'aim', 'yahoo', 'icq', 'phone1', 'phone2', 'address',
        'url', 'description', 'descriptionformat', 'oldusername', 'deleted',
        'password', 'temppassword', 'suspended');
	
	$company = new company(9);
	
	$user = new stdClass;
	$user->mnethostid = $CFG->mnet_localhost_id;
	$user->username = 'mrpotato';
	$user->email = 'raul@bn-tic.es';
	$user->firstname = 'Raúl';
	$user->lastname = 'bnTIC';
	$user->password = 'patatin';
	
	$user->username = clean_param($user->username, PARAM_USERNAME);
	
	$existinguser = $DB->get_record('user', array('username' => $user->username, 'mnethostid' => $user->mnethostid));
	
	if($existinguser){
		$user->id = $existinguser->id;
	}else{
		$user->confirmed = 1;
		$user->timemodified = time();
		$user->timecreated = time();
		$user->password = hash_internal_user_password($user->password);
	}
	
	$defaults = $company->get_user_defaults();
	$user = (object) array_merge((array) $defaults, (array) $user);
	
	$user->id = $DB->insert_record('user', $user);
	
	set_user_preference('auth_forcepasswordchange', 0, $user->id);
	
	profile_save_data($user);
	
	$company->assign_user_to_company($user->id);
	
	\core\event\user_created::create_from_userid($user->id)->trigger();
	
	var_dump($user);
	
?>