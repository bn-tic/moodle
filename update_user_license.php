<?php

require_once('config.php');
require_login();

echo $OUTPUT->header();

//get update data
$dates = $DB->get_records_sql("SELECT mco_z.id_platform, mco_user.id, UNIX_TIMESTAMP(mco_z.`start`) AS NSTART, mco_user_enrolments.TIMESTART AS START, 
    UNIX_TIMESTAMP(mco_z.`end`) AS NEND, mco_user_enrolments.TIMEEND AS END
    FROM mco_user_enrolments, mco_user, mco_enrol, mco_course, mco_z
    WHERE mco_user_enrolments.userid = mco_user.id
    AND mco_user.id = mco_z.id_platform
    AND mco_user_enrolments.enrolid = mco_enrol.id
    AND mco_enrol.courseid = mco_course.id
    #AND  mco_user_enrolments.timestart  >=  UNIX_TIMESTAMP(STR_TO_DATE('01/05/2019','%d/%m/%Y'))
    #AND  mco_user_enrolments.timestart  <= UNIX_TIMESTAMP(STR_TO_DATE('19/05/2019','%d/%m/%Y'))
    AND mco_user.EMAIL NOT like '%editorialcep%'
    #AND  (mco_course.fullname LIKE '%EXCEL%' OR mco_course.fullname LIKE '%PREMI%')
    #AND  mco_course.fullname NOT LIKE '%correo%'
    AND  mco_course.id = 531
    AND  mco_enrol.status = 0
    ORDER BY mco_user.id desc");

foreach ($dates as $date) {
    echo $date->id_platform . "nstart" . $date->nstart . "start" . $date->start . "<br>";

    $update = $DB->execute("UPDATE {user_enrolments} SET timestart = {$date->nstart} WHERE userid = {$date->id_platform}");
    $update = $DB->execute("UPDATE {user_enrolments} SET timeend = {$date->nend} WHERE userid = {$date->id_platform}");
}

echo $OUTPUT->footer();
