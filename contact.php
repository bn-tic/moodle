<?php
require_once('config.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->libdir . '/filelib.php');

echo $OUTPUT->header();
?>

<div class="row">

    <div class="course-pano wrapper">
        <img src="/theme/lambda/pix/contact.jpg">
        <div class="course-pano title">
            <h2><span>Contacto</h2></span>
        </div>
    </div>

    <form id="__vtigerWebForm" name="ERRATA" action="https://vtickets.aulacursosoposiciones.com/modules/Webforms/capture.php" method="post" accept-charset="utf-8" enctype="multipart/form-data">
        <input type="hidden" name="__vtrftk" value="sid:3aaf1a9cbed00c9cf1452c9fb2c8297032ac5219,1552902781">
        <input type="hidden" name="publicid" value="4af8c0ee44427e3f9596af9efe1cfeab">
        <input type="hidden" name="urlencodeenable" value="1">
        <input type="hidden" name="name" value="ERRATA">
        <table class="table table-striped" style="width:370px">
            <tbody>
                <tr>
                    <td>
                        <label>Nombre Solicitante*</label>
                    </td>
                    <td>
                        <input type="text" name="potentialname" data-label="" value="" required="">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Correo*</label>
                    </td>
                    <td>
                        <input type="text" name="cf_932" data-label="" value="" required="">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Teléfono*</label>
                    </td>
                    <td>
                        <input type="text" name="cf_894" data-label="" value="" required="">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>ISBN</label>
                    </td>
                    <td>
                        <input type="text" name="cf_922" data-label="" value="">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Oposici&oacute;n*</label>
                    </td>
                    <td>
                        <input type="text" name="cf_916" data-label="" value="" required="">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Tema*</label>
                    </td>
                    <td>
                        <input type="text" name="cf_918" data-label="" value="" required="">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Errata*</label>
                    </td>
                    <td>
                        <textarea name="cf_912" required=""></textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Correcci&oacute;n*</label>
                    </td>
                    <td>
                        <textarea name="cf_914" required=""></textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Informaci&oacute;n adicional</label>
                    </td>
                    <td>
                        <textarea name="cf_920"></textarea>
                    </td>
                </tr>
            </tbody>
        </table>
        <input type="submit" value="Enviar">
    </form>
    <script type="text/javascript">window.onload = function() { var N = navigator.appName, ua = navigator.userAgent, tem; var M = ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i); if (M && (tem = ua.match(/version\/([\.\d]+)/i)) != null) M[2] = tem[1]; M = M? [M[1], M[2]]: [N, navigator.appVersion, "-?"]; var browserName = M[0]; var form = document.getElementById("__vtigerWebForm"), inputs = form.elements; form.onsubmit = function() { var required = [], att, val; for (var i = 0; i < inputs.length; i++) { att = inputs[i].getAttribute("required"); val = inputs[i].value; type = inputs[i].type; if (type == "email") {if (val != "") {var elemLabel = inputs[i].getAttribute("label"); var emailFilter = /^[_/a-zA-Z0-9]+([!"#$%&()*+,./:;<=>?\^_`{|}~-]?[a-zA-Z0-9/_/-])*@[a-zA-Z0-9]+([\_\-\.]?[a-zA-Z0-9]+)*\.([\-\_]?[a-zA-Z0-9])+(\.?[a-zA-Z0-9]+)?$/; var illegalChars = /[\(\)\<\>\,\;\:\"\[\]]/; if (!emailFilter.test(val)) {alert("For " + elemLabel + " field please enter valid email address"); return false; } else if (val.match(illegalChars)) {alert(elemLabel + " field contains illegal characters"); return false; }}}if (att != null) { if (val.replace(/^\s+|\s+$/g, "") == "") { required.push(inputs[i].getAttribute("label")); } } } if (required.length > 0) { alert("The following fields are required: " + required.join()); return false; } var numberTypeInputs = document.querySelectorAll("input[type=number]"); for (var i = 0; i < numberTypeInputs.length; i++) { val = numberTypeInputs[i].value; var elemLabel = numberTypeInputs[i].getAttribute("label"); var elemDataType = numberTypeInputs[i].getAttribute("datatype"); if (val != "") {if (elemDataType == "double") {var numRegex = /^[+-]?\d+(\.\d+)?$/; } else{var numRegex = /^[+-]?\d+$/; }if (!numRegex.test(val)) {alert("For " + elemLabel + " field please enter valid number"); return false; }}}var dateTypeInputs = document.querySelectorAll("input[type=date]"); for (var i = 0; i < dateTypeInputs.length; i++) {dateVal = dateTypeInputs[i].value; var elemLabel = dateTypeInputs[i].getAttribute("label"); if (dateVal != "") {var dateRegex = /^[1-9][0-9]{3}-(0[1-9]|1[0-2]|[1-9]{1})-(0[1-9]|[1-2][0-9]|3[0-1]|[1-9]{1})$/; if (!dateRegex.test(dateVal)) {alert("For " + elemLabel + " field please enter valid date in required format"); return false; }}}var inputElems = document.getElementsByTagName("input"); var totalFileSize = 0; for (var i = 0; i < inputElems.length; i++) {if (inputElems[i].type.toLowerCase() === "file") {var file = inputElems[i].files[0]; if (typeof file !== "undefined") {var totalFileSize = totalFileSize + file.size; }}}if (totalFileSize > 52428800) {alert("Maximum allowed file size including all files is 50MB."); return false; }}; }</script>
</div>

<?php
echo $OUTPUT->footer();
