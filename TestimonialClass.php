<?php

/*
 * class testimonials 
 */
define("admins", ['2', '5193', '5192', '1002', '5196', '8722', '6358', '1815', '7065', '7069', '7068', '9403', '10423', '10925', '15759', '4236']);

class Testimonial {

    public function __construct() {
        
    }

    /**
     * is admin 
     * @param type $userid
     * @return string
     */
    public function is_admin($userid){
        if (in_array($userid, admins)) {
            $message = 1;
        } else
            $message = 2;

        return $message;
    }
    
    
    /**
     * get testimonial by id
     * @param type $testimonialid
     */
    public function get_testimonial_byid($testimonialid){
        global $DB;
        $testimonial = $DB->get_record_sql("SELECT * FROM mco_testimonial t WHERE t.id = {$testimonialid}");
        
        return $testimonial;
    }
    
    
    /**
     * Get all testimonials
     * @global type $DB
     * @return type
     */
    public function get_testimonials() {
        global $DB;
        $testimonials = $DB->get_records_sql("SELECT * FROM mco_testimonial ORDER BY date DESC");

        return $testimonials;
    }
    
    /**
     * Insert testimonial
     * @global type $DB
     * @param type $name
     * @param type $surname
     * @param type $testimonial
     * @param type $courseid
     */
    public static function insert_testimonial($name, $surname, $testimonial, $courseid) {
        global $DB;

        $data = new stdClass();

        $data->name = $name;
        $data->surname = $surname;
        $data->testimonial = $testimonial;
        $data->courseid = $courseid;
        $data->date = strtotime('now');

        try {
            $transaction = $DB->start_delegated_transaction();
            $DB->insert_record('testimonial', $data, $returnid = TRUE);

            $transaction->allow_commit();
        } catch (Exception $e) {
            $transaction->rollback($e);
        }
    }
    
    /**
     * get course name by id
     * @global type $DB
     * @param type $courseid
     * @return type
     */
    public static function get_coursebyid($courseid){
        global $DB;
        
       $course = $DB->get_record_sql("SELECT c.fullname FROM mco_course c WHERE c.id = {$courseid}");
       
       return $course->fullname;
    }
    
    
    /**
     * delete testimonial by id
     * @global type $DB
     * @param type $id
     */
    public function delete_testimonial($id) {
        global $DB;

        //deleting
        $DB->execute("DELETE FROM mco_testimonial WHERE mco_testimonial.id = {$id}");
        
        echo "Registro eliminado";
    }
    
    
    /**
     * updating a testimonial
     * @global type $DB
     * @param type $id
     * @param type $name
     */
    public function update_testimonial($id, $name, $surname, $testimonial, $courseid){
        global $DB;

        try {
            try {
                $transaction = $DB->start_delegated_transaction();
                //updating
                $DB->execute("UPDATE mco_testimonial SET "
                        . "mco_testimonial.name = '{$name}', "
                        . "mco_testimonial.surname = '{$surname}', "
                        . "mco_testimonial.testimonial = '{$testimonial}', "
                        . "mco_testimonial.courseid = {$courseid}, "
                        . "mco_testimonial.date = " .strtotime('now')." "
                        . "WHERE mco_testimonial.id = {$id}");
                $transaction->allow_commit();
            } catch (Exception $e) {
                // Make sure transaction is valid.
                if (!empty($transaction) && !$transaction->is_disposed()) {
                    $transaction->rollback($e);
                }
            }
        } catch (Exception $e) {
            // Silence the rollback exception or do something else.
            echo "error en la transacción";
        }
    }
    
}
