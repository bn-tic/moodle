<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for local_usertours.
 *
 * @package   local_usertours
 * @copyright 2016 Andrew Nicols <andrew@nicols.co.uk>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['actions'] = 'Acciones';
$string['appliesto'] = 'Se aplica a';
$string['block'] = 'Bloque';
$string['block_named'] = 'Bloque llamado \'{$a}\'';
$string['bottom'] = 'Debajo';
$string['description'] = 'Descripción';
$string['confirmstepremovalquestion'] = '¿Estás seguro de que deseas eliminar este paso?';
$string['confirmstepremovaltitle'] = 'Confirmar eliminar paso';
$string['confirmtourremovalquestion'] = '¿Estás seguro de que deseas eliminar este tour?';
$string['confirmtourremovaltitle'] = 'Confirmar eliminar tour';
$string['content'] = 'Contenido';
$string['content_help'] = 'Este es el contenido del paso.
Puede ingresar un contenido en los siguientes formatos:
<dl>
    <dt> Texto sin formato </dt>
    <dd> Una descripción de texto plano </dd>
    <dt> Moodle MultiLang </dt>
    <dd> Una cadena que utiliza el formato Moodle MultiLang </dd>
    <dt> Cadena traducida de Moodle </dt>
    <dd> Un valor encontrado en un archivo de lenguaje Moodle estándar en el identificador de formato, componente </dd>
</dl>';
$string['cssselector'] = 'Selector CSS';
$string['defaultvalue'] = 'Por defecto ({$a})';
$string['delay'] = 'Demora antes de mostrar el paso';
$string['done'] = 'Hecho';
$string['editstep'] = 'Edición "{$a}"';
$string['enabled'] = 'Habilitado';
$string['exporttour'] = 'Exportar tour';
$string['importtour'] = 'Importar tour';
$string['left'] = 'Izquierda';
$string['movestepdown'] = 'Mueve paso abajo';
$string['movestepup'] = 'Mueve paso arriba';
$string['movetourdown'] = 'Mueve tour abajo';
$string['movetourup'] = 'Mueve tour arriba';
$string['name'] = 'Nombre';
$string['newstep'] = 'Crear paso';
$string['newstep'] = 'Nuevo paso';
$string['newtour'] = 'Crear nuevo tour';
$string['next'] = 'Siguiente';
$string['pathmatch'] = 'Aplicar a la coincidencia de URL';
$string['pathmatch_help'] = 'Los tours se mostrarán en cualquier página cuya URL coincida con este valor.

Puede usar el carácter% como comodín para significar cualquier cosa.
Algunos valores de ejemplo incluyen:

* / my /% - para que coincida con el Tablero
* /course/view.php?id=2 - para que coincida con un curso específico
* /mod/forum/view.php% - para que coincida con la lista de discusión del foro
* /user/profile.php% - para que coincida con la página de perfil de usuario';
$string['placement'] = 'Placement';
$string['pluginname'] = 'User Tours';
$string['resettouronpage'] = 'Restablecer tour';
$string['right'] = 'Derecha';
$string['select_block'] = 'Selecciona un bloque';
$string['select_targettype'] = 'Cada paso está asociado con una parte de la página que debe elegir. Para facilitar esto, hay varios tipos de objetivos para diferentes tipos de contenido de la página.
<dl>
    <dt> Bloque </dt>
    <dd> Muestra el paso al lado del primer bloque coincidente del tipo en la página </dd>
    <dt> Selector </dt>
    <dd> Los selectores CSS son una forma poderosa que le permite seleccionar diferentes partes de la página según los metadatos integrados en la página. </dd>
    <dt> Mostrar en el medio de la página </dt>
    <dd> En lugar de asociar el paso con una parte específica de la página, puede mostrarlo en el medio de la página. </dd>
</dl>';
$string['selector_defaulttitle'] = 'Introduce un título descriptivo';
$string['selectordisplayname'] = 'Un selector de CSS que coincida \'{$a}\'';
$string['skip'] = 'Omitir';
$string['target'] = 'Objetivo';
$string['target_block'] = 'Bloque';
$string['target_selector'] = 'Selector';
$string['target_unattached'] = 'Mostrar en medio de la página';
$string['targettype'] = 'Tupo de objetivo';
$string['title'] = 'Título';
$string['title_help'] = 'Este es el título que se muestra en la parte superior del paso.
Puede ingresar un título en los siguientes formatos:
<dl>
    <dt> Texto sin formato </dt>
    <dd> Una descripción de texto plano </dd>
    <dt> Moodle MultiLang </dt>
    <dd> Una cadena que utiliza el formato Moodle MultiLang </dd>
    <dt> Cadena traducida de Moodle </dt>
    <dd> Un valor encontrado en un archivo de lenguaje Moodle estándar en el identificador de formato, componente </dd>
</dl>';
$string['top'] = 'Arriba';
$string['tourconfig'] = 'Tour archivo de configuración para importar';
$string['tourlist_explanation'] = 'Puede crear tantos recorridos como desee y habilitarlos para diferentes partes de Moodle. Solo se puede crear un recorrido por página.';
$string['tours'] = 'Tours';
$string['pausetour'] = 'Pausa';
$string['resumetour'] = 'Reanudar';
$string['endtour'] = 'Finalizar Tour';
$string['orphan'] = 'Mostrar si el objetivo no se encuentra';
$string['orphan_help'] = 'Muestre el paso si no se pudo encontrar el objetivo en la página.';
$string['backdrop'] = 'Mostrar con telón de fondo';
$string['backdrop_help'] = 'Puede usar un fondo para resaltar la parte de la página a la que está apuntando.

Nota: Los fondos no son compatibles con algunas partes de la página, como la barra de navegación.
';
$string['reflex'] = 'Mover al hacer clic';
$string['reflex_help'] = 'Pase al siguiente paso cuando haga clic en el objetivo.';
$string['placement_help'] = 'Puede colocar un paso arriba, abajo, a la izquierda o a la derecha del objetivo.

Las mejores opciones son superior o inferior, ya que se ajustan mejor para la pantalla móvil.';
$string['delay_help'] = 'Opcionalmente, puede optar por agregar un retraso antes de que se muestre el paso.

Este retraso es en milisegundos.';
$string['selecttype'] = 'Seleccionar tipo de paso';
$string['usertours'] = 'Tours de usuarios';
$string['target_selector_targetvalue'] = 'CSS Selectores';
$string['target_selector_targetvalue_help'] = 'You can use a "CSS Selector" to target almost any element on the page.

CSS Selectors are very powerful and you can easily find parts of the page by building up the selector gradually.

Mozilla provide some [very good
documentation](https://developer.mozilla.org/en/docs/Web/Guide/CSS/Getting_started/Selectors)
for selectors which may help you to build your selectors.

You will also find your browser\'s developer tools to be extremely useful in creating these selectors:

* [Google Chrome](https://developer.chrome.com/devtools#dom-and-styles)
* [Mozilla Firefox](https://developer.mozilla.org/en-US/docs/Tools/DOM_Property_Viewer)
* [Microsoft Edge](https://developer.microsoft.com/en-us/microsoft-edge/platform/documentation/f12-devtools-guide/)
* [Apple Safari](https://developer.apple.com/library/iad/documentation/AppleApplications/Conceptual/Safari_Developer_Guide/ResourcesandtheDOM/ResourcesandtheDOM.html#//apple_ref/doc/uid/TP40007874-CH3-SW1)
';
$string['sharedtourslink'] = 'Tour repositorio';
$string['viewtour_info'] = 'Este es \'{$a->tourname}\' tour. Se aplica a esta ruta \'{$a->path}\'.';
