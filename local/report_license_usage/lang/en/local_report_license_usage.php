<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['blocktitle'] = 'Informe de historial de asignación de licencias';
$string['pluginname'] = 'Informe de historial de asignación de licencias';
$string['privacy:metadata'] = 'El informe del historial de asignación de licencias de Iomad local solo muestra los datos almacenados en otras ubicaciones.';
$string['report_license_usage_title'] = 'Informe de historial de asignación de licencias';
$string['report_license_usage:view'] = 'Ver Informe de historial de asignación de licencias';
$string['repuserlicallocation'] = 'Informe de historial de asignación de licencias';
$string['totalallocate'] = 'Número de asignacioness';
$string['totalunallocate'] = 'Número de desasignaciones';
$string['numstart'] = 'Previamente asignado';
$string['numnet'] = 'Asignaciones netas';
$string['numtotal'] = 'Total asignadas';
