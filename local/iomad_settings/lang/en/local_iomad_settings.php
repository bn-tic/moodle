<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['pluginname'] = 'Configuraciones de Iomad';
$string['privacy:metadata'] = 'El complemento de configuración de Iomad local solo muestra los datos almacenados en otras ubicaciones.';
$string['establishment_code'] = 'Código de Establecimiento';
$string['establishment_code_help'] = '¿Qué debe ver el administrador en el bloque de la lista de cursos?';
$string['customtext2'] = 'Texto personalizado 2';
$string['customtext3'] = 'Texto personalizado 3';
$string['dateformat'] = 'Formato de fecha';
$string['iomad_autoenrol_managers'] = 'Inscribir a los gerentes como no estudiantes'; 
$string['iomad_autoenrol_managers_help'] = 'Si esto no está marcado, las cuentas de administrador no se inscribirán como roles de maestro de la empresa en los cursos de inscripción manual'; 
$string['iomadcertificate_logo'] = 'Logotipo predeterminado para el certificado de la empresa Iomad';
$string['iomadcertificate_signature'] = 'Firma por defecto para el certificado de la empresa Iomad';
$string['iomadcertificate_border'] = 'Borde predeterminado para el certificado de la empresa Iomad';
$string['iomadcertificate_watermark'] = 'Marca de agua por defecto para el certificado de la empresa Iomad';
$string['iomadcertificate_logodesc'] = 'Esta es la imagen de logotipo predeterminada utilizada para el tipo de certificado de la empresa Iomad. Puede anularlo en las páginas de edición de la empresa. La imagen cargada debe tener una altura de 80 píxeles y un fondo transparente.';
$string['iomadcertificate_signaturedesc'] = 'Esta es la imagen de firma predeterminada utilizada para el tipo de certificado de la empresa Iomad. Puede anularlo en las páginas de edición de la empresa. La imagen cargada debe tener 31 píxeles x 150 píxeles y tener un fondo transparente.';
$string['iomadcertificate_borderdesc'] = 'Esta es la imagen de borde predeterminada utilizada para el tipo de certificado de empresa Iomad. Puede anularlo en las páginas de edición de la empresa. La imagen cargada debe ser de 800 píxeles x 604 píxeles.';
$string['iomadcertificate_watermarkdesc'] = 'Esta es la imagen de marca de agua predeterminada utilizada para el tipo de certificado de la empresa Iomad. Puede anularlo en las páginas de edición de la empresa. La imagen cargada no debe tener más de 800 píxeles x 604 píxeles.';
$string['iomad_allow_username'] = 'Puede especificar el nombre de usuario';
$string['iomad_allow_username_help'] = 'Seleccionar esto permitirá que se presente el campo de nombre de usuario al crear cuentas. Esto reemplazará el uso de la dirección de correo electrónico como nombre de usuario.';
$string['iomad_report_fields'] = 'Campos de perfil de informe adicionales';
$string['iomad_report_fields_help'] = 'Esta es una lista de campos de perfil separados por una coma. Si desea usar un campo de perfil opcional, necesita usar profile_field_ <shortname> donde <shortname> es el nombre corto definido para el campo de perfil. El orden dado es el orden en el que se muestran.';
$string['iomad_settings:addinstance'] = 'Agregar una nueva configuración de Iomad';
$string['iomad_sync_department'] = 'Sincronizar el nombre del departamento con el perfil';
$string['iomad_sync_department_help'] = 'Al seleccionar esto, el campo de perfil de usuario del departamento se sincronizará con el nombre del departamento de la empresa en el que se encuentra el usuario.';
$string['iomad_sync_institution'] = 'Sincronizar el nombre de la empresa con el perfil';
$string['iomad_sync_institution_help'] = 'Al seleccionar esto, el campo de perfil de usuario de la institución se sincronizará con el nombre corto de la empresa en la que se encuentra el usuario.';
$string['iomad_use_email_as_username'] = 'Usar email como nombre de usuario';
$string['iomad_use_email_as_username_help'] = 'Al seleccionar esto, cambiará la forma en que se crea automáticamente un nombre de usuario para una nueva cuenta de usuario en Iomad para que simplemente use la dirección de correo electrónico.';
$string['reset_annually'] = 'Anualmente';
$string['reset_daily'] = 'Diario';
$string['reset_never'] = 'Nunca';
$string['reset_sequence'] = 'Restablecer el número de secuencia';
$string['serialnumberformat'] = 'Formato de número de serie';
$string['serialnumberformat_help'] = '<p>Los campos de Texto personalizado y el Formato de número de serie pueden tener las siguientes variables:</p><ul>
                                        <li>{EC} = Código de Establecimiento</li>
                                        <li>{CC} = ID Curso</li>
                                        <li>{CD:DDMMYY} = Fecha (con formato)</li>
                                        <li>{SEQNO:n} = Número de secuencia (con relleno n)</li>
                                        <li>{SN} = Número de serie del certificado (en blanco si se usa en el campo Formato de número de serie)</li>
                                        </ul>';

// SAMPLE Certificate.
$string['sampletitle'] = 'Certificado de entrenamiento';
$string['samplecertify'] = 'Esto es para certificar que';
$string['samplestatement'] = 'ha completado un curso de e-learning en';
$string['sampledate'] = 'en';
$string['samplecoursegrade'] = 'con el resultado de';
$string['typesample'] = 'Muestra';
$string['samplecode'] = 'Número certificado :';
$string['samplesigned'] = 'Firmado: ';
$string['sampleonbehalfof'] = 'En nombre de la empresa';
