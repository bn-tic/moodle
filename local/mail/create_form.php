<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    local-mail
 * @copyright  Albert Gasset <albert.gasset@gmail.com>
 * @copyright  Marc Català <reskit@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir . '/formslib.php');
require_once(dirname(__FILE__) . '/../../CustomClass.php');

class local_mail_create_form extends moodleform {

    public function definition() {
        $mform = $this->_form;
        $courses = $this->_customdata['courses'];

        // Header.

        $label = get_string('compose', 'local_mail');
        $mform->addElement('header', 'general', $label);

        // Course.

        $label = get_string('course');
        $options = array(SITEID => '');
        foreach ($courses as $course) {
            $options[$course->id] = $course->fullname;
        }
        $mform->addElement('autocomplete', 'c', $label, $options);
        $mform->addElement('hidden', 'r', '');

        
        if($_GET['course'] != ""){
            $mform->setDefault('c', $_GET['course']);
            $teacher = Custom::get_teacher_by_course($_GET['course']);
            if($teacher != 0){
                $mform->addElement('html', '<div class="prom-box prom-box-default">
                    <i class="fa fa-envelope-o" aria-hidden="true" style="float:left; margin-right:15px; color:#dbdbdb; font-size: 5em;"></i>
                    <h3>Enviar correo</h3>
                    <p class="plus">Va a enviar un email a '.$teacher->firstname. " " .$teacher->lastname.'</p>
                    </div>');
                $mform->setDefault('r', $teacher->id);
            }
            else{
                $mform->addElement('html', '<div class="prom-box prom-box-default">
                    <i class="fa fa-info-circle" aria-hidden="true" style="float:left; margin-right:15px; color:#dbdbdb; font-size: 5em;"></i>
                    <h3>Este curso no tiene tutor</h3>
                    <p class="plus"><a href="'.$CFG->wwwrootblocks.'/blocks/search_course/view.php">Pulsa aqu&iacute; para volver a tus cursos</a></p>
                    </div>');
            }
        }
        // Button.

        $label = get_string('continue', 'local_mail');
        $mform->addElement('submit', 'continue', $label);
    }

    public function validation($data, $files) {
        global $SITE;

        $errors = array();
        
        if ($data['c'] == $SITE->id) {
            $errors['course'] = get_string('erroremptycourse', 'local_mail');
        }

        return $errors;
    }
}
