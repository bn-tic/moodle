<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['companies'] = 'Empresas';
$string['completed'] = 'Completado';
$string['course'] = 'Curso';
$string['report_completion:view'] = 'Ver el informe de progreso del curso iomad.';
$string['completiondate'] = 'Fecha de Terminación';
$string['completionnotenabled'] = 'El seguimiento de finalización no está habilitado para este curso (no hay datos de informe disponibles)';
$string['coursechart'] = 'Dibujar tabla de resumen para el curso';
$string['iomad_completion:view'] = 'informe de finalización de iomad';
$string['error'] = 'Error al encontrar el campo de perfil personalizado para la empresa';
$string['inprogress'] = 'En progreso';
$string['name'] = 'Nombre';
$string['nodata'] = 'No hay datos del informe';
$string['notstarted'] = 'No empezado';
$string['nousers'] = 'No hay usuarios en este curso';
$string['participant'] = 'Participantes';
$string['percentage'] = 'Porcentaje Completado';
$string['pluginname'] = 'Informe de finalización por curso';
$string['privacy:metadata'] = 'El informe de finalización del curso de Iomad local solo muestra los datos almacenados en otras ubicaciones.';
$string['repcoursecompletion'] = 'Informe de finalización por curso';
$string['remaining'] = 'Restante';
$string['reportselect'] = 'Filtro de informe';
$string['select'] = '--seleccionar--';
$string['status'] = 'Estado';
$string['trackedcount'] = 'Número de recursos / actividades rastreadas: ';
$string['userrecordnotfound'] = 'No se encontró el registro de usuario';
$string['coursename'] = 'Nombre del curso';
$string['completionenabled'] = 'Finalización habilitada';
$string['numusers'] = 'Número de usuarios';
$string['notstartedusers'] = 'No empezado';
$string['inprogressusers'] = 'Todavía en progreso';
$string['completedusers'] = 'Completado';
$string['completionisenabled'] = 'Si';
$string['certificate'] = 'Certificado';
$string['coursesummary'] = 'Resumen del curso';
$string['courseusers'] = 'Resumen del usuario para el curso - ';
$string['percent'] = '% completado';
$string['timecreated'] = 'Fecha de creacion';
$string['timestarted'] = 'Fecha iniciado';
$string['timecompleted'] = 'Fecha completado';
$string['finalscore'] = 'Puntuación final';
$string['downloadcsv'] = 'Descargar como archivo CSV';
$string['invalidcourse'] = 'La ID del curso aprobada no es válida para esta empresa / departamento';
$string['firstname'] = 'Nombre';
$string['lastname'] = 'Apellido';
$string['email'] = 'Email';
$string['timeenrolled'] = 'Fecha matriculación';
$string['dateenrolled'] = 'Fecha matriculación';
$string['datestarted'] = 'Fecha empezado';
$string['datecompleted'] = 'Fecha completado';
$string['department'] = 'Departamento';
$string['completionfrom'] = 'Fecha inicio finalización';
$string['completionto'] = 'Fecha fin finalización';
$string['started'] = 'Empezado';
$string['summarychart'] = 'Dibujar tabla resumen para todos los cursos';
$string['usersummary'] = 'Resumen curso';
$string['cchart'] = 'Gráfico';
$string['reportallusers'] = 'Todos los usuarios del departamento';
$string['allusers'] = 'Mostrar todos los usuarios';
$string['hidehistoricusers'] = 'Eliminar resultados históricos';
$string['historicusers'] = 'Incluir resultados historicos';
$string['historiccompletedusers'] = 'Histórico completado';
$string['timeexpires'] = 'Válido hasta';
$string['showsuspendedusers'] = 'Incluir usuarios suspendidos';
$string['hidesuspendedusers'] = 'Eliminar usuarios suspendidos';
$string['compfrom'] = 'Fecha Inicio';
$string['compto'] = 'Fecha Fin';
