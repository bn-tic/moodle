<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['attendance'] = 'Asistentes';
$string['companies'] = 'Empresas';
$string['completed'] = 'Completado';
$string['course'] = 'Curso';
$string['report_attendance:view'] = 'Ver informe de asistencia del curso iomad.';
$string['completiondate'] = 'Fecha de Terminación';
$string['completionnotenabled'] = 'El seguimiento de finalización no está habilitado para este curso (no hay datos de informe disponibles)';
$string['iomad_attendance:view'] = 'informe de asistencia de iomad';
$string['error'] = 'Error al encontrar el campo de perfil personalizado para la empresa';
$string['event'] = 'Evento de entrenamiento -';
$string['inprogress'] = 'En progreso';
$string['name'] = 'Nombre';
$string['nodata'] = 'No hay datos del informe';
$string['notstarted'] = 'No empezado';
$string['nousers'] = 'No hay usuarios en este curso';
$string['participant'] = 'Participante';
$string['percentage'] = 'Porcentaje Completado';
$string['pluginname'] = 'Reporte de asistencia por curso';
$string['privacy:metadata'] = 'El informe de asistencia de Iomad local solo muestra los datos almacenados en otras ubicaciones.';
$string['repcourseattendance'] = 'Reporte de asistencia por curso';
$string['remaining'] = 'Restante';
$string['reportselect'] = 'Filtro de informe';
$string['select'] = '--seleccionar--';
$string['status'] = 'Estado';
$string['trackedcount'] = 'Número de recursos / actividades rastreadas: ';
$string['userrecordnotfound'] = 'No se encontró el registro de usuario';
$string['coursename'] = 'Nombre del curso';
$string['completionenabled'] = 'Finalización habilitada';
$string['numusers'] = 'Número de usuarios';
$string['nocourses'] = '<h2>No hay eventos de entrenamiento</h2>
<p>No hay actividades de eventos de capacitación establecidas en ninguno de sus cursos. </p>';
$string['notstartedusers'] = 'No empezado';
$string['inprogressusers'] = 'Todavía en progreso';
$string['completedusers'] = 'Completado';
$string['completionisenabled'] = 'Si';
$string['certificate'] = 'Certificado';
$string['coursesummary'] = 'Resumen del curso';
$string['courseusers'] = 'Resumen del usuario para el curso - ';
$string['percent'] = '% completado';
$string['datestarted'] = 'Fecha asignada / iniciada';
$string['datecompleted'] = 'Fecha completada';
$string['finalscore'] = 'Puntuación final';
$string['downloadcsv'] = 'Descargar como archivo CSV';
$string['invalidcourse'] = 'La ID del curso aprobada no es válida para esta empresa / departamento';
