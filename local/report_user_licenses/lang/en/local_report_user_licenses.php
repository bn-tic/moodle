<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['companies'] = 'Empresas';
$string['completed'] = 'Completado';
$string['course'] = 'Curso';
$string['report_user_licenses:view'] = 'Ver el informe de usuario de la licencia de Iomad';
$string['completiondate'] = 'Fecha de Terminación';
$string['completionnotenabled'] = 'El seguimiento de finalización no está habilitado para este curso (no hay datos de informe disponibles)';
$string['coursechart'] = 'Dibujar tabla de resumen para el curso';
$string['iomad_completion:view'] = 'informe de finalización de iomad';
$string['error'] = 'Error al encontrar el campo de perfil personalizado para la empresa';
$string['used'] = 'Usado';
$string['name'] = 'Nombre';
$string['nodata'] = 'No hay datos del informe';
$string['unused'] = 'No usado';
$string['nousers'] = 'No hay usuarios en este curso';
$string['participant'] = 'Participantes';
$string['percentage'] = 'Porcentaje completado';
$string['pluginname'] = 'Informe de uso de licencia por usuario';
$string['privacy:metadata'] = 'El informe de uso de la licencia de Iomad local por usuario solo muestra los datos almacenados en otras ubicaciones.';
$string['repuserlicenses'] = 'Informe de uso de licencia por usuario';
$string['remaining'] = 'Restante';
$string['reportselect'] = 'Filtro de informe';
$string['select'] = '--seleccionar--';
$string['status'] = 'Estado';
$string['trackedcount'] = 'Número de recursos / actividades rastreadas: ';
$string['userrecordnotfound'] = 'No se encontró el registro de usuario';
$string['coursename'] = 'nombre del curso';
$string['completionenabled'] = 'Finalización habilitada';
$string['numallocated'] = 'Licencias asignadas';
$string['notstartedusers'] = 'No empezado';
$string['inprogressusers'] = 'Todavía en progreso';
$string['completedusers'] = 'Completado';
$string['completionisenabled'] = 'Si';
$string['certificate'] = 'Certificado';
$string['coursesummary'] = 'Resumen del curso';
$string['courseusers'] = 'Resumen del usuario para el curso - ';
$string['percent'] = '% completado';
$string['timestarted'] = 'Fecha iniciada';
$string['timecompleted'] = 'Fecha completada';
$string['finalscore'] = 'Puntuación final';
$string['downloadcsv'] = 'Descargar como archivo CSV';
$string['invalidcourse'] = 'La ID del curso aprobada no es válida para esta empresa / departamento';
$string['firstname'] = 'Nombre';
$string['lastname'] = 'Apellido';
$string['email'] = 'Email';
$string['timeenrolled'] = 'Fecha matriculación';
$string['dateenrolled'] = 'Fecha matriculación';
$string['datestarted'] = 'Fecha empezado';
$string['datecompleted'] = 'Fecha completado';
$string['department'] = 'Departamento';
$string['completionfrom'] = 'Fecha inicio finalización';
$string['completionto'] = 'Fecha fin finalización';
$string['started'] = 'Empezado';
$string['summarychart'] = 'Dibujar tabla resumen para todos los cursos';
$string['usersummary'] = 'Resumen curso';
$string['cchart'] = 'Gráfico';
$string['reportallusers'] = 'Todos los usuarios del departamento';
$string['allusers'] = 'Mostrar todos los usuarios';
$string['licensename'] = 'Nombre licencia';
$string['isusing'] = 'En uso';
$string['issuedate'] = 'Fecha asignada';
$string['licensesendreminder'] = '¿Desea enviar un correo electrónico de recordatorio a esta lista de usuarios?';
$string['sendreminderemail'] = 'Enviar un email de recordatorio';
$string['licenseemailsent'] = 'Los correos electrónicos han sido enviados';
