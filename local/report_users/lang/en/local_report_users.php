<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['actions'] = 'Comportamiento';
$string['blocktitle'] = 'Informes de usuario';
$string['certificate'] = 'Certificado';
$string['clear'] = 'Limpiar';
$string['clearconfirm'] = 'El usuario será eliminado del curso y todos sus datos serán eliminados. Todavía tendrán una licencia para reiniciar el curso. ¿Seguro que quieres hacer esto?';
$string['completed'] = 'Completado';
$string['coursedetails'] = 'Informe completo del curso';
$string['datecompleted'] = 'Fecha completada';
$string['datestarted'] = 'Curso asignado / iniciado';
$string['delete'] = 'Eliminar';
$string['deleteconfirm'] = 'Se eliminará al usuario del curso, se eliminarán todos sus datos y se volverán a asignar las licencias. Esto no se puede deshacer. ¿Estás seguro?';
$string['department'] = 'Departamento';
$string['downloadcert'] = 'Ver certificado en PDF';
$string['inprogress'] = 'En progreso';
$string['nocerttodownload'] = 'Certificado no logrado';
$string['nofurtherdetail'] = 'No hay más detalles para mostrar';
$string['notstarted'] = 'No empezado';
$string['pluginname'] = 'Informe de usuarios';
$string['privacy:metadata'] = 'El informe de finalización del usuario de Iomad local solo muestra los datos almacenados en otras ubicaciones.';
$string['report_users_title'] = 'Informe de usuario';
$string['report_users:view'] = 'Ver el informe de los usuarios.';
$string['repusercompletion'] = 'Informe de finalización por usuario';
$string['scormattempts'] = 'Número de intentos';
$string['scormnotstarted'] = 'Usuario matriculado en curso. Módulo no iniciado';
$string['scormquestion'] = 'ID pregunta';
$string['scormresult'] = 'Resultado';
$string['scormresults'] = 'Resultado';
$string['scormscore'] = 'Puntuación';
$string['scormtimestarted'] = 'Comenzó en';
$string['scormtype'] = 'Tipo de pregunta';
$string['user_detail_title'] = 'Informes de usuario';
$string['usercoursedetails'] = 'Detalles de usuario';
$string['userdetails'] = 'Informe de información para ';
$string['viewfullcourse'] = 'Ver resumen completo del curso.';
