<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'local_iomad', language 'en'
 */

$string['cannotemailnontemporarypasswords'] = 'Es inseguro enviar las contraseñas por correo electrónico sin forzarlas a cambiarse en el primer inicio de sesión.';
$string['companycityfilter'] = 'La ubicación de la empresa contiene';
$string['companycountryfilter'] = 'País de la empresa contiene';
$string['companycourses'] = 'Otros cursos de la empresa';
$string['companyfilter'] = 'Filtrar Resultados';
$string['companynamefilter'] = 'El nombre de la empresa contiene'; 
$string['companysearchfields'] = 'Campos de búsqueda de empresas';
$string['crontask'] = 'Iomad Cron';
$string['emailfilter'] = 'La dirección de correo electrónico contiene';
$string['firstnamefilter'] = 'Primer nombre contiene';
$string['lastnamefilter'] = 'El apellido contiene';
$string['missingaccesstocourse'] = 'No tienes permiso para eso.';
$string['nopermissions'] = 'El administrador de Iomad no le ha dado permiso para hacer esto.';
$string['pluginname'] = 'Iomad';
$string['privacy:metadata'] = 'El complemento de Iomad local solo muestra los datos almacenados en otras ubicaciones.';
$string['setupiomad'] = 'Empezar a configurar iomad';
$string['show_suspended_users'] = '¿Mostrar usuarios suspendidos?';
$string['userfilter'] = 'Filtrar Resultados';
$string['usersearchfields'] = 'Campos de búsqueda de usuarios';
$string['show_suspended_companies'] = '¿Mostrar empresas suspendidas?';
