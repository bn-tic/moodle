<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['certificate'] = 'Certificado';
$string['companies'] = 'Compañías';
$string['completed'] = 'Terminado';
$string['completedusers'] = 'Terminado';
$string['completiondate'] = 'Fecha de finalización';
$string['completionenabled'] = 'Finalización habilitada';
$string['completionisenabled'] = 'Si';
$string['completionnotenabled'] = 'El seguimiento de finalización no está habilitado para este curso (no hay datos de informe disponibles)';
$string['course'] = 'Curso';
$string['coursename'] = 'Nombre del curso';
$string['coursesummary'] = 'Resumen del curso';
$string['courseusers'] = 'Resumen del usuario para el curso - ';
$string['courseselect'] = 'Cursos que contienen objetos SCORM';
$string['error'] = 'Error al encontrar el campo de perfil personalizado para la empresa';
$string['inprogress'] = 'En progreso';
$string['inprogressusers'] = 'Todavía en progreso';
$string['name'] = 'Nombre';
$string['nodata'] = 'No hay datos del informe';
$string['noscormcourses'] = 'No hay cursos SCORM disponibles para este departamento.';
$string['notstarted'] = 'No empezado';
$string['notstartedusers'] = 'No empezado';
$string['nousers'] = 'No hay usuarios en este curso';
$string['numattempts'] = 'Intentos totales';
$string['numusers'] = 'Número de usuarios';
$string['participant'] = 'Partícipe';
$string['percent'] = '% completado';
$string['percentage'] = 'Porcentaje';
$string['percentright'] = 'Respondió correctamente';
$string['percentwrong'] = 'Respondió incorrectamente';
$string['pluginname'] = 'Informe general de iomad SCORM';
$string['privacy:metadata'] = 'El informe general de SCORM de Iomad local solo muestra los datos almacenados en otras ubicaciones.';
$string['question'] = 'Pregunta';
$string['questiontype'] = 'Tipo pregunta';
$string['remaining'] = 'Restante';
$string['repcoursecompletion'] = 'Informe de finalización por curso';
$string['report_scorm_overview:view'] = 'Ver el informe general de iomad SCORM';
$string['repscormoverview'] = 'Informe general de SCORM';
$string['reportselect'] = 'Filtro de informe';
$string['scormnoresults'] = 'No hay resultados registrados para esto.';

$string['select'] = '--selecciona--';
$string['status'] = 'Estado';
$string['trackedcount'] = 'Número de recursos / actividades rastreadas: ';
$string['userrecordnotfound'] = 'No se encontró el registro de usuario';

