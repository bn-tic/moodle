<?php

require_once "models/Post.php";

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        echo json_encode(Post::getAll());
        break;
    
    default:
            http_response_code(405);
            break;
}
