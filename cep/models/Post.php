<?php

require_once 'connection/Connection.php';
require_once(dirname(__FILE__) . '/../../CustomClass.php');

class Post{
    /**
     * get all posts
     */
    public static function getAll(){
        $db = new Connection();
        $result = $db->query("SELECT p.ID, p.post_title, p.post_content, p.post_name, p.post_author, p.post_date, pm.meta_key, pm.meta_value, 
                    (SELECT guid FROM blog_posts WHERE ID = pm.meta_value) post_image, (SELECT guid FROM blog_posts WHERE ID = p.ID) post_link
                    FROM blog_posts p
                    JOIN blog_postmeta pm ON pm.post_id = p.ID 
                    WHERE p.post_status = 'publish' 
                    AND p.post_type = 'post' 
                    AND pm.meta_key = '_thumbnail_id'
                    ORDER BY p.post_date DESC LIMIT 6;");
        $posts  = [];
        
        if($result->num_rows){
            while($row = $result->fetch_assoc()){
                $posts[] = [
                    'id' => $row['ID'],
                    'post_image' => $row['post_image'], 
                    'post_link' => $row['post_link'],
                    'post_title' => $row['post_title'],
                    'post_content' => $row['post_content'],
                    'post_name' => $row['post_name'],
                    'post_author' => $row['post_author'],
                    'post_date' => $row['post_date'],
                    'meta_key' => $row['meta_key'],
                    'meta_value' => $row['meta_value']
                ];
            }
            return $posts;
        }
        return $posts;
        
       
    }
}
