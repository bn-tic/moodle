<?php

/**
 * Conexión a la base de datos del blog
 */
class Connection extends Mysqli{
    function __construct() {
        //parent::__construct('localhost', 'root', '', 'wordpress');
        parent::__construct('blog.editorialcep.com', 'blog_wp_u', 'iaw33#6P', 'blog_wp');
        $this->set_charset('utf8');
        $this->connect_error == NULL ? 'Conexión ok' : die('Error al conectarse a la base de datos');
    }
}