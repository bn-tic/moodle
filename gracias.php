<?php
require_once('config.php');
require_once($CFG->dirroot .'/course/lib.php');
require_once($CFG->libdir .'/filelib.php');

echo $OUTPUT->header();?>



<div class="jumbotron text-xs-center">
  <h1 class="display-3">¡Gracias!</h1>
  <p class="lead">En breve atenderemos su petici&oacute;n</p>
  <hr>
  <p class="lead">
    <a class="btn btn-primary btn-sm" href="https://www.aulacursosoposiciones.com/blocks/search_course/view.php" role="button">Clic para volver a la plataforma</a>
  </p>
</div>



<?php echo $OUTPUT->footer();