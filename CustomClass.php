<?php

/*
 * class event (calendar)
 */

class Custom {

    public function __construct() {
        
    }

    /**
     * Control manual enrolments
     * @param type $course
     * @param type $loggeduser
     * @param type $user
     * @param type $action
     * @param type $rol
     */
    public static function insert_log_assignrol($course, $loggeduser, $user, $action, $rol) {
        global $DB;

        $data = new stdClass();

        $data->time = strtotime('now');
        $data->course = $course;
        $data->loggeduser = $loggeduser;
        $data->user = $user;
        $data->action = $action;
        $data->rol = $rol;

        try {
            $transaction = $DB->start_delegated_transaction();
            $DB->insert_record('roleassignments_log', $data, $returnid = FALSE);

            $transaction->allow_commit();
        } catch (Exception $e) {
            $transaction->rollback($e);
        }
    }

    /**
     * get user enrolment info
     * @param type $userid
     */
    public static function get_enrols_by_user($userid) {
        global $DB;

        $enrols = $DB->get_records_sql("SELECT distinct mco_user_enrolments.enrolid, mco_user_enrolments.userid, mco_user_enrolments.timeend, 
            FROM_UNIXTIME(mco_user_enrolments.timeend, '%d/%m/%Y %H:%i:%s') AS timeendformated, mco_course.fullname, mco_course.imgcatalogue, mco_companylicense.name, mco_course.id
            FROM mco_user_enrolments 
            JOIN mco_user ON mco_user.id = mco_user_enrolments.userid
            JOIN mco_enrol ON mco_enrol.id = mco_user_enrolments.enrolid
            JOIN mco_course ON mco_course.id = mco_enrol.courseid
            JOIN mco_companylicense_users ON mco_companylicense_users.userid = mco_user.id
            JOIN mco_companylicense ON mco_companylicense_users.licenseid = mco_companylicense.id
            WHERE mco_user_enrolments.userid = {$userid}");

        return $enrols;
    }

    public static function get_enrol_by_user_and_enrolid($userid, $enrolid) {
        global $DB;

        $enrol = $DB->get_record_sql("SELECT distinct mco_user_enrolments.enrolid, mco_user_enrolments.userid, mco_user_enrolments.timeend, 
            FROM_UNIXTIME(mco_user_enrolments.timeend, '%d/%m/%Y %H:%i:%s') AS timeendformated, mco_course.fullname, mco_course.imgcatalogue, mco_companylicense.name, mco_course.id
            FROM mco_user_enrolments 
            JOIN mco_user ON mco_user.id = mco_user_enrolments.userid
            JOIN mco_enrol ON mco_enrol.id = mco_user_enrolments.enrolid
            JOIN mco_course ON mco_course.id = mco_enrol.courseid
            JOIN mco_companylicense_users ON mco_companylicense_users.userid = mco_user.id
            JOIN mco_companylicense ON mco_companylicense_users.licenseid = mco_companylicense.id
            WHERE mco_user_enrolments.userid = {$userid} AND mco_user_enrolments.enrolid = {$enrolid}");

        return $enrol;
    }

    /**
     * Updating license enrolment by course and user
     * @param type $user
     * @param type $course
     */
    public static function updating_license_enrolment($timeend, $user, $enrol) {
        global $DB;

        try {
            try {
                $transaction = $DB->start_delegated_transaction();
                // Do something here.
                $DB->execute("UPDATE mco_user_enrolments SET mco_user_enrolments.timeend = {$timeend} WHERE mco_user_enrolments.userid = {$user} AND mco_user_enrolments.enrolid = {$enrol}");
                $transaction->allow_commit();
            } catch (Exception $e) {
                // Make sure transaction is valid.
                if (!empty($transaction) && !$transaction->is_disposed()) {
                    $transaction->rollback($e);
                }
            }
        } catch (Exception $e) {
            // Silence the rollback exception or do something else.
            echo "error en la transacción";
        }
    }

    /**
     * Get company by user id
     * @global type $DB
     * @param type $userid
     */
    public static function get_company_by_user($userid) {
        global $DB;

        //get company by user        
        $company = $DB->get_record_sql("SELECT mco_company_users.companyid FROM mco_company_users WHERE mco_company_users.userid = {$userid}");

        return $company;
    }

    /**
     * get license by course and user
     * @global type $DB
     * @param type $course
     * @param type $user
     * @return int
     */
    public static function get_license_by_user_and_course($user, $course) {
        global $DB;

        //get license
        $license = $DB->get_record_sql("SELECT mco_companylicense_users.licenseid "
                . "FROM mco_companylicense_users "
                . "WHERE mco_companylicense_users.userid = {$user} AND mco_companylicense_users.licensecourseid = {$course}");

        if (count($license) != 0)
            return $license;
        else
            return 0;
    }

    /**
     * Get teacher by course
     * @global type $DB
     * @param type $course
     * @return type
     */
    public static function get_teacher_by_course($course) {
        global $DB;
        //get last access on course for logged user
        $teacher = $DB->get_record_sql("SELECT mco_user.id, mco_user.firstname, mco_user.lastname "
                . "FROM mco_user_enrolments "
                . "LEFT JOIN mco_company_users ON mco_user_enrolments.userid = mco_company_users.userid "
                . "LEFT JOIN mco_enrol ON mco_enrol.id = mco_user_enrolments.enrolid "
                . "LEFT JOIN mco_user ON mco_user.id = mco_user_enrolments.userid "
                . "WHERE mco_company_users.educator = 1 AND mco_enrol.courseid = {$course}");

        if (count($teacher) != 0)
            return $teacher;
        else
            return 0;
    }

    /**
     * Get a course group by user
     * @param type $course
     * @param type $user
     */
    public static function get_group_by_course($course, $user) {
        global $DB;

        $groups = $DB->get_records_sql("SELECT mco_groups.name "
                . "FROM mco_groups "
                . "JOIN mco_groups_members ON mco_groups_members.groupid = mco_groups.id "
                . "WHERE mco_groups.courseid = {$course} "
                . "AND mco_groups_members.userid = {$user}");

        $groupnames = array();

        foreach ($groups as $group) {
            array_push($groupnames, $group->name);
        }

        return $groupnames;
    }

    /**
     * Get a group by course
     * @global type $DB
     * @param type $course
     * @return array
     */
    public static function get_groups_by_course($course) {
        global $DB;

        $groups = $DB->get_records_sql("SELECT mco_groups.id, mco_groups.name "
                . "FROM mco_groups "
                //. "JOIN mco_groups_members ON mco_groups_members.groupid = mco_groups.id "
                . "WHERE mco_groups.courseid = {$course};");

        return $groups;
    }

    /**
     * insert user into group table
     * @param type $user
     * @param type $group
     */
    public static function insert_member_into_group($user, $group) {
        global $DB;

        $data = new stdClass();

        $data->groupid = $group;
        $data->userid = $user;
        $data->timeadded = strtotime('now');
        $data->component = "";
        $data->itemid = 0;

        try {
            $transaction = $DB->start_delegated_transaction();
            $DB->insert_record('groups_members', $data, $returnid = TRUE);

            $transaction->allow_commit();
        } catch (Exception $e) {
            $transaction->rollback($e);
        }
    }

    /**
     * create group on a course
     * @global type $DB
     * @param type $course
     */
    public static function create_group_on_course($course, $name, $description) {
        global $DB;

        $data = new stdClass();

        $data->courseid = $course;
        $data->idnumber = "";
        $data->name = $name;
        $data->description = $description;
        $data->descriptionformat = 1;
        $data->enrolmentkey = null;
        $data->picture = 0;
        $data->hidepicture = 1;
        $data->timecreated = strtotime('now');
        $data->timemodified = strtotime('now');

        try {
            $transaction = $DB->start_delegated_transaction();
            $group = $DB->insert_record('groups', $data, $returnid = TRUE);

            $transaction->allow_commit();
        } catch (Exception $e) {
            $transaction->rollback($e);
        }

        return $group;
    }

    public static function updating_enrolments_not_null() {
        global $DB;

        $enrolments = $DB->get_records_sql("SELECT * FROM mco_user_enrolments WHERE mco_user_enrolments.timeendprevio IS NOT NULL AND mco_user_enrolments.userid = 12150");

        foreach ($enrolments as $enrolment) {
            echo $enrolment->id . "-->" . $enrolment->timeend . "FIN  <--> PREVIO" . $enrolment->timeendprevio;
            if ($enrolment->timeendprevio > $enrolment->timeend) {
                $DB->execute("UPDATE mco_user_enrolments SET mco_user_enrolments.timeend = {$enrolment->timeendprevio} WHERE mco_user_enrolments.id = {$enrolment->id}");
                $count++;
                echo " si <br>";
            } else {
                echo " no <br>";
            }
        }
        echo "actualizados " . $count;
    }

    
    /**
     * Get all visible courses
     */
    public static function get_all_courses(){
        global $DB;
        
        $courses = $DB->get_records_sql("SELECT id, fullname FROM mco_course WHERE mco_course.visible = 1 ORDER BY fullname asc");

        return $courses;
    }
    
    
    /**
     * Get accredited course
     * @global type $DB
     * @return type
     */
    public static function get_accredited_course() {
        global $DB;

        $courses = $DB->get_records_sql("SELECT * FROM mco_course c WHERE c.fullname LIKE '%Curso Temario oposiciones%'");

        return $courses;
    }

    /**
     * Get enroled students by course
     * @global type $DB
     * @param type $course
     * @return type
     */
    public static function get_accredited_students_by_course($course) {
        global $DB;

        $students = $DB->get_records_sql("SELECT u.id, concat(u.firstname , ' ' , u.lastname) nombre, u.email "
                . "FROM mco_enrol e "
                . "JOIN mco_user_enrolments ue ON ue.enrolid = e.id "
                . "JOIN mco_user u ON u.id = ue.userid "
                . "WHERE e.courseid = {$course} AND ue.timeend <= NOW()");

        return $students;
    }

    /**
     * Get tests by course
     * @global type $DB
     * @param type $course
     * @param type $userid
     * @return type
     */
    public static function get_tests_by_course($course) {
        global $DB;

        // tests by course
        $quizs = $DB->get_records_sql("SELECT * FROM mco_quiz q WHERE q.course = {$course}");
        
        return $quizs;
    }
    
    /**
     * get attempt by quiz and user
     * @global type $DB
     * @param type $quizid
     * @param type $userid
     * @return type
     */
    public static function get_attempt_by_quiz_and_user($quizid, $userid){
        global $DB;
        
        $attempt = $DB->get_record_sql("SELECT * FROM mco_quiz_attempts qa WHERE qa.quiz = {$quizid} AND qa.userid = {$userid} AND qa.state = 'finished'");
        
        return $attempt;
    }
    
    /**
     * update acreditado
     * @param type $courseid
     * @param type $userid
     */
    public function update_acreditado($courseid, $userid){
        global $DB;
        
        $enrolid = $DB->get_record_sql("SELECT e.id FROM mco_user_enrolments ue JOIN mco_enrol e ON e.id = ue.enrolid WHERE ue.userid = {$userid} AND e.courseid = {$courseid}");
        
        try {
            try {
                $transaction = $DB->start_delegated_transaction();
                // Do something here.
                $DB->execute("UPDATE mco_user_enrolments SET mco_user_enrolments.acreditado = 1 WHERE mco_user_enrolments.enrolid = {$enrolid->id} AND mco_user_enrolments.userid = {$userid}");
                $transaction->allow_commit();
            } catch (Exception $e) {
                // Make sure transaction is valid.
                if (!empty($transaction) && !$transaction->is_disposed()) {
                    $transaction->rollback($e);
                }
            }
        } catch (Exception $e) {
            // Silence the rollback exception or do something else.
            echo "error en la transacción";
        }
        
    }

}
