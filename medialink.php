<?php
	require 'repository/pitraco/vendor/autoload.php';
	use Aws\S3\S3Client;

	$s3 = new Aws\S3\S3Client([
        	'profile' => 'default',
                'version' => 'latest',
                'region' => 'eu-west-2'
	]);

	$cmd = $s3->getCommand('GetObject', [
		'Bucket' => 'recursos-multimedia',
		'Key'    => base64_decode($_GET['src'])
	]);

	$request = $s3->createPresignedRequest($cmd, '+10 minutes');
	header("Location: " . $request->getUri(), 301);
?>
