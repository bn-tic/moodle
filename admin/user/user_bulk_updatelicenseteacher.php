<?php

require_once('../../config.php');
require_once($CFG->libdir . '/adminlib.php');

$confirm = optional_param('confirm', 0, PARAM_BOOL);

require_login();
admin_externalpage_setup('userbulk');
require_capability('moodle/user:update', context_system::instance());

$return = $CFG->wwwroot . '/' . $CFG->admin . '/user/user_bulk.php';

if (empty($SESSION->bulk_users)) {
    redirect($return);
}

echo $OUTPUT->header();

$users = $SESSION->bulk_users;

class updating_licenseteacher_form extends moodleform {

    public function definition() {
        //form
        global $CFG;

        $mform = $this->_form;
        $mform->addElement('html', '<hr>');
        $mform->addElement('date_time_selector', 'timeend', 'Nueva fecha');
        $mform->setDefault('timeend', $timeend);

        $this->add_action_buttons(false, 'Actualizar licencia');
    }

}

$updating_form = new updating_licenseteacher_form(new moodle_url('/admin/user/user_bulk_updatelicenseteacher.php'), null, 'get');

//render form
$updating_form->display();

if (isset($_GET['sesskey'])) {
    $timeend = $_GET['timeend']['year'] . "-" . $_GET['timeend']['month'] . "-" . $_GET['timeend']['day'] . $_GET['timeend']['hour'] . ':' . $_GET['timeend']['minute'];
    $date = strtotime($timeend);
    
    //users id
    foreach ($users as $user) {
        // por cada usuario nos traemos sus cursos
        $sql = "SELECT mco_user_enrolments.id
            FROM mco_enrol 
            JOIN mco_user_enrolments ON mco_user_enrolments.enrolid = mco_enrol.id
            JOIN mco_course ON mco_enrol.courseid = mco_course.id
            WHERE mco_user_enrolments.userid = {$user} AND mco_course.visible = 1
            AND mco_course.FULLNAME NOT LIKE '%OBSOLETO%' AND mco_enrol.`status` = 0
            AND (mco_user_enrolments.timeend > 1610969073 OR mco_user_enrolments.timeend = 0)";

        $enrolments = $DB->get_records_sql($sql);

        foreach ($enrolments as $enrolment) {
            //actualizamos los cursos
            try {
                $transaction = $DB->start_delegated_transaction();
                $DB->execute("UPDATE mco_user_enrolments SET mco_user_enrolments.timeend =  {$date}  WHERE mco_user_enrolments.id = {$enrolment->id}");
                $transaction->allow_commit();
            } catch (Exception $e) {
                $transaction->rollback($e);
            }
        }
    }
    $redirecturl = new moodle_url('/admin/user/user_bulk.php');
    redirect($redirecturl, "<div class='alert alert-info'>Licencias actualizadas correctamente</div>");
}


echo $OUTPUT->footer();
