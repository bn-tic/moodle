<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tool_redocerts', language 'en', branch 'MOODLE_22_STABLE'
 *
 * @package    tool
 * @subpackage redocerts
 * @copyright  2011 Petr Skoda {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['doit'] = 'Sí, hazlo!';
$string['notifyfinished'] = '...terminado';
$string['pageheader'] = 'Recrear certificados almacenados';
$string['pluginname'] = 'Recrear certificados almacenados';
$string['searchusers'] = 'Selecciona un usuario';
$string['searchcourses'] = 'Selecciona un curso';
$string['searchcompanies'] = 'Selecciona una empresa';
$string['idnumber'] = 'Desde tracking id number';
$string['fromdate'] = 'Finalización desde la fecha';
$string['todate'] = 'Finalización hasta la fecha';
