<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Parent theme: Bootstrapbase by Bas Brands
 * Built on: Essential by Julian Ridden
 *
 * @package   theme_lambda
 * @copyright 2014 redPIthemes
 *
 */
//API CLOUDIA
require_once(dirname(__FILE__) . '/../../../../TestimonialClass.php');
require_once(dirname(__FILE__) . '/apiclient.php');


$haslogo = (!empty($PAGE->theme->settings->logo));

$hasheaderprofilepic = (empty($PAGE->theme->settings->headerprofilepic)) ? false : $PAGE->theme->settings->headerprofilepic;

$checkuseragent = '';
if (!empty($_SERVER['HTTP_USER_AGENT'])) {
    $checkuseragent = $_SERVER['HTTP_USER_AGENT'];
}
$username = get_string('username');
if (strpos($checkuseragent, 'MSIE 8')) {
    $username = str_replace("'", "&prime;", $username);
}
?>

    <?php if ($PAGE->theme->settings->socials_position == 1) { ?>
    <div class="container-fluid socials-header"> 
    <?php require_once(dirname(__FILE__) . '/socials.php'); ?>
    </div>
    <?php }
?>

<header id="page-header" class="clearfix">

    <div class="container-fluid">    
        <div class="row-fluid">
            <!-- HEADER: LOGO AREA -->

<?php if (!$haslogo) { ?>
                <div class="span6">
                    <h1 id="title" style="line-height: 2em"><?php echo $SITE->shortname; ?></h1>
                </div>
<?php } else { ?>
                <div class="logo-header">
                    <a class="logo" href="<?php echo $CFG->wwwroot; ?>" title="<?php print_string('home'); ?>">
                        <?php
                        echo html_writer::empty_tag('img', array('src' => $PAGE->theme->setting_file_url('logo', 'logo'), 'class' => 'logo', 'alt' => 'logo'));
                        ?>
                    </a>
                </div>
<?php } ?>      	

            <div class="login-header">
                <div class="profileblock">

                    <?php

                    function get_content() {
                        global $USER, $CFG, $SESSION, $COURSE;
                        $wwwroot = '';
                        $signup = '';
                    }

                    if (empty($CFG->loginhttps)) {
                        $wwwroot = $CFG->wwwroot;
                    } else {
                        $wwwroot = str_replace("http://", "https://", $CFG->wwwroot);
                    }

                    if (!isloggedin() or isguestuser()) {
                        ?>
                        <style>
                            @media screen and (max-width: 768px) {
                                #block-login{
                                    width: 310px;
                                    margin-right:-40px;
                                }
                            }		
                            #block-login{
                                width: 310px;
                            }
                            #block-login input[type="text"], #block-login input[type="password"] {
                                display: inline;
                                width: 135px;
                                border: 1px solid #e9e9e9; 
                                margin-bottom: 2px;
                            }
                            #submit{
                                margin: 0px 0px 0px 2px;
                            }			
                            #forgot_password{
                                margin:5px 10px 5px 0px;
                                text-align: right;
                            }
                            label{
                                display: inline;
                            }
                        </style>
                        <form class="navbar-form pull-right" method="post" action="<?php echo $wwwroot; ?>/login/index.php?authldap_skipntlmsso=1">
                            <div id="block-login">
                                <label id="user"><i class="fa fa-user"></i></label>
                                <input id="inputName" class="span2" type="text" name="username" placeholder="<?php echo $username; ?>">
                                <label id="pass"><i class="fa fa-key"></i></label>
                                <input id="inputPassword" class="span2" type="password" name="password" id="password" placeholder="<?php echo get_string('password'); ?>">
                                <input type="submit" id="submit" name="submit" value=""/>
                                <div id="forgot_password">
                                    <a href="<?php echo $CFG->wwwroot ?>/login/forgot_password.php">Recuperar contraseña</a>
                                </div>
                            </div>
                        </form>

                    <?php
                    } else {

                        echo '<div id="loggedin-user">';
                        echo $OUTPUT->user_menu();
                        echo "<a href='" . $CFG->wwwroot . "/user/edit.php'>" . $OUTPUT->user_picture($USER, array('size' => 60, 'link' => false, 'class' => 'welcome_userpicture')) . '</a>';
                        echo '</div>';
                    }
                    ?>

                </div>
            </div>

        </div>
    </div>

</header>

<header role="banner" class="navbar">
    <nav role="navigation" class="navbar-inner">
        <div class="container-fluid">
<?php if (isloggedin()) { ?>
                <a class="brand" href="<?php echo $CFG->wwwroot; ?>"><?php echo $SITE->shortname; ?></a>
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="nav-collapse collapse">
    <?php echo $OUTPUT->custom_menu(); ?>
                    <div class="nav-divider-right"></div>
                    <ul class="nav pull-right">
                        <li><?php echo $OUTPUT->page_heading_menu(); ?></li>
                    </ul>

                    <form id="search" action="<?php echo $CFG->wwwroot; ?>/course/search.php" method="GET">
                        <?php $isadmin = Testimonial::is_admin($USER->id); if($isadmin == 1) {?><a class="btn btn-warning" style="margin-top:0px; height: 38px; border-radius: 0px;" href="<?php echo $CFG->wwwroot; ?>/blocks/testimonial/view.php"><i class="fa fa-star" style="margin-top: 5px; font-size: 28px;"></i></a><?php }?>
                        <div class="nav-divider-left"></div>							
                        <input id="coursesearchbox" type="text" onFocus="if (this.value == '<?php echo get_string('searchcourses'); ?>')
                                                        this.value = ''" onBlur="if (this.value == '')
                                                                    this.value = '<?php echo get_string('searchcourses'); ?>'" value="<?php echo get_string('searchcourses'); ?>" name="search">
                        <input type="submit" value="">							
                    </form>

                </div>
<?php } ?>
        </div>
    </nav>	
    <!-- Hotjar Tracking Code for https://www.aulacursosoposiciones.com/ -->
    <script>

        (function (h, o, t, j, a, r) {

            h.hj = h.hj || function () {
                (h.hj.q = h.hj.q || []).push(arguments)};

            h._hjSettings = {hjid: 2651191, hjsv: 6};

            a = o.getElementsByTagName('head')[0];

            r = o.createElement('script');
            r.async = 1;

            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;

            a.appendChild(r);

        })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');


        function createCountDown(elementId, date) {
            console.log(date);
            // fecha fin de licencia
            var countDownDate = new Date(date).getTime();
            //console.log(countDownDate);

            // cada segundo update
            var x = setInterval(function () {

            // fecha de now
            var now = new Date().getTime();

            // diferencia de fechas
            var distance = (countDownDate) - (now);

        
            // Dias, horas, min y segundos
                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                //console.log(days);
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                // id time para ver cuenta atrás
                document.getElementById(elementId).innerHTML = days + "d " + hours + "h " + minutes + "m " + seconds + "s ";

                // Si la licencia ha expirado
                if (distance < 0)
                {
                    clearInterval(x);
                    document.getElementById(elementId).innerHTML = "Su licencia ha expirado";
                }
            }, 1000);
        }

    </script>
</header>