<?php
	class CrossCloudIAClient{
		private static $endpoint = 'https://api.grupocep.es/landings/';
		private static $__instance = null;
		
		private function __construct(){
		}
		
		public static function getInstance(){
			if(self::$__instance == null){
				self::$__instance = new CrossCloudIAClient();
			}
			return self::$__instance;
		}
		
		public function parse($plugin_token){
			$c = curl_init(self::$endpoint . "parse.php");
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($c, CURLOPT_POST, 1);
			curl_setopt($c, CURLOPT_POSTFIELDS, http_build_query(array("token"=>$plugin_token)));
			
			curl_setopt($c, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
			curl_setopt($c, CURLOPT_TIMEOUT, 15);
			$ret = curl_exec($c);
			
			
			$httpcode = curl_getinfo($c, CURLINFO_HTTP_CODE);
			curl_close($c);
			
			if($httpcode == 403){
				$err = json_decode($ret);
				if($err && $err->error){
					return "Error: " . $err->errstr;
				}
			}else{
				return $ret;
			}
		}
		
		
	};
	
	
?>