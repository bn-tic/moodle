<?php

/**
 * Makes our changes to the CSS
 *
 * @param string $css
 * @param theme_config $theme
 * @return string 
 */
function saga_process_css($css, $theme) {

    $themecolor = $theme->settings->themecolor;
    
    switch ($themecolor) {
        case 'blue':
            $color1 = "#F1F0F0";
            $color2 = "#487E85";
            $color3 = "#095573";
            $color4 = "#97A12A";
            $themespriteposition = "-25px";
            break;
        case 'purple':
            $color1 = "#E2E2E2";
            $color2 = "#777FAD";
            $color3 = "#000018";
            $color4 = "#535353";
            $themespriteposition = "-50px";
            break;
        case 'black':
            $color1 = "#E2E2E2";
            $color2 = "#505050";
            $color3 = "#0A0A0A";
            $color4 = "#505050";
            $themespriteposition = "-75px";
            break;
    }
    
    $css = str_replace("[[setting:color1]]", $color1, $css);
    $css = str_replace("[[setting:color2]]", $color2, $css);
    $css = str_replace("[[setting:color3]]", $color3, $css);
    $css = str_replace("[[setting:color4]]", $color4, $css);
    $css = str_replace("[[setting:themespriteposition]]", $themespriteposition, $css);
    
    $css = str_replace("[[setting:logourl]]", $theme->settings->logourl, $css);
    $css = str_replace("[[setting:footermod_aboutus_whitelogo]]", $theme->settings->footermod_aboutus_whitelogo, $css);

    $slideritems = json_decode($theme->settings->slideshowdata);
    
    $slidecss = "";
    for($x=1;$x<=sizeof($slideritems);$x++){
        $slidecss .= '.bg-img-'.$x.' {
                            background: url('.$slideritems[$x-1]->image.') no-repeat;
                        }';
    }
    $css = str_replace("[[setting:slidebackgrounds]]", $slidecss, $css);
    
    // Return the CSS
    return $css;
}

?>