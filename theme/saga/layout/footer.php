<div id="footer" class="row">
    <div class="sklt-container">
        <div class="four columns alpha">
            <?php echo $OUTPUT->footermod("module1"); ?>
        </div> 
        <div class="four columns">
            <?php echo $OUTPUT->footermod("module2"); ?>
        </div>
        <div class="four columns">
            <?php echo $OUTPUT->footermod("module3"); ?>
        </div>
        <div class="four columns omega">
            <?php echo $OUTPUT->footermod("module4"); ?>
        </div>
    </div>
</div>