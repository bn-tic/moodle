<?php 
    $menubar = empty($PAGE->layout_options['nomenubar']);
    $noslider = !empty($PAGE->layout_options['noslider']);
    $search = empty($PAGE->layout_options['nosearch']);
    $topbutton = (isset($PAGE->layout_options['topbutton']))?$PAGE->layout_options['topbutton']:'logout';
?>

<div id="topbar" class="row">
    <div class="sklt-container">
        <div class="<?php echo ($search)?"nine":"fifteen"; ?> columns alpha">
            <?php echo $OUTPUT->socialicons('header'); ?>
        </div>
        <?php if($search){ ?>
            <div class="six columns omega">
                <?php echo $OUTPUT->searchbar($CFG->wwwroot); ?>
            </div>
        <?php } ?>
        <div class="one columns alpha omega">
            <?php
                switch ($topbutton) {
                    case 'home':
                        echo '<div id="home" class="topbutton"><a href="'.$CFG->wwwroot.'">Home</a></div>';
                    break;
                    default:
                        if(isloggedin())
                            echo '<div id="logout" class="topbutton"><a href="'.$CFG->wwwroot.'/login/logout.php">Logout</a></div>';
                        else
                            echo '<div id="login" class="topbutton"><a href="'.$CFG->wwwroot.'/login">Login</a></div>';
                }
            ?>
        </div>
    </div>
</div>

<?php if($menubar) { ?>
    <?php echo $OUTPUT->menu($CFG->wwwroot); ?>
<?php } ?>