<?php
class theme_saga_core_renderer extends core_renderer {
    private function footermod_aboutus(){
        $logourl = get_config('theme_saga', 'footermod_aboutus_whitelogo');
        $text = get_config('theme_saga', 'footermod_aboutus_text');
        
        $content = '<div class="footermod footermod_aboutus">';
        
        if(!$logourl || trim($logourl)=="")
            $content .= '<div id="defaultlogowhite"></div>';
        else{
            $content .= '<div id="logowhite"></div>';
        }
        
        $content .= "<p>".$text."</p>";
        
        $content .= "</div>";
        
        return $content;
    }

    private function footermod_links(){
        $links = json_decode(get_config('theme_saga', 'footermod_links'));
        
        $content = '<div class="footermod footermod_links">'; 
        $content .= '<p class="title">Links</p>';
        
        $content .= '<ul class="links">';
        
        for($x=0;$x<sizeof($links);$x++){
            $content .= '<li><a target="blank" href="'.$links[$x]->link.'">'.$links[$x]->text.'</a></li>';
        }
        
        $content .= '</ul>';
        $content .= '</div>';
        
        return $content;
    }
    
    private function footermod_contactinfo(){
        $address = get_config('theme_saga', 'footermod_contact_address');
        $city = get_config('theme_saga', 'footermod_contact_city');
        $phone = get_config('theme_saga', 'footermod_contact_phone');
        $mail = get_config('theme_saga', 'footermod_contact_mail');
        
        $content = '<div class="footermod footermod_contactinfo">';
        
        $content .= '<p class="title">Contact Info</p>';
        
        $content .= '<ul class="contactinfos">';
        $content .= '<li>'.$address.'</li>';
        $content .= '<li>'.$city.'</li>';
        $content .= '<li>'.$phone.'</li>';
        $content .= '<li>'.$mail.'</li>';
        $content .= '</ul>';
        
        $content .= "</div>";
        
        return $content;
    }
    
    private function footermod_image(){
        $title = get_config('theme_saga', 'footermod_image_title');
        $src = get_config('theme_saga', 'footermod_image_url');
        
        $content = '<div class="footermod footermod_image">';
        
        $content .= '<p class="title">'.$title.'</p>';
        $content .= '<div class="image"><img src="'.$src.'"/></div>';
        
        $content .= "</div>";
        
        return $content;
    }
    
    private function footermod_notice(){
        $title = get_config('theme_saga', 'footermod_notice_title');
        $text = get_config('theme_saga', 'footermod_notice_text');
        
        $content = '<div class="footermod footermod_text">';
        
        $content .= '<p class="title">'.$title.'</p>';
        $content .= '<p>'.$text.'</p>';
        
        $content .= "</div>";
        
        return $content;
    }

    private function footermod_none(){
        return '<div style="width:1px;height:1px;"></div>';
    }

    
    protected function footermod($modulearea){
        $module = get_config("theme_saga","footer".$modulearea);
        if(trim($module)!=""){
            $module = "footermod_".$module;
            return $this->$module();
        }else{
            return ' ';
        }
    }
    
    protected function mycourses($CFG,$sidebar){
        $mycourses = enrol_get_users_courses($_SESSION['USER']->id);
        
        $courselist = array();
        foreach ($mycourses as $key=>$val){
            $courselist[] = $val->id;
        }
        
        $content = '';
        
        for($x=1;$x<=sizeof($courselist);$x++){
            $course = get_course($courselist[$x-1]);
            $title = $course->fullname;
            
            if ($course instanceof stdClass) {
                require_once($CFG->libdir. '/coursecatlib.php');
                $course = new course_in_list($course);
            }

            $url = $CFG->wwwroot."/theme/saga/pix/coursenoimage.jpg";
            foreach ($course->get_course_overviewfiles() as $file) {
                $isimage = $file->is_valid_image();
                $url = file_encode_url("$CFG->wwwroot/pluginfile.php",
                        '/'. $file->get_contextid(). '/'. $file->get_component(). '/'.
                        $file->get_filearea(). $file->get_filepath(). $file->get_filename(), !$isimage);
                if (!$isimage) {
                    $url = $CFG->wwwroot."/theme/saga/pix/coursenoimage.jpg";
                }
            }
            
            $content .= '<div class="view view-second view-mycourse '.(($x%2==0)?'view-nomargin':'').'">
                            <img src="'.$url.'" />
                            <div class="mask"></div>
                            <div class="content">
                                <h2>'.$title.'</h2>
                                <a href="'.$CFG->wwwroot.'/course/view.php?id='.$courselist[$x-1].'" class="info">Enter</a>
                            </div>
                        </div>';
        }
                    
        return $content;
    }
    
    protected function banner(){
        $showbanner = get_config('theme_saga', 'showbanner');
        $bannerurl = get_config('theme_saga', 'bannerurl');
        
        $content = "";
        
        if($showbanner){
            $content = '<div id="bannerRow" class="row">
                            <div class="sklt-container">
                                <div class="full columns alpha omega" id="banner">
                                    <center>
                                        <img src="'.$bannerurl.'"/>
                                    </center>
                                </div>
                            </div>
                        </div>';
        }
        
        return $content;
    }
    
    protected function html_blocks(){
        $showhtmlblocks              = get_config('theme_saga','showhtmlblocks');
        
        $content = '';
        
        if($showhtmlblocks){
            $htmlblock1title             = get_config('theme_saga','htmlblock1title');
            $htmlblock1                  = get_config('theme_saga','htmlblock1');
            $htmlblock2title             = get_config('theme_saga','htmlblock2title');
            $htmlblock2                  = get_config('theme_saga','htmlblock2');
            
            $content = '<div class="row">
                            <div class="sklt-container">
                                <div class="eight columns alpha">
                                    <h1>'.$htmlblock1title.'</h1>
                                    <div>'.$htmlblock1.'</div>
                                </div>
                                <div class="eight columns omega">
                                    <h1>'.$htmlblock2title.'</h1>
                                    <div>'.$htmlblock2.'</div>
                                </div>
                            </div>
                        </div>';
        }
        return $content;
    }
    
    protected function featuredcourses($CFG){
        $showfeaturedcourses = get_config("theme_saga","showfeaturedcourses");
        $content = '';

        if($showfeaturedcourses){
            $featuredcourses = get_config('theme_saga', 'featuredcourses');
            $courselist = json_decode($featuredcourses);
            $content = '<div id="featuredCoursesRow" class="row">
                            <div class="sklt-container">
                                <div class="thirteen columns alpha" id="featuredCourses">
                                    Featured Courses
                                </div>
                                <div class="three columns omega" id="featuredCoursesNav">';

            if(sizeof($courselist)>3){
                $content .= '<div id="featuredCoursesNavRight">></div>                                    
                                    <div id="featuredCoursesNavLeft" class="navBlocked"><</div>';
            }
            
            $content .= '</div>
                                <div class="full columns alpha omega">';

            $coursesinline = 4;

            for($x=1;$x<=sizeof($courselist);$x++){
                $course = get_course($courselist[$x-1]);
                $title = $course->fullname;
                
                if ($course instanceof stdClass) {
                    require_once($CFG->libdir. '/coursecatlib.php');
                    $course = new course_in_list($course);
                }

                $url = $CFG->wwwroot."/theme/saga/pix/coursenoimage.jpg";
                foreach ($course->get_course_overviewfiles() as $file) {
                    $isimage = $file->is_valid_image();
                    $url = file_encode_url("$CFG->wwwroot/pluginfile.php",
                            '/'. $file->get_contextid(). '/'. $file->get_component(). '/'.
                            $file->get_filearea(). $file->get_filepath(). $file->get_filename(), !$isimage);
                    if (!$isimage) {
                        $url = $CFG->wwwroot."/theme/saga/pix/coursenoimage.jpg";
                    }
                }

                $align = "";
                if($x % $coursesinline == 1)
                    $align = "alpha";
                else if($x % $coursesinline == 0)
                    $align = "omega";

                $content .= '<div class="view view-second" '.(($x>3)?'style="display:none"':'').'>
                                <img src="'.$url.'" />
                                <div class="mask"></div>
                                <div class="content">
                                    <h2>'.$title.'</h2>
                                    <a href="'.$CFG->wwwroot.'/course/view.php?id='.$courselist[$x-1].'" class="info">Read More</a>
                                </div>
                            </div>';
            }
            
            $content .= '</div></div></div>';
        }
        
        return $content;
    }
    
    protected function menu($wwwroot){
        $menuitems = json_decode(get_config('theme_saga', 'menudata'));
        $fixedmenu = get_config('theme_saga','fixedmenu');
        
        $content = '<div id="menubar" class="row'.(($fixedmenu)?' canfix':'').'">
            <div class="sklt-container">
                <div class="four columns">
                    <a href="'.$wwwroot.'">
                        '.$this->logo('header').'
                    </a>
                </div>
                <div class="twelve columns omega" style="float: right;">';
                    
                
        
        $content .= '<nav id="menu">';
        
        for($x=0;$x<sizeof($menuitems);$x++){
            $content .= '<a href="'.$menuitems[$x]->link.'">'.$menuitems[$x]->text.'</a>';
        }
        
        $content .= '</nav>';
        
        $content .= '</div>
            </div>
        </div>';
        
        return $content;
    }

    protected function searchbar($wwwroot){
        $searchbar = get_config('theme_saga', 'searchbar');

        if($searchbar && isloggedin()){
            $content = '<div id="sb-search" class="sb-search">
                            <form action="'.$wwwroot.'/admin/search.php" method="GET">
                                <input class="sb-search-input" placeholder="Enter your search ..." type="text" value="" name="query" id="search">
                                <input class="sb-search-submit" type="submit" value="">
                                <span class="sb-icon-search"></span>
                            </form>
                        </div>';
        }else{
            $content = " ";
        }
        
        return $content;
    }
    
    public function logo($area){
        $logourl = get_config('theme_saga', 'logourl');
        $content = '';
        if(!$logourl || trim($logourl)=="")
            $content = '<div id="defaultlogo" class="'.(($area=='header')?'logoleft':'logocenter').'"></div>';
        else{
            $content = '<div id="logo" class="'.(($area=='header')?'logoleft':'logocenter').'"></div>';
        }
        return $content;
    }

    public function favicon() {
        $faviconurl = get_config('theme_saga', 'faviconurl');
        if(!$faviconurl || trim($faviconurl)=="")
            $faviconurl = $this->page->theme->pix_url('favicon', 'theme');
        return $faviconurl;
    }

    protected function socialicons($area){
        $hassocialicons = get_config('theme_saga', $area.'socialicon');
        
        $social_facebook = get_config('theme_saga','social_facebook');
        $social_twitter = get_config('theme_saga','social_twitter');
        $social_gplus = get_config('theme_saga','social_gplus');
        $social_youtube = get_config('theme_saga','social_youtube');
        $social_vimeo =  get_config('theme_saga','social_vimeo');
        $social_wordpress = get_config('theme_saga','social_wordpress');
        $social_pinterest = get_config('theme_saga','social_pinterest');
        $social_flickr = get_config('theme_saga','social_flickr');
        $social_rss = get_config('theme_saga','social_rss');
        $social_dribbble = get_config('theme_saga','social_dribbble');
        $social_linkedin = get_config('theme_saga','social_linkedin');
        $social_tumblr = get_config('theme_saga','social_tumblr');
        $social_behance = get_config('theme_saga','social_behance');
        
        $content = '';
        
        if($hassocialicons){
            if(isset($social_facebook) && trim($social_facebook)!="")
                $content .= '<a href="'.$social_facebook.'" target="blank"><div class="facebooksocial iconsocial"></div></a>';
            if(isset($social_twitter) && trim($social_twitter)!="")
                $content .= '<a href="'.$social_twitter.'" target="blank"><div class="twittersocial iconsocial"></div></a>';
            if(isset($social_gplus) && trim($social_gplus)!="")
                $content .= '<a href="'.$social_gplus.'" target="blank"><div class="gplussocial iconsocial"></div></a>';
            if(isset($social_youtube) && trim($social_youtube)!="")
                $content .= '<a href="'.$social_youtube.'" target="blank"><div class="youtubesocial iconsocial"></div></a>';
            if(isset($social_vimeo) && trim($social_vimeo)!="")
                $content .= '<a href="'.$social_vimeo.'" target="blank"><div class="vimeosocial iconsocial"></div></a>';
            if(isset($social_wordpress) && trim($social_wordpress)!="")
                $content .= '<a href="'.$social_wordpress.'" target="blank"><div class="wordpresssocial iconsocial"></div></a>';
            if(isset($social_pinterest) && trim($social_pinterest)!="")
                $content .= '<a href="'.$social_pinterest.'" target="blank"><div class="pinterestsocial iconsocial"></div></a>';
            if(isset($social_flickr) && trim($social_flickr)!="")
                $content .= '<a href="'.$social_flickr.'" target="blank"><div class="flickrsocial iconsocial"></div></a>';
            if(isset($social_rss) && trim($social_rss)!="")
                $content .= '<a href="'.$social_rss.'" target="blank"><div class="rsssocial iconsocial"></div></a>';
            if(isset($social_dribbble) && trim($social_dribbble)!="")
                $content .= '<a href="'.$social_dribbble.'" target="blank"><div class="dribbblesocial iconsocial"></div></a>';
            if(isset($social_linkedin) && trim($social_linkedin)!="")
                $content .= '<a href="'.$social_linkedin.'" target="blank"><div class="linkedinsocial iconsocial"></div></a>';
            if(isset($social_tumblr) && trim($social_tumblr)!="")
                $content .= '<a href="'.$social_tumblr.'" target="blank"><div class="tumblrsocial iconsocial"></div></a>';            
            if(isset($social_behance) && trim($social_behance)!="")
                $content .= '<a href="'.$social_behance.'" target="blank"><div class="behancesocial iconsocial"></div></a>';            
        } 
        
        $content .= ' ';
        
        return $content;
    }

    protected function slider(){
        $slider = get_config('theme_saga', 'slider');
        
        if($slider){
            $slideritems = json_decode(get_config('theme_saga', 'slideshowdata'));
            $sliderEffects = array('<div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">','<div class="sl-slide" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15" data-slice1-scale="1.5" data-slice2-scale="1.5">');        

            $content = '<div id="sliderarea" class="row">';

            $content .= '<div id="slider" class="sl-slider-wrapper">
                            <div class="sl-slider">';

            for($x=0;$x<sizeof($slideritems);$x++){
                $content .= $sliderEffects[$x%2];
                $content .= '<div class="sl-slide-inner">
                                    <div class="bg-img bg-img-'.($x+1).'"></div>
                                    <div class="title"><cite>'.$slideritems[$x]->title.'</cite></div>
                                    <blockquote><p>'.$slideritems[$x]->description.'</p></blockquote>
                                </div>
                            </div>';
            } 

            $content .= '</div>

                            <nav id="nav-arrows" class="nav-arrows">
                                <span class="nav-arrow-prev">Previous</span>
                                <span class="nav-arrow-next">Next</span>
                            </nav>
                        </div>

                            <nav id="nav-dots" class="nav-dots">
                                <span class="nav-dot-current"></span>';

            for($x=1;$x<sizeof($slideritems);$x++)
                $content .= '<span></span>';

            $content .= '</nav>';
            $content .= '</div>';
        }else{
            $content = '';
        }
        
        return $content;
    }

    protected function forcefooter($page = 'login'){
        $code = '<style type="text/css">
                    @media screen and (min-height: 600px){
                       #footer,#footerend{
                           position: absolute !important;
                       }
                    }
                </style>';
        if($page=='login'){
            echo $code;
        }else{
            echo '<pre>';
            $url = explode("/",$_SERVER['PHP_SELF']);
            $page = $url[sizeof($url)-1];
            if($page=='logout.php') echo $code;
            echo '</pre>';
        }
    }
    
    protected function render_navigation_node(navigation_node $item) {
        $content = $item->get_content();
        $title = $item->get_title();
        if ($item->icon instanceof renderable && !$item->hideicon) {
            if(trim($content) == 'Saga')
                $item->icon->pix = 'g/saga_'.get_config('theme_saga','themecolor');
            $icon = $this->render($item->icon);
            if(trim($content) == 'Saga')
                $content = '<b>Saga</b>';
            $content = $icon.$content; // use CSS for spacing of icons
        }
        if ($item->helpbutton !== null) {
            $content = trim($item->helpbutton).html_writer::tag('span', $content, array('class'=>'clearhelpbutton', 'tabindex'=>'0'));
        }
        if ($content === '') {
            return '';
        }
        if ($item->action instanceof action_link) {
            $link = $item->action;
            if ($item->hidden) {
                $link->add_class('dimmed');
            }
            if (!empty($content)) {
                // Providing there is content we will use that for the link content.
                $link->text = $content;
            }
            $content = $this->render($link);
        } else if ($item->action instanceof moodle_url) {
            $attributes = array();
            if ($title !== '') {
                $attributes['title'] = $title;
            }
            if ($item->hidden) {
                $attributes['class'] = 'dimmed_text';
            }
            $content = html_writer::link($item->action, $content, $attributes);

        } else if (is_string($item->action) || empty($item->action)) {
            $attributes = array('tabindex'=>'0'); //add tab support to span but still maintain character stream sequence.
            if ($title !== '') {
                $attributes['title'] = $title;
            }
            if ($item->hidden) {
                $attributes['class'] = 'dimmed_text';
            }
            $content = html_writer::tag('span', $content, $attributes);
        }
        return $content;
    }
}
?>
