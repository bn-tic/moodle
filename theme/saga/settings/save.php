<?php
    include("../../../config.php");

    /* POSTs */
    $general                = $_POST["general"];
    $header                 = $_POST["header"];
    $footer                 = $_POST["footer"];
    $footermod_aboutus      = $_POST["footermod_aboutus"];
    $footermod_links        = $_POST["footermod_links"];
    $footermod_contact      = $_POST["footermod_contact"];
    $footermod_image        = $_POST["footermod_image"];
    $footermod_notice       = $_POST["footermod_notice"];
    $frontpage              = $_POST["frontpage"];
    $social                 = $_POST["social"];
    
    /* Saving Configs */
    
    #GENERAL
    set_config('themecolor',$general["themecolor"],'theme_saga');
    set_config('generalsidebar',$general["generalsidebar"],'theme_saga');
    set_config('logourl',$general["logourl"],'theme_saga');
    set_config('faviconurl',$general["faviconurl"],'theme_saga');
    set_config('fixedmenu',$general["fixedmenu"],'theme_saga');
    
    #HEADER
    set_config('headersocialicon',$header["headersocialicon"],'theme_saga');
    if(isset($header["menudata"]))
        set_config('menudata',json_encode($header["menudata"]),'theme_saga');
    set_config('searchbar',$header["searchbar"],'theme_saga');
    
    #FOOTER
    set_config('footermodule1',$footer["footermodule1"],'theme_saga');
    set_config('footermodule2',$footer["footermodule2"],'theme_saga');
    set_config('footermodule3',$footer["footermodule3"],'theme_saga');
    set_config('footermodule4',$footer["footermodule4"],'theme_saga');
    
    #FOOTER MOD ABOUT US
    set_config('footermod_aboutus_whitelogo',$footermod_aboutus["footermod_aboutus_whitelogo"],'theme_saga');
    set_config('footermod_aboutus_text',$footermod_aboutus["footermod_aboutus_text"],'theme_saga');
    
    #FOOTER MOD LINKS
    if(isset($footermod_links["footermod_links"]))
        set_config('footermod_links',json_encode($footermod_links["footermod_links"]),'theme_saga');
    
    #FOOTER MOD CONTACT
    set_config('footermod_contact_address',$footermod_contact["footermod_contact_address"],'theme_saga');
    set_config('footermod_contact_city',$footermod_contact["footermod_contact_city"],'theme_saga');
    set_config('footermod_contact_phone',$footermod_contact["footermod_contact_phone"],'theme_saga');
    set_config('footermod_contact_mail',$footermod_contact["footermod_contact_mail"],'theme_saga');
    
    #FOOTER MOD IMAGE
    set_config('footermod_image_title',$footermod_image["footermod_image_title"],'theme_saga');
    set_config('footermod_image_url',$footermod_image["footermod_image_url"],'theme_saga');
    
    #FOOTER NOTICE
    set_config('footermod_notice_title',$footermod_notice["footermod_notice_title"],'theme_saga');
    set_config('footermod_notice_text',$footermod_notice["footermod_notice_text"],'theme_saga');
    
    #FRONTPAGE
    set_config('slider',$frontpage["slider"],'theme_saga');
    if(isset($frontpage["slideshowdata"]))
        set_config('slideshowdata',json_encode($frontpage["slideshowdata"]),'theme_saga');
    set_config('showfeaturedcourses',$frontpage["showfeaturedcourses"],'theme_saga'); 
    if(isset($frontpage["featuredcourses"]))
        set_config('featuredcourses',  json_encode($frontpage["featuredcourses"]),'theme_saga');
    set_config('showbanner',$frontpage["showbanner"],'theme_saga');
    set_config('bannerurl',$frontpage["bannerurl"],'theme_saga');
    
    set_config('showhtmlblocks',$frontpage["showhtmlblocks"],'theme_saga');
    set_config('htmlblock1title',$frontpage["htmlblock1title"],'theme_saga');
    set_config('htmlblock1',$frontpage["htmlblock1"],'theme_saga');
    set_config('htmlblock2title',$frontpage["htmlblock2title"],'theme_saga');
    set_config('htmlblock2',$frontpage["htmlblock2"],'theme_saga');
    
    #SOCIAL
    set_config('social_rss',$social["social_rss"],'theme_saga');
    set_config('social_twitter',$social["social_twitter"],'theme_saga');
    set_config('social_dribbble',$social["social_dribbble"],'theme_saga');
    set_config('social_vimeo',$social["social_vimeo"],'theme_saga');
    set_config('social_facebook',$social["social_facebook"],'theme_saga');
    set_config('social_youtube',$social["social_youtube"],'theme_saga');
    set_config('social_flickr',$social["social_flickr"],'theme_saga');
    set_config('social_gplus',$social["social_gplus"],'theme_saga');
    set_config('social_linkedin',$social["social_linkedin"],'theme_saga');
    set_config('social_tumblr',$social["social_tumblr"],'theme_saga');
    set_config('social_behance',$social["social_behance"],'theme_saga');
    set_config('social_wordpress',$social["social_wordpress"],'theme_saga');
    set_config('social_pinterest',$social["social_pinterest"],'theme_saga');
    
    redirect('index.php');
?>
