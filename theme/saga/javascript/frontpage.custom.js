$(function() {
    /* Slideshow */
    var Page = (function() {
        var $navArrows = $( '#nav-arrows' ),
        $nav = $( '#nav-dots > span' ),
        slitslider = $( '#slider' ).slitslider( {
            onBeforeChange : function( slide, pos ) {
                $nav.removeClass( 'nav-dot-current' );
                $nav.eq( pos ).addClass( 'nav-dot-current' );
            }
        }),
        init = function(){
            initEvents();
        },
        initEvents = function() {
            // add navigation events
            $navArrows.children( ':last' ).on( 'click', function() {
                slitslider.next();
                return false;
            });
            $navArrows.children( ':first' ).on( 'click', function() {
                slitslider.previous();
                return false;
            });

            $nav.each( function( i ) {
                $( this ).on( 'click', function( event ) {
                    var $dot = $( this );
                        if( !slitslider.isActive() ) {
                            $nav.removeClass( 'nav-dot-current' );
                            $dot.addClass( 'nav-dot-current' );
                        }
                        slitslider.jump( i + 1 );
                        return false;
                });
            });
        };
        return { init : init };
    })();
    Page.init();
    
    /* Featured Courses */
    $("#featuredCoursesNavRight").click(function(){
       if(!$(this).hasClass('navBlocked')){
            var first = false;
            var lastEq = 0;
            $(".view").each(function(){
                if($(this).css('display')!='none' && !first){
                    index = $(this).index();
                    $(this).hide();
                    lastEq = index+3;
                    first = true;
                    if((index+4) == $(".view").length) $("#featuredCoursesNavRight").addClass("navBlocked");
                }
            });
            $(".view").eq(lastEq).show();
            $("#featuredCoursesNavLeft").removeClass("navBlocked");
       }
    });
    $("#featuredCoursesNavLeft").click(function(){
        if(!$(this).hasClass('navBlocked')){
            var first = false;
            var Eq = 0;
            $(".view").each(function(){
                if($(this).css('display')!='none' && !first){
                    index = $(this).index();
                    $(".view").eq(index-1).show();
                    $(".view").eq(index+2).hide();
                    first = true;
                     if(index == 1) $("#featuredCoursesNavLeft").addClass("navBlocked");
                }
            });
            $("#featuredCoursesNavRight").removeClass("navBlocked");
        }
    });
    
    
    var nav = $('.canfix');

    $(window).scroll(function () {
        if ($(this).scrollTop() > 33) {
            nav.addClass('menubar-fixed');
        } else {
            nav.removeClass('menubar-fixed');
        }
    });
});

new UISearch( document.getElementById( 'sb-search' ) );