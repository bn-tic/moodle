<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Parent theme: Bootstrapbase by Bas Brands
 * Built on: Essential by Julian Ridden
 *
 * @package   theme_accadem
 * @copyright 2014 redPIthemes
 *
 */
 
$hasfrontpageblocks = ($PAGE->blocks->region_has_content('side-pre', $OUTPUT) || $PAGE->blocks->region_has_content('side-post', $OUTPUT));
$carousel_pos = $PAGE->theme->settings->carousel_position;
$haslogo = (!empty($PAGE->theme->settings->logo));
$standardlayout = (empty($PAGE->theme->settings->layout)) ? false : $PAGE->theme->settings->layout;

if (right_to_left()) {
    $regionbsid = 'region-bs-main-and-post';
} else {
    $regionbsid = 'region-bs-main-and-pre';
}

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <noscript>
			<link rel="stylesheet" type="text/css" href="<?php echo $CFG->wwwroot;?>/theme/accadem/style/nojs.css" />
	</noscript>
    <!-- Google web fonts -->
    <?php require_once(dirname(__FILE__).'/includes/fonts.php'); ?>
</head>

<body <?php echo $OUTPUT->body_attributes('two-column'); ?>>

<?php echo $OUTPUT->standard_top_of_body_html() ?>

<div id="wrapper">
<?php require_once(dirname(__FILE__).'/includes/header.php'); ?>

<div class="text-center" style="line-height:1em;">
	<img src="<?php echo $CFG->wwwroot;?>/theme/accadem/pix/bg/shadow.png" class="slidershadow frontpage-shadow" alt="">
</div>

<div id="page" class="container-fluid">

<?php require_once(dirname(__FILE__).'/includes/slideshow.php');?>

    <div id="page-content" class="row-fluid">
		<!--div class="text-box">
			<div class="text-box-heading">NUESTROS CURSOS</div>
			<div class="arrow-down"></div>
			
			<div class="row">
				<div class="col-sm-12">
				
					<div class="iconbox span3">
						<div class="iconcircle">
						<i class="fa fa-folder-open-o"></i>						
						</div>
						<div class="iconbox-content">
							<h4>CAMPUS ADMINISTRACI&Oacute;N</h4>
							<div>
								<ul class="list-style-3 colored">
								<li>link</li>
								<li>link</li>
								<li>link</li>
								</ul>
							</div>
						</div>
					</div>

					<div class="iconbox span3">
						<div class="iconcircle">
						<i class="fa fa-balance-scale"></i>
						</div>
						<div class="iconbox-content">
						<h4>CAMPUS JUSTICIA</h4>
							<div>
								<ul class="list-style-3 colored">
								<li>link</li>
								<li>link</li>
								<li>link</li>
								</ul>
							</div>
						</div>
					</div>

					<div class="iconbox span3">
						<div class="iconcircle">
						<i class="fa fa-ambulance"></i>
						</div>
						<div class="iconbox-content">
							<h4>CAMPUS SANITARIOS</h4>
							<div>
								<ul class="list-style-3 colored">
								<li>link</li>
								<li>link</li>
								<li>link</li>
								</ul>
							</div>
						</div>
					</div>

					<div class="iconbox span3">
						<div class="iconcircle">
						<i class="fa fa-graduation-cap"></i>
						</div>
						<div class="iconbox-content">
							<h4>CAMPUS EDUCACI&Oacute;N</h4>
							<div>
								<ul class="list-style-3 colored">
								<li>link</li>
								<li>link</li>
								<li>link</li>
								</ul>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div-->
		<!-- banner cep online -->
		<!--div class="row-fluid">
			<div class="col-sm-12">
				<div class="spotlight spotlight-v3">
					<h2>CEP ONLINE</h2>
					<div class="blocknumber_box">
						<div class="blocknumber_icon">
							<i class="fa fa-info"></i>
						</div>
						<div class="blocknumber_content text-justify">
							<p>En CEP tenemos la mayor oferta del mercado en libros y cursos para la preparación de oposiciones.</p>
							<p>Puedes hacerlo tanto de forma específica, con una oferta concreta (Auxiliares Administrativos del SAS), como transversal con la que podrías optar a múltiples ofertas dentro de la categoría que elijas (Auxiliares Administrativos Estado, de Servicios de Salud, de Juntas de las Comunidades Autónomas, Diputaciones, Ayuntamientos, Organismos, etc).</p>
						</div>
					</div>
				</div>
			</div>
		</div-->

		<?php if(!isloggedin()){?>
		<!-- login -->
		<div class="row-fluid">
			<div class="col-sm-12">
			<div class="spotlight spotlight-v2 spotlight-full" style="text-align:center;padding-top: 25px;padding-bottom: 25px;">
			<h4 style="display: inline-block;vertical-align: middle;"><i class="fa fa-sign-in"></i> Entra al campus
			<!--small>Acceso a la plataforma</small--></h4>
			<span style="text-transform: uppercase;font-weight: 600;display: inline-block;margin-left: 30px;">
			<a class="btn btn-primary" href="<?php echo $CFG->wwwroot; ?>/login" style="padding: 12px 18px;line-height: 1.5rem;"> LOGIN</a></span>
			</div>
			</div>
		</div>		
		<?php }?>
    	<?php if ($hasfrontpageblocks==1) { ?>
            <div id="<?php echo $regionbsid ?>" class="span9">
            <div class="row-fluid">
            	<?php if ($standardlayout) { ?>
                <section id="region-main" class="span8 pull-right">
                <?php } else { ?>
                <section id="region-main" class="span8">
                <?php } ?>
        <?php } else { ?>
        	<div id="<?php echo $regionbsid ?>">
            <div class="row-fluid">
            	<section id="region-main" class="span12">
        <?php } ?>
            	<?php
            		echo $OUTPUT->course_content_header();
					if ($carousel_pos=='0') require_once(dirname(__FILE__).'/includes/carousel.php');
            		echo $OUTPUT->main_content();
            		echo $OUTPUT->course_content_footer();
					if ($carousel_pos=='1') require_once(dirname(__FILE__).'/includes/carousel.php');
            	?>
        		</section>
        	<?php
        	if ($hasfrontpageblocks==1) { 
				if ($standardlayout) {echo $OUTPUT->blocks('side-pre', 'span4 desktop-first-column pull-left');}
				else {echo $OUTPUT->blocks('side-pre', 'span4 desktop-first-column pull-right');}
			} ?>
            </div>
        	</div>
        <?php echo $OUTPUT->blocks('side-post', 'span3'); ?>
    </div>

    <?php if (is_siteadmin()) { ?>
	<div class="hidden-blocks">
    	<div class="row-fluid">
        	<h4><?php echo get_string('visibleadminonly', 'theme_accadem') ?></h4>
            <?php
                echo $OUTPUT->blocks('hidden-dock');
            ?>
    	</div>
	</div>
	<?php } ?>

	<a href="#top" class="back-to-top"><i class="fa fa-chevron-circle-up fa-3x"></i><p><?php print_string('backtotop', 'theme_accadem'); ?></p></a>

</div>

	<footer id="page-footer" class="container-fluid">
		<?php require_once(dirname(__FILE__).'/includes/footer.php'); ?>
	</footer>

    <?php echo $OUTPUT->standard_end_of_body_html() ?>


<!--[if lte IE 9]>
<script src="<?php echo $CFG->wwwroot;?>/theme/accadem/javascript/ie/iefix.js"></script>
<![endif]-->


<script>
jQuery(document).ready(function ($) {
$('.navbar .dropdown').hover(function() {
	$(this).addClass('extra-nav-class').find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();
}, function() {
	var na = $(this)
	na.find('.dropdown-menu').first().stop(true, true).delay(100).slideUp('fast', function(){ na.removeClass('extra-nav-class') })
});

});

jQuery(document).ready(function() {
    var offset = 220;
    var duration = 500;
    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() > offset) {
            jQuery('.back-to-top').fadeIn(duration);
        } else {
            jQuery('.back-to-top').fadeOut(duration);
        }
    });
    
    jQuery('.back-to-top').click(function(event) {
        event.preventDefault();
        jQuery('html, body').animate({scrollTop: 0}, duration);
        return false;
    })
});

<?php if ($hasslideshow) { ?>
	jQuery(function(){
		jQuery('#camera_wrap').camera({
			fx: '<?php echo $imgfx; ?>',
			height: 'auto',
			loader: '<?php echo $loader; ?>',
			thumbnails: false,
			pagination: false,
			autoAdvance: <?php echo $advance; ?>,
			hover: false,
			navigationHover: <?php echo $navhover; ?>,
			opacityOnGrid: false
		});
	});
<?php } ?>

<?php if ($hascarousel) { ?>
	jQuery(document).ready(function(){
  		jQuery('.slider1').bxSlider({
			pager: false,
			nextSelector: '#slider-next',
			prevSelector: '#slider-prev',
			nextText: '>',
			prevText: '<',
			slideWidth: 240,
    		minSlides: 1,
    		maxSlides: 6,
			moveSlides: 1,
    		slideMargin: 10			
  		});
	});
<?php } ?>

</script>

</body>
</html>