<?php

$currentDir = getcwd();
$errors = []; // Store all foreseen and unforseen errors here
$fileExtensions = ['jpeg', 'jpg', 'png', 'html']; // Get all the file extensions
//get content
$header = file_get_contents("scorm/templates/header.html");
$footer = file_get_contents("scorm/templates/footer.html");
$box_html = file_get_contents("scorm/resources/box.html");


if (isset($_POST['submit'])) {

    //upload files
    $countfiles = count($_FILES['file']['name']);
    for ($i = 0; $i < $countfiles; $i++) {
        $fileName = str_replace(" ", "_", $_FILES['file']['name'][$i]);
        $fileSize = $_FILES['file']['size'][$i];
        $fileTmpName = $_FILES['file']['tmp_name'][$i];
        $fileType = $_FILES['file']['type'][$i];
        $extension = explode('.', $fileName);
        $fileExtension = strtolower(end($extension));

        if ($fileExtension == "html") {
            $uploadDirectory = "/scorm/";
            $zipName = explode('.', $fileName);
        } else
            $uploadDirectory = "/scorm/image/";

        $uploadPath = $currentDir . $uploadDirectory . basename($fileName);

        if (isset($_POST['submit'])) {

            if (!in_array($fileExtension, $fileExtensions)) {
                $errors[] = "La extensión de los ficheros debe ser 'html, png, jpg o jpeg'";
            }

            /* if ($fileSize > 2000000) {
              $errors[] = "This file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
              } */

            if (empty($errors)) {
                $didUpload = move_uploaded_file($fileTmpName, $uploadPath);

                if ($didUpload) {
                    if ($fileExtension == "html") {
                        //get content
                        $content = file_get_contents("scorm/{$fileName}");

                        //loadHtml
                        $dom = new DOMDocument;
                        $dom->loadHTML($content);

                        //all tags
                        $tags = array('h3', 'h4', 'h2', 'p');
                        $texts = array();

                        //read tags
                        foreach ($tags as $tag) {
                            $elementList = $dom->getElementsByTagName($tag);
                            foreach ($elementList as $element) {
                                $texts[$element->tagName][] = $element->textContent;
                            }
                        }

                        //get string between
                        function get_string_between($string, $start, $end) {
                            $string = ' ' . $string;
                            $ini = strpos($string, $start);
                            if ($ini == 0)
                                return '';
                            $ini += strlen($start);
                            $len = strpos($string, $end, $ini) - $ini;
                            return substr($string, $ini, $len);
                        }
                        
                        //pretty json
                        function indent($json) {
                            $result = '';
                            $pos = 0;
                            $strLen = strlen($json);
                            $indentStr = '  ';
                            $newLine = "\n";
                            $prevChar = '';
                            $outOfQuotes = true;

                            for ($i=0; $i<=$strLen; $i++) {

                                // Grab the next character in the string.
                                $char = substr($json, $i, 1);

                                // Are we inside a quoted string?
                                if ($char == '"' && $prevChar != '\\') {
                                    $outOfQuotes = !$outOfQuotes;

                                // If this character is the end of an element,
                                // output a new line and indent the next line.
                                } else if(($char == '}' || $char == ']') && $outOfQuotes) {
                                    $result .= $newLine;
                                    $pos --;
                                    for ($j=0; $j<$pos; $j++) {
                                        $result .= $indentStr;
                                    }
                                }

                                // Add the character to the result string.
                                $result .= $char;

                                // If the last character was the beginning of an element,
                                // output a new line and indent the next line.
                                if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
                                    $result .= $newLine;
                                    if ($char == '{' || $char == '[') {
                                        $pos ++;
                                    }

                                    for ($j = 0; $j < $pos; $j++) {
                                        $result .= $indentStr;
                                    }
                                }

                                $prevChar = $char;
                            }

                            return $result;
                        }

                        //set styles
                        $content = str_replace("&#9;", " ", $content);
                        //text-justify
                        $content = str_replace('"comun"', '"text-justify"', $content);
                        //img-responsive
                        $content = str_replace("_idGenObjectAttribute", "img-fluid rounded ", $content);
                        //boxes
                        $content = str_replace("guion-1", "list-group-item", $content);
                        //tables
                        $content = str_replace("TableStyle", " table table-bordered table-striped ", $content);
                        
                        //introduction file
                        $count = 0;
                        //items for xml 
                        $items = "";
                        //subcontent control
                        $last = 1;
                        //resources for xml 
                        $resources = "";
                        $file = "index_" . $count . ".html";

                        
                        //graph
                        $childs = "";
                        $content_graph = "<div id='tree-container'></div>";
                        
                        $graph_content = $header. $content_graph .$footer;
                        file_put_contents("scorm/". "esquema.html", $graph_content);
                        $items .= "<item identifier='SCOGRAPH' identifierref='SCOGRAPH'>
                                <title>Esquema</title>
                              </item>";

                        $resources .= "<resource identifier='SCOGRAPH' type='webcontent' adlcp:scormtype='sco' href='esquema.html'>
                                        <file href='esquema.html' />
                                    </resource>";
                        
                        //introduction
                        $file_content = $header . '<p class="tema">' . get_string_between($content, '<p class="tema">', '<h2 class="_-">1') . $footer;
                        file_put_contents("scorm/" . $file, $file_content);
                        $items .= "<item identifier='SCO{$count}' identifierref='SCO{$count}'>
                                <title>Introducción</title>
                              </item>";

                        $resources .= "<resource identifier='SCO{$count}' type='webcontent' adlcp:scormtype='sco' href='index_{$count}.html'>
                                        <file href='index_{$count}.html' />
                                    </resource>";

                        //rename image path 
                        $content = preg_replace('/="\S*resources\//', '="', $content);
                        
                        //theme files subcontent
                        for ($j=0; $j < (count($texts['h3'])); $j++) {

                            $index = trim(substr($texts['h3'][$j], 0, 1));
                            $sub_index = trim(substr($texts['h3'][$j], 1, 3));
                            $next_sub_index = trim(substr($texts['h3'][$j+1], 1, 3));
                            
                            if($j == count($texts['h3'])-1){
                                $filesubcontent = $header . '<h3 class="_-1">' . $texts['h3'][$j] . '</h3>' . get_string_between($content, $texts['h3'][$j], '</body>') . $footer;

                                //info box
                                $title_box = get_string_between($filesubcontent, '<p class="titulo-cuadros"><span class="negrita">', '</span></p>');
                                $content_box = get_string_between($filesubcontent, '<p class="texto-cuadros">', '</p>');
                                
                                $filesubcontent = str_replace(get_string_between($filesubcontent, '<div class="_idGenObjectLayout-1">', '</div>'), $box_html, $filesubcontent);
                                $filesubcontent = str_replace(get_string_between($filesubcontent, '<div class="_idGenObjectLayout-2">', '</div>'), $box_html, $filesubcontent);
                                $filesubcontent = str_replace("title", $title_box, $filesubcontent);
                                $filesubcontent = str_replace("content", $content_box, $filesubcontent);
                                
                                if(substr($texts['h3'][$j], 2, 3) > 9)
                                    $subname = substr($texts['h3'][$j], 0, 4);
                                else
                                    $subname = substr($texts['h3'][$j], 0, 3);
                                
                                $subfile = "index_" .  trim($subname) . ".html";
                                file_put_contents("scorm/" . $subfile, $filesubcontent);
                            }
                            else{
                                //between point & point
                                for($i=1;$i<=count($texts['h2']);$i++){
                                    $point_content = get_string_between($content, '<h2 class="_-">'.$texts['h2'][$i-1].'</h2>', '<h2 class="_-">'.$texts['h2'][$i].'</h2>');
                                    $finded = strpos($point_content, 'h3');
                                    if($finded === false){
                                        if(strlen($point_content) > 20){
                                            $filesubcontent = $header . '<h3 class="_-">' . $texts['h2'][$i-1]. '</h3>' . $point_content . $footer;
                                            $subfile = "index_" .  ($i) . ".html";
                                            file_put_contents("scorm/" . $subfile, $filesubcontent);
                                        }
                                    }
                                }
                                
                                //between point & point.1
                                if($sub_index === ".1"){
                                    //anterior sin punto
                                    $aditional_content = get_string_between($content, '<h2 class="_-">'.$texts['h2'][$index-1].'</h2>', '<h3 class="_-1">'.$texts['h3'][$j].'</h3>');
                                    if(strlen($aditional_content) > 20){
                                        $filesubcontent = $header . '<h3 class="_-">' . $texts['h2'][$index-1]. '</h3>' . $aditional_content . $footer;
                                        $subfile = "index_" .  ($index) . ".html";
                                        file_put_contents("scorm/" . $subfile, $filesubcontent);
                                    }
                                }
                                
                                $filesubcontent = $header . '<h3 class="_-1">' . $texts['h3'][$j] . '</h3>' .get_string_between($content, $texts['h3'][$j], $texts['h3'][$j+1]) . $footer;
                                $subname = trim(substr($texts['h3'][$j], 0, 4));
                                
                                //info box
                                $title_box = get_string_between($filesubcontent, '<p class="titulo-cuadros"><span class="negrita">', '</span></p>');
                                $content_box = get_string_between($filesubcontent, '<p class="texto-cuadros">', '</p>');

                                $filesubcontent = str_replace(get_string_between($filesubcontent, '<div class="_idGenObjectLayout-1">', '</div>'), $box_html, $filesubcontent);
                                $filesubcontent = str_replace(get_string_between($filesubcontent, '<div class="_idGenObjectLayout-2">', '</div>'), $box_html, $filesubcontent);
                                $filesubcontent = str_replace("title", $title_box, $filesubcontent);
                                $filesubcontent = str_replace("content", $content_box, $filesubcontent);
                                //delete last h2
                                $filesubcontent = str_replace('<h2 class="_-">'.$texts['h2'][$index].'</h2>', "", $filesubcontent);
                                
                                $subfile = "index_" .  $subname . ".html";
                                file_put_contents("scorm/" . $subfile, $filesubcontent);
                            }

                            //save xml items
                            if($last != $index){
                                //end
                                $items .= "</item>";
                                $childs .= ",";
                            }
                            
                            if(($sub_index === '.1')){
                                $last = $index;
                                //start
                                //child1
                                $childs .= '{"name": "'.$texts['h2'][$index-1].'",';
                                $childs .= '"children": [{"name": "'.$texts['h3'][$j].'"},';
                                
                                //first document
                                if($j==0){
                                    if(file_exists("scorm/index_".($j+1).".html")){
                                    $items .= "<item identifier='{$texts['h2'][$j]}'><title>{$texts['h2'][$j]}</title>";
                                    $items .= "<item identifier='SCO".($j+1)."' identifierref='SCO".($j+1)."'>
                                        <title>{$texts['h2'][$j]}</title>
                                    </item></item>";
                                    $resources .= "<resource identifier='SCO".($j+1)."' type='webcontent' adlcp:scormtype='sco' href='index_".($j+1).".html'>
                                        <file href='index_".($j+1).".html' />
                                    </resource>";
                                    }
                                }
                                
                                $items .= "<item identifier='{$texts['h2'][$index-1]}'><title>{$texts['h2'][$index-1]}</title>";
                                
                                if(file_exists("scorm/index_".($index).".html")){
                                    $items .= "<item identifier='SCO".($index)."' identifierref='SCO".($index)."'>
                                        <title>{$texts['h2'][$index-1]}</title>
                                    </item>";
                                        
                                        $resources .= "<resource identifier='SCO".($index)."' type='webcontent' adlcp:scormtype='sco' href='index_".($index).".html'>
                                    <file href='index_".($index).".html' />
                                </resource>";
                                }
                                
                                $items .= "<item identifier='SCO{$subname}' identifierref='SCO{$subname}'>
                                    <title>{$texts['h3'][$j]}</title>
                                </item>";
                                    
                            }else{
                                $items .= "<item identifier='SCO{$subname}' identifierref='SCO{$subname}'>
                                    <title>{$texts['h3'][$j]}</title>
                                </item>";
                                //child2 graph
                                $childs .= '{"name": "'.$texts['h3'][$j].'"}';
                                if(($next_sub_index === '.1'))
                                    $childs .= "]}";
                                else
                                    $childs .= ",";
                                //end final point
                                if($j == count($texts['h3'])-1){
                                    $items .= "</item>";
                                }
                            }
                            
                            $resources .= "<resource identifier='SCO{$subname}' type='webcontent' adlcp:scormtype='sco' href='index_{$subname}.html'>
                                <file href='index_{$subname}.html' />
                            </resource>";
                        }
    
                        //json file
                        $flare_content = '{"name": "'.$fileName.'","children": ['.$childs.']}]}';
                        $flare_content = str_replace(",]}]}", "]}]}", $flare_content);
                        file_put_contents("scorm/flare.json", str_replace("	", " ", indent($flare_content)));
                        
                        //create manifest
                        $manifest = "<?xml version='1.0' encoding='UTF-8'?>
                            <!--Generated by CEP -->
                              <manifest identifier='CourseID' version='1.2'
                                        xmlns='http://www.imsproject.org/xsd/imscp_rootv1p1p2'
                                        xmlns:adlcp='http://www.adlnet.org/xsd/adlcp_rootv1p2'
                                        xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
                                        xsi:schemaLocation='http://www.imsproject.org/xsd/imscp_rootv1p1p2 imscp_rootv1p1p2.xsd
                                                            http://www.imsglobal.org/xsd/imsmd_rootv1p2p1 imsmd_rootv1p2p1.xsd
                                                            http://www.adlnet.org/xsd/adlcp_rootv1p2 adlcp_rootv1p2.xsd' >

                                <organizations default='CEP'>
                                  <organization identifier='{$fileName}'>
                                    <title>{$fileName}</title>
                                    {$items}
                                    <metadata>
                                      <schema>ADL SCORM</schema>
                                      <schemaversion>1.2</schemaversion>
                                    </metadata>
                                  </organization>
                                </organizations>
                              <resources>
                                  {$resources}
                                  <resource identifier='common_files' type='webcontent' adlcp:scormtype='asset'>
                                      <file href='templates/launchpage.html'/>
                                      <file href='js/highlight.js'/>
                                      <file href='js/scormfunctions.js'/>
                                      <file href='js/dndTree.js'/>
                                      <file href='css/style.css'/>
                                </resource>
                              </resources>
                          </manifest>";

                        file_put_contents("scorm/imsmanifest.xml", $manifest);

                        //create SCORM
                        $rootPath = realpath('scorm');
                        
                        //delete uploaded html
                        unlink("scorm/{$fileName}");

                        // Initialize archive object
                        $zip = new ZipArchive();
                        $zip->open("output/{$fileName}.zip", ZipArchive::CREATE | ZipArchive::OVERWRITE);

                        // Create recursive directory iterator
                        $files = new RecursiveIteratorIterator(
                                new RecursiveDirectoryIterator($rootPath), RecursiveIteratorIterator::LEAVES_ONLY
                        );

                        foreach ($files as $name => $file) {
                            // Skip directories (they would be added automatically)
                            if (!$file->isDir()) {
                                // Get real and relative path for current file
                                $filePath = $file->getRealPath();
                                $relativePath = substr($filePath, strlen($rootPath) + 1);

                                // Add current file to archive
                                $zip->addFile($filePath, $relativePath);
                            }
                        }

                        // Zip archive will be created only after closing object
                        $zip->close();

                        header('Content-type: application/zip');
                        header('Content-Disposition: attachment; filename="'.basename("output/{$fileName}.zip").'"');
                        header("Content-length: " . filesize("output/{$fileName}.zip"));
                        header("Pragma: no-cache");
                        header("Expires: 0");
                        ob_clean();
                        flush();
                        readfile("output/{$fileName}.zip");
//                        unlink("output/{$fileName}.zip");
        
                        //delete test.html & remove images folder
                        $images = glob('scorm/image/*');
                        foreach($images as $image){
                          if(is_file($image))
                              unlink($image);
                        }
                        unlink("scorm/imsmanifest.xml");
                        unlink("scorm/flare.json");
                        $indexs = glob('scorm/*.html');
                        foreach($indexs as $indx){
                          if(is_file($indx))
                              unlink($indx);
                        }
                    }
                    //echo "<script>alert('SCORM transformado correctamente')</script>";
                    //header('Refresh: 2; url=index.php');
                    //echo "El fichero " . basename($fileName) . " se subió con éxito";
                } else {
                    echo "<script>alert('Ocurrió un error mientras se subía el fichero')</script>";
                }
            } else {
                foreach ($errors as $error) {
                    echo $error . "Estos son los errores " . "\n";
                }
            }
        }
    }
}
  