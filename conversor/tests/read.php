<?php

$currentDir = getcwd();
$errors = []; // Store all foreseen and unforseen errors here
$fileExtensions = ['html']; // Get all the file extensions
/**
 * Zip file
 * @param type $source
 * @param type $destination
 * @param string $flag
 * @return boolean
 */
function zipFile($source, $destination, $flag = ''){
    if (!extension_loaded('zip') || !file_exists($source)) {
       return false;
    }

    $zip = new ZipArchive();
    if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
       return false;
    }

    $source = str_replace('\\', '/', realpath($source));
    if($flag)
    {
        $flag = basename($source) . '/';
    }

    if (is_dir($source) === true)
    {
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

        foreach ($files as $file)
        {

            $file = str_replace('\\', '/', realpath($file));

            if (strpos($flag.$file,$source) !== false) { // this will add only the folder we want to add in zip

                if (is_dir($file) === true)
                {
                    $zip->addEmptyDir(str_replace($source . '/', '', $flag.$file . '/'));

                }
                else if (is_file($file) === true)
                {
                    $zip->addFromString(str_replace($source . '/', '', $flag.$file), file_get_contents($file));
                }
            }
        }
    }
    else if (is_file($source) === true)
    {
        $zip->addFromString($flag.basename($source), file_get_contents($source));
    }

    return $zip->close();
}

/**
 * get string between two strings
 * @param string $string
 * @param type $start
 * @param type $end
 * @return string
 */
function get_string_between($string, $start, $end) {
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0)
        return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

if (isset($_POST['submit'])) {
    //upload files
    $countfiles = count($_FILES['file']['name']);
    for ($i = 0; $i < $countfiles; $i++) {
        $fileName = str_replace(" ", "_", $_FILES['file']['name'][$i]);
        $fileSize = $_FILES['file']['size'][$i];
        $fileTmpName = $_FILES['file']['tmp_name'][$i];
        $fileType = $_FILES['file']['type'][$i];
        $extension = explode('.', $fileName);
        $fileExtension = strtolower(end($extension));

        if ($fileExtension == "html") {
            $uploadDirectory = "/scorm/";
            $zipName = explode('.', $fileName);
        }

        $uploadPath = $currentDir . $uploadDirectory . basename($fileName);

        if (isset($_POST['submit'])) {

            if (!in_array($fileExtension, $fileExtensions)) {
                $errors[] = "La extensión de los ficheros deben ser 'html'";
            }
        }

        if (empty($errors)) {
            $didUpload = move_uploaded_file($fileTmpName, $uploadPath);

            if ($didUpload) {
                if ($fileExtension == "html") {
                    //get content
                    $content = file_get_contents("scorm/{$fileName}");
                    $content = str_replace("&#160;", " ", $content);
                    $content = str_replace('<span class="CELDA">&#9;¢&#9;</span>', " ", $content);
                    $content = str_replace('&#9;</p>', "</p>", $content);
                    $content = str_replace("&#9;", " ", $content);
                    $content = str_replace('<p class="reserva">PREGUNTAS DE RESERVA</p>', "", $content);
                    $content = str_replace('<p class="reserva">Preguntas de reserva</p>', "", $content);
                    
                    
                    //comprobamos los anchor
                    for($anchor=0;$anchor<10;$anchor++){
                        $content = str_replace('<a id="_idTextAnchor00'.$anchor.'"></a>', "", $content);
                    }
//loadHtml
                    $dom = new DOMDocument;
                    $dom->loadHTML($content);

                    $xpath = new DomXPath($dom);
                    
                    //supuesto práctico
                    $sspp = "";
                    //open assessment
                    $assessment = file_get_contents("scorm/shared/assessmenttemplate.html");
                    
                    if($xpath->query("//p[@class='TIT-SIMULACRO']")->length > 0){
                        $sspp = get_string_between($content, '<p class="TIT-SIMULACRO">Supuesto práctico</p>', '<p class="titulo-referencias">Cuestiones</p>');
                        $content = str_replace('<p class="TIT-PREGUNTAS-RESERVA">PREGUNTAS DE RESERVA</p>', "", $content);
                        $sspp = '<section class="alert alert-dark fade question">'.$sspp.'</section>';
                    }
                        
                    $assessment = str_replace('#SSPP', $sspp, $assessment);
                    $update = fopen("scorm/shared/assessmenttemplate.html",'w');
                    fwrite($update, $assessment);
                    fclose($update);
                    
                    $nodeList = $xpath->query("//p[@class='pregunta']");
                    $nodeResponse = $xpath->query("//p[@class='respuestas']");

//total preguntas
                    /* echo "total preguntas " . $nodeList->length;
                      echo "total respuestas " . $nodeResponse->length; */
                    $file = '';
//mostramos preguntas
                    for ($i = 0; $i < $nodeList->length; $i++) {

                        $node = $nodeList->item($i);
                        //sacar número de pregunta
                        $numQuestion = explode(".", $node->nodeValue);

                        if ($i < $nodeList->length - 1) {
                            $next = $nodeList->item($i + 1);
                            $numNext = explode(".", $next->nodeValue);
                        }

                        $question = $node->nodeValue;
                        
//    echo $question;
                        //respuestas
                        if ($numNext[0] == $numQuestion[0]) {
                            $end = '<p class="tema-sol">';
                        } else
                            $end = '<p class="pregunta">' . $numNext[0] . '.';

                        $responses = get_string_between($content, '<p class="pregunta">' . $node->nodeValue . '</p>', $end);
                        
//                        $responses = str_replace('<span class="CELDA">', '', $responses);
//                        $responses = str_replace('</span>', '', $responses);
                      
                        $responses = str_replace('<p class="respuestas">', '"', $responses);
                        $responses = str_replace('</p>', '",', $responses);
                        $responses = str_replace("¢", '', $responses);
                        $responses = str_replace('", ', '"', $responses);

                        $responses = str_replace(array("\r\n", "\r", "\n", "\t"), '', $responses);
                        
//    echo $responses . "<br>";

                        $final = 0;

//    echo "-->".$numNext[0] ." -- ". $numQuestion[0];
                        //soluciones
                        if ($numNext[0] < $numQuestion[0]) {
                            
                            if($xpath->query("//p[@class='TIT-SIMULACRO']")->length > 0)
                                $endSol = '<p class="TIT-PREGUNTAS-RESERVA">';
                            else
                                $endSol = '<p class="reserva-sol">';
                            
                            $final = 1;
                        } else if ($numNext[0] == $numQuestion[0]) {
                            $endSol = '</div>';
                        } else {
                            $endSol = '<p class="z1-">' . $numNext[0] . '.</p>';
                        }
                        
                        //es para la otra versión que incluye el feedback
                //    $correctAnswer = get_string_between($content, '<p class="z1-">'.$numQuestion[0].'.</p>', $endSol);
                        //modelo Victor con feedback
                        if ($xpath->query("//p[@class='z-Motiv-']")->length > 0) 
                            $correctAnswer = get_string_between($content, '<p class="z1-">' . $numQuestion[0] . '.</p>', '<p class="z-Motiv-">');
                        //sin feedback
                        else
                            $correctAnswer = get_string_between($content, '<p class="z1-">' . $numQuestion[0] . '.</p>', $endSol);

                        $correctAnswer = str_replace('<p class="z-solución">', '', $correctAnswer);
                //    $correctAnswer = str_replace('<p class="z-Motiv-">', '', $correctAnswer);
                //    $correctAnswer = str_replace('<p class="z-Motiv-guion">', '', $correctAnswer);
                        $correctAnswer = str_replace('</p>', '', $correctAnswer);
                        $correctAnswer = str_replace('<br>', '', $correctAnswer);

                        $correctAnswer = str_replace(array("\r\n", "\r", "\n", "\t"), '', $correctAnswer);
                        
                        if ($xpath->query("//p[@class='z-Motiv-']")->length > 0){
                            $complete = get_string_between($content, '<p class="z1-">' . $numQuestion[0] . '.</p>', $endSol);
                            $complete .= "<fin>";

                            $feedback = get_string_between($complete, '<p class="z-Motiv-">', '<fin>');
                            $feedback = str_replace(array("\r\n", "\r", "\n", "\t"), '', $feedback);
                        }else
                            $feedback = '';

                    //    echo "<p>===============SOLUCIÓN=========================</p>";
                    //    $responses = $responses.$correctAnswer;
                    //    echo $responses;
                    //    echo "<p>================================================</p>";
                    //    $title = get_string_between($content, '<span class="NUM-TEST">', '</p>');
                    //    $title = str_replace('</span>', '', $title);
                    //    $title = str_replace(' ', '', $title);

                        $title = "TEST";

                        $file .= "test.AddQuestion( new Question ('com.scorm.editorialcep.interactions.{$title}_{$i}',
                                '{$question}',
                               QUESTION_TYPE_CHOICE,
                                new Array($responses),
                                '$correctAnswer', '{$feedback}',
                                'obj_etiquette')
                        );";
                                
                        //nos saltamos las preguntas de reserva
                        if ($final == 1)
                            break;
                    }
                    
                    //comprobamos que existe el directorio
                    $path = "scorm/{$title}";
                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    } else {
                        //una vez creado el directorio comprobamos que existe el js de las preguntas
                        unlink("scorm/scorm.zip");
                    }

                    //creamos el fichero
                    file_put_contents("scorm/{$title}/questions.js", $file);

                    
                    //delete uploaded html
                    unlink("scorm/{$fileName}");                    
                    //delete output
                    unlink("output/scorm.zip");
                    
                    //create zip
                    zipFile('scorm','output/scorm.zip', false);
                    
                    //volvemos el assessment al estado original 
                    $assessment = file_get_contents("scorm/shared/assessmenttemplate.html");
                    $oldText = get_string_between($assessment, '<div class="sspp">', '</div>');
                    
                    if(!$oldText)
                        $assessment = str_replace('<div class="sspp"></div>', '<div class="sspp">#SSPP</div>', $assessment);
                    else{
                        $assessment = str_replace($oldText, '#SSPP', $assessment);
                    }

                    $update = fopen("scorm/shared/assessmenttemplate.html",'w');
                    fwrite($update, $assessment);
                    fclose($update);
                    
                    header('Content-type: application/zip');
                    header('Content-Disposition: attachment; filename="' . basename("output/scorm.zip") . '"');
                    header("Content-length: " . filesize("output/scorm.zip"));
                    header("Pragma: no-cache");
                    header("Expires: 0");
                    ob_clean();
                    flush();
                    readfile("output/scorm.zip");
                }
            } else {
                echo "<script>alert('Ocurrió un error mientras se subía el fichero')</script>";
            }
        } else {
            foreach ($errors as $error) {
                echo $error . "Estos son los errores " . "\n";
            }
        }
    }
}
?>