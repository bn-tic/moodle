var test;

var btnPrev, btnNext, btnSubmitResults;
var currentPage, totalPages;
var totalQuestions;

var questionContainer;
var resultsContainer;

var rpp = 5;

if(document.querySelectorAll('.alert-dark').length > 0)
            rpp = rpp+1;
        
var curPage = 1;
var pageCount = 0;
var questionCount;

//CONSTANTES:

var QUESTION_TYPE_CHOICE = "choice";
var QUESTION_TYPE_TF = "true-false";
var QUESTION_TYPE_NUMERIC = "numeric";

//CLASES:
function Question(id, text, type, answers, correctAnswer, feedback, objectiveId) {
	this.Id = id;
	this.Text = text;
	this.Type = type;
	this.Answers = answers;
	this.CorrectAnswer = correctAnswer;
	this.Feedback = feedback;
	this.ObjectiveId = objectiveId;
}

function Test(questions) {
	this.Questions = questions;
}

Test.prototype.AddQuestion = function (question){
	this.Questions[this.Questions.length] = question;
}

//FUNCIONES:

function UpdateUI(){
	if(curPage == 1){
		btnPrev.classList.add('hide');
	}else{
		btnPrev.classList.remove('hide');
	}
	
	if(curPage == pageCount){
		btnNext.classList.add('hide');
		btnSubmitResults.classList.remove('hide');
	}else{
		btnNext.classList.remove('hide');
		btnSubmitResults.classList.add('hide');
	}
	
	totalQuestions.innerText = questionCount;
	
	currentPage.innerText = curPage;
}

function nextPage(){
	curPage = Math.min(curPage+1, pageCount);
	RenderTest(curPage);
	
}

function prevPage(){
	curPage = Math.max(curPage-1, 1);
	RenderTest(curPage);
}


function numPages(){
	return Math.ceil(test.Questions.length / rpp);
}


function CreateQuestions(){
	for(var i=0;i<test.Questions.length;i++){
		var q = test.Questions[i];
		var box = document.createElement('DIV');
		box.dataset['qnum'] = i;
		box.id = 'q_'+q.Id;
		box.className = 'alert alert-primary fade hide question';
		box.innerHTML = '<div class="question-text">' + q.Text + '</div> \
						 <div class="question-options"></div>';
		var htmlAnswer = "";
		/*
		box.innerHTML = '<div data-qnum="'+i+'" id="q_'+q.Id+'" class="alert alert-primary alert-dismissible fade hide question-box"> \
							<div class="question">' + q.Text + '</div> \
							<div class="options"></div> \
						</div>';
						
		*/
		
		switch(q.Type){
			case QUESTION_TYPE_CHOICE:
				var ansIndex = 0;
				for (var j=0;j<q.Answers.length;j++){
					htmlAnswer += '<div class="radio"><input type="radio" data-anum="'+j+'" name="q_'+q.Id+'_choices" id="q_'+q.Id+'_'+j+'" />' + q.Answers[j] + '</div>';
				}
				break;
			
			default:
				alert('Tipo de pregunta no encontrado...');
		}
		
		questionContainer.appendChild(box);
		box.querySelector('.question-options').innerHTML = htmlAnswer;
	}
	totalPages.innerText = numPages();
}


function SubmitAnswers(){
	var correctCount = 0;
	var totalQuestions = questionCount;
	
	var resultData = [];
	
	var resultsSummary = "";
	
	for(var i=0;i<totalQuestions;i++){
		var q = test.Questions[i];
		
		var wasCorrect = false;
		var correctAnswer = null;
		var learnerResponse = "";
		var feedback = "";
		var correctAnswerText = "";
		var learnerAnswerText = "";
		
		switch(q.Type){
			case QUESTION_TYPE_CHOICE:
				var correctIdx = q.Answers.findIndex(ans=>ans.localeCompare(q.CorrectAnswer)===0);
				if(correctIdx >= 0){
					correctAnswer = correctIdx;
					correctAnswerText = q.CorrectAnswer;
					feedback = q.Feedback;
				}
				
				var checked = document.querySelector('[data-qnum="'+i+'"] input:checked');
				if(checked){
					learnerResponse = parseInt(checked.dataset['anum']);
					learnerAnswerText = q.Answers[learnerResponse];
				}else{
					learnerResponse = null;
					learnerAnswerText = 'No respondida';
				}
		}
		wasCorrect = (correctAnswer == learnerResponse);
		if(wasCorrect){
			correctCount++;
		}
		
		resultData.push({
			questionId: i,
			type: q.Type,
			correct: wasCorrect,
			learnerResponse: learnerResponse,
			learnerAnswerText: learnerAnswerText,
			correctAnswerText: correctAnswerText,
			feedback: feedback
		});
		
	}
	
	RenderResults(resultData);
	
	return resultData;
}

function RenderTest(page){
	if(!page){ page = 1; }
	UpdateUI();
	Array.prototype.slice.call(document.querySelectorAll('.question')).map(q=>q.classList.add('hide'));
	Array.prototype.slice.call(document.querySelectorAll('.question')).slice((page-1)*rpp, page*rpp).map(q=>q.classList.remove('hide'));
	
}

function RenderResults(data){
	window.scrollTo(0, 0);
	var resultsOutput = document.createElement('DIV');
	resultsOutput.classList.add('results');
	
	var resultsQuestions = document.createElement('DIV');
	resultsQuestions.classList.add('results-questions');
	
	for(var i=0;i<data.length;i++){
		var result = document.createElement('DIV');
		result.classList.add('color-box','space');
		result.innerHTML = '<div class="shadow"> \
								<div class="info-tab tip-icon"><i class="far fa-lightbulb"></i></div> \
								<div class="tip-box"> \
									<p><strong>Pregunta ' + test.Questions[data[i].questionId].Text + '</strong></p> \
									' + (data[i].correct?' \
									<p><strong><em>Correcto</em></strong><br> \
									Tu respuesta: ' + data[i].correctAnswerText + '<br></p> \
									' : ' \
									<p><strong><em>Incorrecto</em></strong><br> \
									<strong>Tu respuesta:</strong> ' + data[i].learnerAnswerText + '<br><br> \
									<strong>Respuesta correcta</strong>: ' + data[i].correctAnswerText + '<br><br> \
									<strong>Feedback</strong>: ' + data[i].feedback + '<br></p>') + ' \
								</div> \
							</div>';
		resultsQuestions.appendChild(result);
	}
	
	var correctCount = data.filter(function(r){ return r.correct; }).length;
	var score = Math.round(correctCount * 100 / test.Questions.length);
	
	var scoreContainer = document.createElement('DIV');
	scoreContainer.classList.add('results-score');
	scoreContainer.innerHTML = '<h3>Puntuación: ' + score + '</h3>';
	resultsOutput.appendChild(scoreContainer);
	
	resultsOutput.appendChild(resultsQuestions);
	
	resultsContainer.innerHTML = '';
	
	resultsContainer.appendChild(resultsOutput);
	
	document.getElementById('test').classList.add('results');
	
	try{
		if(parent && parent.RecordTest){
			parent.RecordTest(score);
		}
	}catch(e){ ; }
}


// Al terminar de cargar la pagina:

document.addEventListener('DOMContentLoaded', function(){
	 test = new Test(new Array());
	
	//carga del fichero de preguntas.
	var qs = document.location.search.replace("?", "");
	var includeFiles = qs.split("&");
	var questionsFile;
	for(var i=0;i<includeFiles.length;i++){
		questionsFile = includeFiles[i].replace("questions=", "");
		
		var qscript = document.createElement('SCRIPT');
		
		qscript.addEventListener('load', function(){
			console.log('Preguntas cargadas.', test);
			
			pageCount = numPages();
			
			questionCount = test.Questions.length;
			
			CreateQuestions();
			
			RenderTest();
			
		});
		
		qscript.src = '../'+questionsFile+'/questions.js';
		qscript.type = 'text/javascript';
		
		document.head.appendChild(qscript);
		break;
	}
	
	btnPrev = document.querySelector('.btn-prev');
	btnPrev.addEventListener('click', prevPage);
	btnNext = document.querySelector('.btn-next');
	btnNext.addEventListener('click', nextPage);
	currentPage = document.querySelector('.current-page');
	totalPages = document.querySelector('.total-pages');
	totalQuestions = document.querySelector('.total-questions>span');
	questionContainer = document.querySelector('.question-container');
	resultsContainer = document.querySelector('.test-results');
	
	btnSubmitResults = document.querySelector('.btn-submit-results');
	
	btnSubmitResults.addEventListener('click', SubmitAnswers);
	
}, false);