<?php

require_once('config.php');
require_login();

echo $OUTPUT->header();

//get all excellence
$excellences = $DB->get_records_sql("SELECT * FROM {course} WHERE fullname LIKE '%excell%'");

foreach ($excellences as $excellence) {
    //insert course section
    $courses = $DB->get_records_sql("SELECT * FROM {course_sections} WHERE course = {$excellence->id} ORDER BY section DESC");


    foreach ($courses as $course) {
        $course_id = $course->course;
        $last_section = $course->section + 1;
        $availability = "\"{'op':'&'','c':[],'showc':[]}\"";
        break;
    }

//insert section
    for ($i = 0; $i < 3; $i++) {
        switch ($i) {
            case '0':
                $color = "Verde";
                $category = 1;
                $level = "baja";
                break;
            case '1':
                $color = "Naranja";
                $category = 2;
                $level = "media";
                break;
            case '2':
                $color = "Rojo";
                $category = 3;
                $level = "elevada";
                break;
        }
        //insert section
        $DB->execute("INSERT INTO {course_sections} (course, section, name, summary, summaryformat, sequence, visible, availability) "
                . "VALUES ({$course_id}, {$last_section}, 'Preguntas de dificultad {$level} ({$color})', '', 1,'',1," . $availability . ") ");

        $last_id = $DB->get_field('course_sections', 'MAX(id)', array());

        for ($j = 0; $j < 3; $j++) {
            switch ($j) {
                case '0':
                    $name = "parent";
                    $value = 0;
                    break;
                case '1':
                    $name = "visibleold";
                    $value = 1;
                    break;
                case '2':
                    $name = "collapsed";
                    $value = 0;
                    break;
            }
            //insert format options
            $DB->execute("INSERT INTO {course_format_options} (courseid, format, sectionid, name, value) "
                    . "VALUES ({$course_id}, 'flexsections', {$last_id}, '{$name}', {$value}) ");
        }

        //insert coloreport
        $options = new stdClass();
        $options->course = $course_id;
        $options->name = 'Batería de preguntas de color ' . $color;
        $options->intro = '';
        $options->introformat = $category;
        $options->content = $category;
        $options->contentformat = $category;
        $options->legacyfiles = 0;
        $options->legacyfileslast = NULL;
        $options->display = 0;
        $options->displayoptions = "\"a:2:{s:12:'printheading';s:1:'0';s:10:'printintro';s:1:'0';}\"";
        $options->revision = 1;
        $options->timemodified = time();

        //capability of return the last id inserted
        $inserted_report = $DB->insert_record('coloreport', $options, $returnid = true);

        //insert coloreport module
        $colormodule = new stdClass();
        $colormodule->course = $course_id;
        $colormodule->module = 36;
        $colormodule->instance = $inserted_report;
        $colormodule->section = $last_section;
        $colormodule->idnumber = '';
        $colormodule->added = time();
        $colormodule->score = 0;
        $colormodule->indent = 0;
        $colormodule->visible = 1;
        $colormodule->visibleoncoursepage = 1;
        $colormodule->visibleold = 1;
        $colormodule->groupmode = 0;
        $colormodule->groupingid = 0;
        $colormodule->completion = 1;
        $colormodule->completiongradeitemnumber = NULL;
        $colormodule->completionview = 0;
        $colormodule->completionexpected = 0;
        $colormodule->showdescription = 0;
        $colormodule->availability = NULL;
        $colormodule->deletioninprogress = 0;

        $inserted_module = $DB->insert_record('course_modules', $colormodule, $cm_id = true);

        //update sequence on course section
        $DB->execute("UPDATE {course_sections} SET sequence = {$inserted_module} WHERE id = {$last_id}");

        echo "<div class='prom-box prom-box-info'><h3>Informe {$color} insertado correctamente</h3></div>";
        $last_section++;
    }
}

echo $OUTPUT->footer();
