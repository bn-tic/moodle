<?php

// Include required files.
require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->dirroot . '/blocks/iomad_progress/lib.php');
require_once(dirname(__FILE__) . '/../../EventClass.php');

//get info
$iomad_progressblock = $DB->get_record('block_instances', array('id' => $_GET['progressid']), '*', MUST_EXIST);
$iomad_progressconfig = unserialize(base64_decode($iomad_progressblock->configdata));

echo $OUTPUT->header();

$modules = block_iomad_progress_modules_in_use($_GET['course']);
$events = block_iomad_progress_event_information($iomad_progressconfig, $modules, $_GET['course']);

$course = $DB->get_record('course', array('id' => $_GET['course']), '*', MUST_EXIST);
$context = block_iomad_progress_get_course_context($_GET['course']);

$userevents = block_iomad_progress_filter_visibility($events, $USER->id, $context, $course);

if (!empty($userevents)) {
    $attempts = block_iomad_progress_attempts($modules, $iomad_progressconfig, $userevents, $USER->id, $course->id);
} else {
    $iomad_progressbar = get_string('no_visible_events_message', 'block_iomad_progress');
    $iomad_progressvalue = 0;
    $iomad_progress = '?';
}

$frontpageurl = new moodle_url('/course/view.php?id=' . $course->id, ['redirect' => 0]);
$types = ['resource', 'scorm', 'video', 'page', 'book', 'quiz'];

if ($_GET['recalc'] > 0) {
    //recalc with end license date
    if ($_GET['function'] == 'recalc') {

        //if pulse recalc button without finishtime
        if($_GET['finishtime']['year'] == '' && $_GET['finishtime']['month'] == '' && $_GET['finishtime']['day'] == ''){
            $finishtime = Event::get_end_license($USER->id, $course->id);
        }else{
            $finishtime = $_GET['finishtime']['year'] . "-" . $_GET['finishtime']['month'] . "-" . $_GET['finishtime']['day'];
        }
        
        
        $now = date('Y-m-d H:i:s', time());
        $diff = 0;

        $info_events = Event::get_info_events($course->id, $USER->id);
        $lessons = Event::get_lessons($course->id);

        if (strtotime($now) > strtotime($finishtime)) {
            redirect($frontpageurl, "<strong>Error:</strong> La fecha introducida no es correcta, debe ser mayor a la actual... ");
        }

        //get not passed events
        foreach ($lessons as $lesson) {
            foreach ($info_events as $info_event) {
                if ($info_event->name == $lesson->name) {
                    $total ++;
                    if (strpos($lesson->sequence, ',') !== false) {
                        $sequence = explode(",", $lesson->sequence);
                        for ($i = 0; $i < count($sequence); $i++) {
                            $resource = Event::search_resource($course->id, $sequence[$i]);
                            foreach ($events as $event) {

                                if ($event['name'] == $resource) {
                                    if (in_array($event['type'], $types)) {
                                        //updating all events
                                        if ($finishtime) {
                                            if($finishtime > Event::get_end_license($USER->id, $course->id))
                                                redirect($frontpageurl, "<strong>Error:</strong> La fecha introducida no puede ser mayor que la fecha de expiración de su licencia... ");

                                            $now = date('Y-m-d H:i:s', strtotime($now) + $diff);
                                            $diff = Event::recalc($USER->id, $course->id, count($events), $now, $diff, $info_event->id, true, $finishtime);
                                        }
                                        //recalculate old events
                                        $attempted = $attempts[$event['type'] . $event['id']];
                                        if ($attempted === true) {
                                            continue;
                                        } else if (((!isset($config->orderby) || $config->orderby == 'orderbytime') && ($info_event->timestart + $info_event->timeduration) < time()) || ($attempted === 'failed')) {
                                            //insert event (recalc)
                                            if ($last_event != $info_event->id) {
                                                $now = date('Y-m-d H:i:s', strtotime($now) + $diff);
                                                $diff = Event::recalc($USER->id, $course->id, $_GET['recalc'], $now, $diff, $info_event->id, false, '');
                                            }

                                            $last_event = $info_event->id;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        redirect($frontpageurl, "Actualizando planificación... ");
    } else {
        redirect($frontpageurl, "Error al recalcular la planificación");
    }
} else {
    redirect($frontpageurl, "Error al recalcular la planificación");
}

echo $OUTPUT->footer();
