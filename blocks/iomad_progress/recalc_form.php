<?php

/*
 * FORM TO RECALC PLANNING WITH A CUSTOM DATE
 */

require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->dirroot.'/blocks/iomad_progress/lib.php');

class recalc_planning_form extends moodleform {

    //Add elements to form
    public function definition() {
        global $CFG;
        
        $id = required_param('iomad_progressbarid', PARAM_INT);
        $courseid = required_param('courseid', PARAM_INT);
        
        $htmlhead = "";
        $mform = $this->_form; // Don't forget the underscore! 

        $htmlhead .= "<div class='panel-group' id='accordion'>";
        $htmlhead .= "<div class='panel panel-default'>";
        $htmlhead .= "<div class='panel-heading'>";
        $htmlhead .= "<a data-toggle='collapse' data-parent='#accordion' href='#collapseDate' class='collapsed'>";
        $htmlhead .= "Recalcula tu planning";
        $htmlhead .= "</a>";
        $htmlhead .= "</div>";
        $htmlhead .= "<div id='collapseDate' class='panel-collapse collapse'>";
        $htmlhead .= "<div class='panel-body'><div class='form-group'><p>";
        $htmlhead .= "Te ofrecemos un planning acorde a la duración de tu licencia. Si por el contrario quieres una planificación a medida, a continuación puedes establecer la fecha límite que más te convenga:";
        
        $htmlfooter .= "</p></div></div>";
        $htmlfooter .= "</div></div></div>";
        
        
        $mform->addElement('html', $htmlhead);
        //content form
        $mform->addElement('date_selector', 'finishtime', get_string('finishtime','block_iomad_progress'));
        $mform->addElement('hidden','course',$courseid);
        $mform->addElement('hidden','progressid',$id);
        $mform->addElement('hidden','recalc',1);
        $mform->addElement('hidden','function','recalc');
        
        //$mform->addElement('button', 'submit', get_string('submit_recalc','block_iomad_progress'));
        $this->add_action_buttons(false, get_string('submit_recalc','block_iomad_progress'));
        
        
        $mform->addElement('html', $htmlfooter);
        
        
    }
}
