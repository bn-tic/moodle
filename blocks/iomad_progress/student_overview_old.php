<?php

// Include required files.
require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->dirroot . '/blocks/iomad_progress/lib.php');
require_once(dirname(__FILE__) . '/../../EventClass.php');

require_once('recalc_form.php');


// Gather form data.
$id = required_param('iomad_progressbarid', PARAM_INT);
$courseid = required_param('courseid', PARAM_INT);
$departmentid = optional_param('departmentid', 0, PARAM_INT);

// Determine course and context.
$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
$context = block_iomad_progress_get_course_context($courseid);

// Get specific block config and context.
$iomad_progressblock = $DB->get_record('block_instances', array('id' => $id), '*', MUST_EXIST);
$iomad_progressconfig = unserialize(base64_decode($iomad_progressblock->configdata));
$iomad_progressblockcontext = block_iomad_progress_get_block_context($id);

// Set up page parameters.
$PAGE->set_course($course);
$PAGE->requires->css('/blocks/iomad_progress/styles.css');
$PAGE->set_url('/blocks/iomad_progress/student_overview.php', array('iomad_progressbarid' => $id, 'courseid' => $courseid, 'departmentid' => $departmentid));
$PAGE->set_context($context);
$title = '<i class="fa fa-clock-o"></i> '. get_string('student_overview', 'block_iomad_progress');
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->navbar->add($title);
$PAGE->set_pagelayout('base');

// Check user is logged in and capable of grading.
require_login($course, false);
require_capability('block/iomad_progress:student_overview', $iomad_progressblockcontext);

// Start page output.
echo $OUTPUT->header();
echo $OUTPUT->heading($title, 2);
echo $OUTPUT->container_start('block_iomad_progress');

// Get the modules to check iomad_progress on.
$modules = block_iomad_progress_modules_in_use($course->id);
if (empty($modules)) {
    echo get_string('no_events_config_message', 'block_iomad_progress');
    echo $OUTPUT->container_end();
    echo $OUTPUT->footer();
    die();
}

// Check if activities/resources have been selected in config.
$events = block_iomad_progress_event_information($iomad_progressconfig, $modules, $course->id);
if ($events == null) {
    echo get_string('no_events_message', 'block_iomad_progress');
    echo $OUTPUT->container_end();
    echo $OUTPUT->footer();
    die();
}
if (empty($events)) {
    echo get_string('no_visible_events_message', 'block_iomad_progress');
    echo $OUTPUT->container_end();
    echo $OUTPUT->footer();
    die();
}
$numevents = count($events);


echo html_writer::start_tag('div', array('class' => 'row'));
echo html_writer::start_tag('div', array('class' => 'span8'));

echo "<h5>Tareas evaluables </h5>";

$userevents = block_iomad_progress_filter_visibility($events, $USER->id, $context, $course);

if (!empty($userevents)) {
    $attempts = block_iomad_progress_attempts($modules, $iomad_progressconfig, $userevents, $USER->id, $course->id);
} else {
    $iomad_progressbar = get_string('no_visible_events_message', 'block_iomad_progress');
    $iomad_progressvalue = 0;
    $iomad_progress = '?';
}

block_iomad_table_events($course, $modules, $config, $events, $attempts);

//span9
echo html_writer::end_tag('div');

//foot
echo html_writer::start_tag('div', array('class' => 'span4'));


$types = ['resource', 'scorm', 'video', 'page', 'book', 'quiz'];

$recalc = 0;
$expected = time();

$info_events = Event::get_info_events($course->id, $USER->id);
$lessons = Event::get_lessons($course->id);

foreach ($lessons as $lesson) {
    foreach ($info_events as $info_event) {
        if ($info_event->name == $lesson->name) {
            $total ++;
            if (strpos($lesson->sequence, ',') !== false) {
                $sequence = explode(",", $lesson->sequence);
                for ($i = 0; $i < count($sequence); $i++) {
                    $resource = Event::search_resource($course->id, $sequence[$i]);
                    $count_resources ++;

                    foreach ($events as $event) {

                        if ($event['name'] == $resource) {
                            if (in_array($event['type'], $types)) {
                                $attempted = $attempts[$event['type'] . $event['id']];
                                if ($attempted === true) {
                                    $completed ++;
                                } else if ((($info_event->timestart + $info_event->timeduration) < time()) || ($attempted === 'failed')) {
                                    if (($info_event->timestart + $info_event->timeduration) < $expected)
                                        $expected = ($info_event->timestart + $info_event->timeduration);
                                    $recalc++;
                                }
                            }
                        }
                    }
                }
            }
            else if ($lesson->sequence != "") {
                $resource = Event::search_resource($course->id, $lesson->sequence);
                if ($event['name'] == $resource) {
                    if (in_array($event['type'], $types)) {
                        $attempted = $attempts[$event['type'] . $event['id']];
                        if ($attempted === true) {
                            $completed ++;
                        } else if ((($info_event->timestart + $info_event->timeduration) < time()) || ($attempted === 'failed')) {
                            if (($info_event->timestart + $info_event->timeduration) < $expected)
                                $expected = ($info_event->timestart + $info_event->timeduration);
                            $recalc++;
                        }
                    }
                }
            }
        }
    }
}



echo "<div class='alert alert-warning'>Planificación calculada en base a la fecha de expiración de su licencia: <strong>".date('d/m/Y', strtotime(Event::get_end_license($USER->id,$course->id)))."</strong></div>";

echo "<h5>Porcentaje completado " . round(($completed * 100) / count($events), 2) . "%</h5>";

$data = new \core\chart_series('Tareas ', [$completed, count($events) - $completed]);
$labels = array("Completadas", "Sin visualizar, fuera de plazo o sin completar");

//PIE CHART
$chart = new \core\chart_pie();
$CFG->chart_colorset = ['#dff0d8', '#f2dede'];
$chart->add_series($data);
$chart->set_labels($labels);
echo $OUTPUT->render($chart);

if ($recalc > 0) {
    //recalc
    $parameters = array('function' => 'recalc',
        'recalc' => $recalc,
        'course' => $courseid,
        'progressid' => $id);
    $url = new moodle_url('/blocks/iomad_progress/recalculating.php', $parameters);
    $label = get_string('recalculating', 'block_iomad_progress');
    $options = array('class' => 'overviewButton');
    echo $OUTPUT->single_button($url, $label, 'get', $options);
}

$recalc_form = new recalc_planning_form(new moodle_url('/blocks/iomad_progress/recalculating.php'), null, 'get');

//render form
$recalc_form->display();

echo html_writer::end_tag('div');
//end row
echo html_writer::end_tag('div');

echo $OUTPUT->container_end();

echo $OUTPUT->footer();
