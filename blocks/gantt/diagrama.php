<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* SIN BARRA DE PROGRESO
  $completion = new completion_info($courseid);
  $activities = $completion->get_activities();

  foreach($activities as $activity) {
  echo $activity->name;
  }
 */


// Include required files.
require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->dirroot . '/blocks/iomad_progress/lib.php');
require_once(dirname(__FILE__) . '/../../EventClass.php');

//required params
$courseid = required_param('courseid', PARAM_INT);
$courseformat = required_param('courseformat', PARAM_ALPHANUMEXT);
$userid = required_param('userid', PARAM_INT);


//echo $idbar;exit;
// Determine course and context.
$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
$context = block_iomad_progress_get_course_context($courseid);

$idbar = Event::get_iomadbar_byparentcontext($context->id);

// Get specific block config and context.
$iomad_progressblock = $DB->get_record('block_instances', array('id' => $idbar), '*', MUST_EXIST);
$iomad_progressconfig = unserialize(base64_decode($iomad_progressblock->configdata));
$iomad_progressblockcontext = block_iomad_progress_get_block_context($idbar);


// Set up page parameters.
$PAGE->set_course($course);
$PAGE->requires->css('/blocks/gantt/styles.css');
$PAGE->set_url('/blocks/gantt/diagrama.php', array('iomad_progressbarid' => $idbar, 'courseid' => $courseid));
$PAGE->set_context($context);
$title = 'Diagrama de Gantt';
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->navbar->add($title);
$PAGE->set_pagelayout('base');


//required login
require_login($course, false);

// Start page output.
echo $OUTPUT->header();

// Get the modules to check iomad_progress on.
$modules = block_iomad_progress_modules_in_use($courseid);
if (empty($modules)) {
    echo get_string('no_events_config_message', 'block_iomad_progress');
    echo $OUTPUT->container_end();
    echo $OUTPUT->footer();
    die();
}
$events = block_iomad_progress_event_information($iomad_progressconfig, $modules, $courseid);
if ($events == null) {
    echo get_string('no_events_message', 'block_iomad_progress');
    echo $OUTPUT->container_end();
    echo $OUTPUT->footer();
    die();
}
if (empty($events)) {
    echo get_string('no_visible_events_message', 'block_iomad_progress');
    echo $OUTPUT->container_end();
    echo $OUTPUT->footer();
    die();
}


$userevents = block_iomad_progress_filter_visibility($events, $userid, $context, $course);


if (!empty($userevents)) {
    $attempts = block_iomad_progress_attempts($modules, $iomad_progressconfig, $userevents, $userid, $courseid);
} else {
    $iomad_progressbar = get_string('no_visible_events_message', 'block_iomad_progress');
    $iomad_progressvalue = 0;
    $iomad_progress = '?';
}

$numevents = count($events);
$timestart = strtotime(Event::get_start_license($userid, $courseid));
$timeend = strtotime(Event::get_end_license($userid, $courseid));
$days = round((date($timeend) - date($timestart)) / 60 / 60 / 24);
$diario = $days / $numevents;
$last = 0;
$checked = 0;
$now = time();

$info_events = Event::get_info_events($courseid, $userid);




//info to collapsible table
$activities_completion_info = Event::get_all_completion_resources($courseid, $userid);

$table = "<div class='scroll-calendar'><table class='table'>";
$table .= "<tr>";
$table .= "<th>TAREAS</th>";

for ($i = 0; $i <= $days; $i++) {
    if (date('d/m/y', $timestart + (86400 * $i)) == date('d/m/y', $now)) {
        $table .= "<th class='today'>";
    } else {
        $table .= "<th class='days'>";
    }
    $table .= date('d/m/y', $timestart + (86400 * $i)) . "</th>";
}

$table .= "</tr>";

//temas
$in = 1;
$lastin = 0;
$escrito = 0;

//course format
if ($courseformat == 'tiles') {
    $infos = Event::get_info_events($courseid, $userid);
    foreach ($infos as $info) {
        foreach ($events as $event) {
            if ($event['name'] == $info->name) {
                $table .= "<tr id='{$in}'>";
                $table .= "<td class='tareas'><b>{$event['name']}</b></td>";

                $attempted = $attempts[$event['type'] . $event['id']];

                for ($j = 0; $j <= $days; $j++) {
                    if (($j <= ($diario * $in)) && ($j >= $last)) {
                        if ($in != $lastin && $escrito != 1) {
                            if ($attempted === true) {
                                $class = "success";
                                $checked ++;
                            } else if (((!isset($config->orderby) || $config->orderby == 'orderbytime') && ($info->timestart + $info->timeduration) < $now) || ($attempted === 'failed')) {
                                $class = "danger";
                            } else {
                                $class = "info";
                            }
                            $table .= "<td id= '{$j}' class='bar {$class}' colspan='" . round($diario) . "'>";
                            $table .= $OUTPUT->action_link('/mod/' . $event['type'] . '/view.php?id=' . $event['cm']->id, $OUTPUT->pix_icon('icon', '', $event['type'], array('class' => 'moduleIcon')) . s($event['name']));
                            $table .= "</td>";
                            $escrito = 1;
                        }
                    } else {
                        $table .= "<td id= '{$j}'>";
                        $table .= "</td>";
                    }
                }
                $lastin = $in;
                $escrito = 0;
                $last = $diario * $in;
                $last_resource = $resource;
                $in++;
            }
            $table .= "</tr>";
        }
    }
} else {
    $lessons = Event::get_lessons($courseid);
    foreach ($lessons as $lesson) {
        //search in calendar
        foreach ($info_events as $info_event) {
            if ($info_event->name == $lesson->name) {

                foreach ($events as $event) {

                    if (strpos($lesson->sequence, ',') !== false) {
                        $sequence = explode(",", $lesson->sequence);

                        $last_resource = "";

                        for ($i = 0; $i < count($sequence); $i++) {
                            $resource = Event::search_resource($courseid, $sequence[$i]);

                            if ($resource == $event['name']) {

                                if ($last_resource != $resource) {
                                    $table .= "<tr id='{$in}'>";
                                    $table .= "<td class='tareas'><b>{$event['name']}</b></td>";

                                    $attempted = $attempts[$event['type'] . $event['id']];

                                    for ($j = 0; $j <= $days; $j++) {
                                        if (($j <= ($diario * $in)) && ($j >= $last)) {
                                            if ($in != $lastin && $escrito != 1) {
                                                if ($attempted === true) {
                                                    $class = "success";
                                                    $checked ++;
                                                } else if (((!isset($config->orderby) || $config->orderby == 'orderbytime') && ($info_event->timestart + $info_event->timeduration) < $now) || ($attempted === 'failed')) {
                                                    $class = "danger";
                                                } else {
                                                    $class = "info";
                                                }
                                                $table .= "<td id= '{$j}' class='bar {$class}' colspan='" . round($diario) . "'>";
                                                $table .= $OUTPUT->action_link('/mod/' . $event['type'] . '/view.php?id=' . $event['cm']->id, $OUTPUT->pix_icon('icon', '', $event['type'], array('class' => 'moduleIcon')) . s($event['name']));
                                                $table .= "</td>";
                                                $escrito = 1;
                                            }
                                        } else {
                                            $table .= "<td id= '{$j}'>";
                                            $table .= "</td>";
                                        }
                                    }
                                    $lastin = $in;
                                    $escrito = 0;
                                    $last = $diario * $in;
                                    $last_resource = $resource;
                                    $in++;
                                }
                            }
                        }
                    }
                }
                $table .= "</tr>";
            }
        }
    }
}


/* $in = 1;
  $lastin = 0;
  $escrito = 0;

  foreach ($events as $event) {
  $attempted = $attempts[$event['type'] . $event['id']];

  $table .= "<tr id='{$in}'>";
  $table .= "<td class='tareas'><b>{$event['name']}</b></td>";

  //resto de columnas para completar la tabla
  for ($j = 0; $j <= $days; $j++) {
  if (($j <= ($diario * $in)) && ($j >= $last)) {
  if ($in != $lastin && $escrito != 1) {
  if ($attempted === true) {
  $class = "success";
  } else if (($attempted === 'failed')) {
  $class = "danger";
  } else {
  $class = "info";
  }
  $table .= "<td id= '{$j}' class='bar {$class}' colspan='" . round($diario) . "'>";
  $table .= $OUTPUT->action_link('/mod/' . $event['type'] . '/view.php?id=' . $event['cm']->id, $OUTPUT->pix_icon('icon', '', $event['type'], array('class' => 'moduleIcon')) . s($event['name']));
  $table .= "</td>";
  $escrito = 1;
  }
  } else {
  $table .= "<td id= '{$j}'>";
  $table .= "</td>";
  }
  }
  $table .= "</tr>";
  $last = $diario * $in;
  $lastin = $in;
  $escrito = 0;
  $in++;
  } */

$table .= "</table></div>";


//end table / start info
$html .= "<div class='row-fluid infopanel'>";
$html .= "<div class='span4'>";
$html .= "<div class='blocknumber_box'>";
$html .= "<div class='blocknumber_icon'>";
$html .= "<i class='fa fa-clock-o'></i>";
$html .= "</div>";
$html .= "<div class='blocknumber_content'>";
$html .= "<h5>Fecha de matriculaci&oacute;n</h5>";
$html .= "<p class='dateprogress'><b>" . date('d/m/y', $timestart) . "</b></p>";
$html .= "</div>";
$html .= "</div>";
$html .= "</div>";
$html .= "<div class='span4'>";
$html .= "<div class='blocknumber_box'>";
$html .= "<div class='blocknumber_icon'>";
$html .= "<i class='fa fa-graduation-cap'></i>";
$html .= "</div>";
$html .= "<div class='blocknumber_content'>";
$html .= "<h5>Fecha fin de licencia</h5>";
$html .= "<p class='dateprogress'><b>" . date('d/m/y', $timeend) . "</b></p>";
$html .= "</div>";
$html .= "</div>";
$html .= "</div>";
$html .= "<div class='span4'>";
$html .= "<div class='blocknumber_box'>";
$html .= "<div class='blocknumber_icon'>";
$html .= "<i class='fa fa-calendar-check-o'></i>";
$html .= "</div>";
$html .= "<div class='blocknumber_content'>";
$html .= "<h5>Items vistos</h5>";
$html .= "<p class='dateprogress'><b>" . $checked . " / " . $numevents . "</b></p>";
$html .= "</div>";
$html .= "</div>";
$html .= "</div>";
$html .= "</div>";

//end info
// start table completion activity
$info_activity = "<div class='panel-group' id='accordion' style='margin-top: 50px;'>";
$info_activity .= "<div class='panel panel-default'>";
$info_activity .= "<div class='panel-heading'>";
$info_activity .= "<a data-toggle='collapse' data-parent='#accordion' href='#collapseOne'>";
$info_activity .= "Más info sobre tu participación en el curso";
$info_activity .= "</a>";
$info_activity .= "</div>";
$info_activity .= "<div id='collapseOne' class='panel-collapse collapse in'>";
$info_activity .= "<div class='panel-body'>";
$info_activity .= "<table class='table table-responsive table-striped'>";
$info_activity .= "<tr>";
$info_activity .= "<td><b>Actividad</b></td>";
$info_activity .= "<td><b>Tipo</b></td>";
$info_activity .= "<td><b>Progreso</b></td>";
$info_activity .= "<td><b>Cuándo</b></td>";
$info_activity .= "</tr>";
foreach ($activities_completion_info as $data) {
    $info_activity .= "<tr><td><b>" . $data->activity . "</b></td>";
    $info_activity .= "<td>" . $OUTPUT->pix_icon('icon', '', $data->type) . "</td>";
    $info_activity .= "<td>" . $data->progress . "</td>";
    $info_activity .= "<td>" . date('d/m/Y', strtotime($data->whenf)) . "</td></tr>";
}
$info_activity .= "</table>";
$info_activity .= "</div>";
$info_activity .= "</div>";
$info_activity .= "</div>";
$info_activity .= "</div>";

// end table completion activity


echo $html . $table . $info_activity;


echo $OUTPUT->footer();
