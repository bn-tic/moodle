<?php
$string['pluginname'] = 'Bloque diagrama de Gantt';
$string['configtitle'] = 'Título del bloque';
$string['configcontent'] = 'Introduce tu HTML para dar la bienvenida al diagrama de Gantt';
$string['gantt:addinstance'] = 'Añade un nuevo diagrama de Gantt';
$string['gantt:myaddinstance'] = 'Añade un nuevo diagrama de Gantt';
$string['blockstring'] = 'Contenido del bloque';
$string['buttonview'] = 'Consultar diagrama';
$string['gantt:gantt_view'] = 'Ver diagrama de Gantt';

