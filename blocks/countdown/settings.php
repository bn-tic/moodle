<?php
$settings->add(new admin_setting_heading(
            'headerconfig',
            get_string('headerconfig', 'block_countdown'),
            get_string('descconfig', 'block_countdown')
        ));

$settings->add(new admin_setting_configcheckbox(
            'countdown/Allow_HTML',
            get_string('labelallowhtml', 'block_countdown'),
            get_string('descallowhtml', 'block_countdown'),
            '0'
        ));