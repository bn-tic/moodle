<?php
require_once(dirname(__FILE__) . '/../../EventClass.php');

class block_countdown extends block_base {
    public function init() {
        $this->title = get_string('countdown', 'block_countdown');
    }

    public function get_content() {
        global $USER;
            
        if ($this->content !== null) {
            return $this->content;
        }

        $this->content = new stdClass;

        if (!empty($this->config->text)) {
            $this->content->text = $this->config->text;
        } else {
            $message = '<div class="alert alert-warning countdown-block"><i class="fa fa-clock-o"></i>&nbsp;<span id="time'.$_GET['id'].'">'.Event::get_countdown($USER->id,$_GET['id']).'</span></div>';
            $this->content->text = $message;
        }

        //$this->content->footer = 'Footer';

        return $this->content;
    }

}
