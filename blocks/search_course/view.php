<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Block to display enrolled, completed, inprogress and undefined courses according to course completion criteria named 'grade' based on login user.
 *
 * @package    block_search_course
 * @copyright  3i Logic<lms@3ilogic.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */
require_once('../../config.php');
require_once('course_form.php');
require_once('lib.php');
require_once(dirname(__FILE__) . '/../../EventClass.php');

require_login();
global $DB, $OUTPUT, $PAGE, $CFG, $USER;
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string("pluginname", 'block_search_course'));
$PAGE->set_heading('Course Finder');
$pageurl = '/blocks/search_course/view.php';
$PAGE->set_url($pageurl);
$PAGE->navbar->ignore_active();
$PAGE->navbar->add(get_string("pluginname", 'block_search_course'));

echo $OUTPUT->header();

$form = new course_search_form();
$search = optional_param('search', null, PARAM_RAW);
$toform['search'] = $search;
$form->set_data($toform);
$form->display();

//admin list
$admins = ['2', '5193', '5192', '1002', '5196', '8722', '6358', '1815', '7065', '7069', '7068', '9403', '10423', '10925','14814','14978','15250', '15759','4236'];

//init form
$fromform = $form->get_data();

//admin query (all courses)
$admin_searchquery = "Select id from {course} WHERE";
$admin_searchquery .= " fullname LIKE '%" . $fromform->search . "%'";
if ($fromform->description == true)
    $admin_searchquery .= " OR summary LIKE '%" . $fromform->search . "%'";
if ($fromform->searchfromtime != 0)
    $admin_searchquery .= " AND startdate >= " . $fromform->searchfromtime . "";
if ($fromform->searchtilltime != 0)
    $admin_searchquery .= " AND enddate  <= " . $fromform->searchtilltime . "";
if ($fromform->courseformat != "")
    $admin_searchquery .= " AND format  = '" . $fromform->courseformat . "'";
if ($fromform->completioncriteria >= 0 && isset($fromform->completioncriteria))
    $admin_searchquery .= " AND enablecompletion  = " . $fromform->completioncriteria . "";
if ($fromform->sortmenu != "")
    $admin_searchquery .= " ORDER BY " . $fromform->sortmenu;

//user query (course user enrolment)
$searchquery_user = "SELECT mco_course.id
            FROM mco_enrol 
            JOIN mco_user_enrolments ON mco_user_enrolments.enrolid = mco_enrol.id
            JOIN mco_course ON mco_enrol.courseid = mco_course.id
            WHERE mco_user_enrolments.userid = {$USER->id} AND mco_course.visible = 1
            AND mco_course.FULLNAME NOT LIKE '%OBSOLETO%' AND mco_enrol.`status` = 0
            AND (mco_user_enrolments.timeend > 1610969073 OR mco_user_enrolments.timeend = 0) AND";
$searchquery_user .= " fullname LIKE '%" . $fromform->search . "%'";

foreach ($admins as $admin) {
    if (in_array($USER->id, $admins)) {
        //result filtered
        if ($fromform = $form->get_data()) {
            $courses = $DB->get_records_sql($admin_searchquery, null);
            echo getCourses($courses);
        }//all courses
        else {
            $courses = $DB->get_records_sql($admin_searchquery, null);
            echo getCourses($courses);
        }
        break;
    } else {
        if ($fromform = $form->get_data()) {
            $courses = $DB->get_records_sql($searchquery_user, null);
            echo getCourses($courses);
        } else {
            $courses = $DB->get_records_sql($searchquery_user, null);
            echo getCourses($courses);
        }
        break;
    }
}

echo $OUTPUT->footer();
