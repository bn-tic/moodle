<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Block to search courses by filter out course name and description.
 *
 * @package    block_search_course
 * @copyright  3i Logic<lms@3ilogic.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */
$string['pluginname']           = 'Buscador';
$string['search_course']        = 'Buscador';
$string['searchcourses']        = 'Curso';
$string['searchfromtime']       = 'Inicio curso';
$string['searchtilltime']       = 'Fin Curso';
$string['description']          = 'Search in course summary';
$string['courseformat']         = 'Formato';
$string['completioncriteria']   = 'Completion criteria';
$string['sortby']               = 'Ordenar por';
$string['search']               = 'Buscar';
$string['filterresults']        = 'Search Filter';
$string['sortheading']          = 'Ordenar por';