<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings.
 *
 * @package   block_stash
 * @copyright 2016 Adrian Greeve <adrian@moodle.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['accept'] = 'Aceptar';
$string['additem'] = 'Añadir recompensa';
$string['addnewdrop'] = 'Añadir ubicación';
$string['addnewtradeitem'] = 'Añadir item a bonus';
$string['addtradeitem'] = 'Añadir bonus';
$string['addtoinventory'] = 'Añadir item al inventario';
$string['addtrade'] = 'Añadir un widget bonus';
$string['aftercreatinglocationhelp'] = 'Una vez que haya creado la recompensa y la ubicación, debe agregar un fragmento de código a su curso para que se muestre. Después de personalizar cómo se mostrará a sus alumnos, copie el fragmento a continuación y péguelo en su contenido, en la descripción de una tarea, por ejemplo.';
$string['appearance'] = 'Apariencia';
$string['backtostart'] = 'Volver al menú';
$string['buttontext'] = 'Texto del botón';
$string['configurationtitle'] = 'Título de la recompensa';
$string['copypaste'] = 'Copie y pegue esto en un editor en diferentes actividades alrededor de su curso.';
$string['copytoclipboard'] = 'Copiar al portapapeles';
$string['cost'] = 'Valor';
$string['deletedrop'] = 'Eliminar {$a}';
$string['deleteitem'] = 'Eliminar {$a}';
$string['dropa'] = 'Ubicación \'{$a}\'';
$string['dropname'] = 'Ubicación';
$string['dropname_help'] = 'El nombre de la ubicación solo es útil para organizarlos, no se mostrará a los estudiantes.';
$string['drops'] = 'Ubicaciones';
$string['drops_help'] = '
<p>Las ubicaciones son lugares donde se encuentran las insignias. Sin una <em> ubicación </em>, un estudiante no puede recoger un artículo.</p>
<p>Las ubicaciones vienen con algunas opciones, incluida la cantidad de veces que un solo estudiante puede recogerlas y la frecuencia con la que reaparecen después de ser recogidas.</p>
<p>Por ejemplo, si sus alumnos necesitan un <em> elemento clave </em> para acceder a una actividad, lo más probable es que lo configure para que sus alumnos solo puedan recogerlo una vez en esa ubicación.</p>
<p>Pero si necesitan <em> 5 insignias </em> para acceder a otra, puede configurarla para que reaparezca cada día para alentarlos a visitar el curso todos los días.</p>
<p>Tenga en cuenta que los elementos no aparecen mágicamente en su curso, deberá agregar un código especial a su contenido para que se muestre el elemento.</p>
';
$string['dropslist'] = 'Lista de ubicaciones';
$string['dropsnippet'] = 'Fragmento de \'{$a}\'';
$string['dropsummary'] = 'Resumen de ubicaciones';
$string['edit'] = 'Editar'; // Should be replaced with the pix icon.
$string['editdrop'] = 'Editar ubicación \'{$a}\'';
$string['edititem'] = 'Editar insignia \'{$a}\'';
$string['edittrade'] = 'Editar bonus \'{$a}\'';
$string['edittradeitem'] = 'Editar insignia bonus \'{$a}\'';
$string['eginthecastle'] = 'E.j. En el castillo';
$string['eventitemacquired'] = 'Se adquirió una recompensa.';
$string['filtershortcodesnotactive'] = 'The filter plugin Shortcodes is installed but not yet enabled for this course. Visit <a href="{$a->activeurl}" target="_blank">this page</a> to enable it for this course.';
$string['filtershortcodesnotenabled'] = 'The filter plugin Shortcodes is installed but not yet <a href="{$a->enableurl}" target="_blank">enabled</a>.';
$string['filtershortcodesnotinstalled'] = 'We recommend that you install and enable the <a href="{$a->installurl}" target="_blank">filter plugin Shortcodes</a>. It makes it easier and more reliable to use the snippets. It also enables trading.';
$string['filterstashdeprecated'] = 'You are using the older filter plugin Stash which is no longer supported, we now recommend that you install and enable the <a href="{$a->installurl}" target="_blank">filter Shortcodes</a> instead. It also enables trading.';
$string['gain'] = "Ganar";
$string['gainloss'] = "Ganar o perder";
$string['gaintitle'] = "Título ganar";
$string['gaintitle_help'] = "Título de la columna de recompensas que el usuario adquirirá en este bonus.";
$string['image'] = 'Imagen';
$string['imageandbutton'] = 'Imagen y botón';
$string['item'] = 'Recompensa';
$string['items'] = 'Recompensas';
$string['itemdetail'] = 'Detalles';
$string['itemdetail_help'] = 'Detalles sobre la recompensa.';
$string['itemimage'] = 'Imagen';
$string['itemimage_help'] = 'Esta imagen se usará para mostrar la recompensa. El tamaño recomendado es de 100x100 píxeles.';
$string['itemname'] = 'Nombre de la recompensa';
$string['itemname_help'] = 'El nombre de la recompensa se mostrará a los alumnos.';
$string['items'] = 'Recompensas';
$string['itemslist'] = 'Lista de recompensas';
$string['locations'] = 'Ubicaciones';
$string['loss'] = 'Perder';
$string['losstitle'] = 'Título perder';
$string['losstitle_help'] = 'Título de la columna de recompensas que el usuario perderá en este bonus.';
$string['maxnumber'] = 'Máximo coleccionable';
$string['maxpickup'] = 'Clicks';
$string['maxpickup_help'] = 'El número de veces que cada alumno puede recoger la recompensa en esta ubicación. Por ejemplo, si configura esto en \'1\' la recompensa estará disponible una sola vez por alumno. Si se configura como \'5\', cada alumno puede adquirir esta recompensa 5 veces como máximo en esa ubicación. Un valor diferente de \'1\' es la mejor combinación para usar el \'intervalo de recogida\'.';
$string['navdrops'] = 'Ubicaciones';
$string['navinventory'] = 'Recompensas escondidas';
$string['navitems'] = 'Recompensas';
$string['navreport'] = 'Reporte';
$string['navtrade'] = 'Bonus';
$string['none'] = 'No';
$string['number'] = 'Número';
$string['pickupa'] = 'Recoger \'{$a}\'';
$string['pickupinterval'] = 'Intervalo de recogida';
$string['pickupinterval_help'] = 'Define el tiempo requerido para que el elemento vuelva a aparecer a los alumnos que ya lo recogieron. Por ejemplo, si creaste un artículo\'tarta\' puede establecer un intervalo de recogida de 24 horas para simular el tiempo que le toma al panadero hornear otra. Es importante tener en cuenta que los alumnos no se ven afectados por las capturas de recompensas de otros. Esta configuración no tiene efecto cuando \'el número de insignias\' es \'1\'.';
$string['pluginname'] = 'Cazarrecompensas';
$string['privacy:metadata:pickup'] = 'Un registro de las recompensas en un lugar determinado.';
$string['privacy:metadata:pickup:dropid'] = 'El ID de la ubicación.';
$string['privacy:metadata:pickup:lastpickup'] = 'La hora a la que ocurrió la última recogida de recompensas.';
$string['privacy:metadata:pickup:pickupcount'] = 'La cantidad recogida en esa ubicación.';
$string['privacy:metadata:pickup:timecreated'] = 'La hora a la que se creó el registro.';
$string['privacy:metadata:pickup:timemodified'] = 'La hora a la que se modificó el registro por última vez.';
$string['privacy:metadata:pickup:userid'] = 'La identificación del usuario que recogió la recompensa.';
$string['privacy:metadata:useritem'] = 'Registro de las recompensas que posee un alumno.';
$string['privacy:metadata:useritem:itemid'] = 'ID de la recompensa.';
$string['privacy:metadata:useritem:quantity'] = 'Cantidad en propiedad.';
$string['privacy:metadata:useritem:timecreated'] = 'La hora a la que se creó el registro.';
$string['privacy:metadata:useritem:timemodified'] = 'La hora a la que se modificó el registro.';
$string['privacy:metadata:useritem:userid'] = 'ID del usuario que recogió la recompensa.';
$string['quantity'] = 'Cantidad';
$string['reallydeletedrop'] = '¿Está seguro que quiere eliminar esta ubicación?';
$string['reallydeleteitem'] = '¿Está seguro que desea eliminar esta recompensa?';
$string['reallyresetstashof'] = '¿Está seguro que desea restablecer el contador de este bonus {$a}?';
$string['report'] = 'Reporte';
$string['resetstashof'] = 'Reset contador de {$a}';
$string['saveandnext'] = 'Guardar y Siguiente';
$string['savechanges'] = 'Guardar cambios';
$string['settings'] = 'Configuración';
$string['setup'] = 'Setup';
$string['shortcode:stashdrop'] = 'Muestra una recompensa para que un usuario lo recoja';
$string['shortcode:stashtrade'] = 'Muestra el widget de bonus';
$string['snippet'] = 'Snippet';
$string['stash'] = 'Cazarrecompensas';
$string['stash:acquireitems'] = 'El usuario puede adquirir recompensas';
$string['stash:addinstance'] = 'Agregar un bloque a una página';
$string['stash:view'] = 'Ver el escondite y su contenido';
$string['stashdisabled'] = 'El escondite no está habilitado. ¿Se agregó el bloque al curso?';
$string['text'] = 'Texto';
$string['title'] = 'Título';
$string['trade'] = 'Bonus';
$string['tradecreated'] = 'Bonus creado \'{$a}\'';
$string['tradeitem'] = 'Bonus item';
$string['tradeitems'] = 'Bonus items';
$string['tradelist'] = 'Lista de widgets "bonus"';
$string['tradename'] = 'Nombre bonus';
$string['tradename_help'] = 'El nombre del widget "Bonus", esto se puede mostrar a los alumnos.';
$string['tradewidget'] = 'Creando un bonus.';
$string['tradewidget_help'] = '<p> Este widget de bonus permite obtener elementos de la columna izquierda a cambio de perder elementos en la columna derecha. </p> <p> Haga clic en el símbolo + en la parte inferior del cuadro para agregar elementos a esa columna. </ p > <p> La cantidad de una recompensa debe ser un número positivo. </p>';
$string['thedrophasbeendeleted'] = 'La ubicación \'{$a}\' fue eliminada';
$string['theitemhasbeendeleted'] = 'La recompensa \'{$a}\' fue eliminada';
$string['thestashofhasbeenreset'] = 'El escondite de {$a} fue reiniciado';
$string['thetradehasbeendeleted'] = 'El bonus widget \'{$a}\' ha sido eliminado';
$string['thetradeitemhasbeendeleted'] = 'El item del bonus ha sido eliminado';
$string['unlimited'] = 'Ilimitado';
$string['whataredrops'] = '¿Qué son las ubicaciones?';
$string['whatisadrophelp'] = 'Una ubicación es un lugar donde tiene la intención de mostrar su recompensa.';
$string['whatisatradedrophelp'] = 'Una ubicación es un lugar donde tiene la intención de mostrar su widget bonus.';
$string['whatisthisthing'] = '¿Que es esta cosa? Estoy seguro de que puedes encontrar uso para ello.';
$string['whatsthis'] = '¿Qué es esto?';
$string['whatsnext'] = '¿Lo próximo?';
$string['yourinventoryisempty'] = '¿Aún sin recompensas? Navega por el curso y obtenlas mientras aprendes.';

// Deprecated.
$string['filterstashnotactive'] = 'The filter plugin is installed but not yet enabled for this course. Visit <a href="{$a->activeurl}" target="_blank">this page</a> to enable it for this course.';
$string['filterstashnotenabled'] = 'The filter plugin is installed but not yet <a href="{$a->enableurl}" target="_blank">enabled</a>.';
$string['filterstashnotinstalled'] = 'We recommend that you install and enable the <a href="{$a->installurl}" target="_blank">filter plugin for Stash</a>. It makes it easier and more reliable to use the snippets. It also enables trading.';
$string['filterstashwrongversion'] = 'The filter plugin that you have installed is an early version and does not work with trading. Please visit <a href="{$a}" target="_blank">this page</a> to get the latest version.';
