<?php
$string['pluginname'] = 'Manage Event block';
$string['manage_events'] = 'Gestión de Eventos';
$string['manage_events:addinstance'] = 'Add events to calendar';
$string['manage_events:myaddinstance'] = 'Add a new simple Manage Event block to the My Moodle page';
$string['access_button'] = 'Gestionar Eventos';
$string['access_info'] = 'Solo profesores:';
$string['submit'] = 'Guardar';
$string['form'] = 'Gestión de eventos';
$string['pagetitle'] = 'Gestión de eventos';
$string['form_text'] = 'Nuevo Evento';
$string['mintime'] = 'Desde';
$string['maxtime'] = 'Hasta';
$string['mintime_help'] = 'Fecha de inicio del evento';
$string['maxtime_help'] = 'Fecha fin del evento';
$string['form_help'] = 'Formulario que permite la creación de nuevos eventos';
$string['userid'] = 'Usuario';
$string['userid_help'] = 'Usuario matriculado al que añadir evento';
$string['eventname'] = 'Evento';
$string['eventname_help'] = 'Breve descripción del evento';

