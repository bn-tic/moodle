<?php

defined('MOODLE_INTERNAL') || die();

global $CFG;

require_once($CFG->libdir . '/formslib.php');

// Form to select start and end date ranges and session time.
class manage_event_block_selection_form extends moodleform {

    public function definition() {
        $courseid = required_param('courseid', PARAM_INT);
        
        $mform = & $this->_form;

        $mform->addElement('header', 'general', get_string('form', 'block_manage_events'));
        $mform->addHelpButton('general', 'form', 'block_manage_events');

        $mform->addElement('html', html_writer::tag('p', get_string('form_text', 'block_manage_events')));

        $mform->addElement('date_time_selector', 'mintime', get_string('mintime', 'block_manage_events'));
        $mform->addHelpButton('mintime', 'mintime', 'block_manage_events');

        $mform->addElement('date_time_selector', 'maxtime', get_string('maxtime', 'block_manage_events'));
        $mform->addHelpButton('maxtime', 'maxtime', 'block_manage_events');

        //get enrolled users
        $students = array();
        $select_array = array();
        
        $students = get_enrolled_users(context_course::instance($courseid));
        
        foreach ($students as $key => $value) {
            $select_array[$key] = $value->firstname . " " .$value->lastname;
        }
        
        $mform->addElement('select', 'userid', get_string('userid', 'block_manage_events'), $select_array);
        $mform->addHelpButton('userid', 'userid', 'block_manage_events');
        
        $mform->addElement('text', 'eventname', get_string('eventname', 'block_manage_events'),'size="45"');
        $mform->addHelpButton('eventname', 'eventname','block_manage_events');
        
        // Buttons.
        $this->add_action_buttons(false, get_string('submit', 'block_manage_events'));
    }

}
