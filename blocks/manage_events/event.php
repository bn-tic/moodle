<?php

global $DB, $PAGE, $OUTPUT;

require_once("../../config.php");

// Input params.
$courseid = required_param('courseid', PARAM_INT);
$instanceid = required_param('instanceid', PARAM_INT);

// Require course login.
$course = $DB->get_record("course", array("id" => $courseid), '*', MUST_EXIST);
require_course_login($course);

// Require capability to use this plugin in block context.
$context = context_block::instance($instanceid);
require_capability('block/manage_events:use', $context);


//require user dedication
require_once('manage_events_lib.php');

//submit form params
$action = optional_param('action', '', PARAM_ALPHANUM);
$id = optional_param('id', 0, PARAM_INT);

// Current url.
$pageurl = new moodle_url('/blocks/manage_events/event.php');
$pageurl->params(array(
    'courseid' => $courseid,
    'instanceid' => $instanceid,
    'action' => $action,
    'id' => $id,
));

// Page format.
$PAGE->set_context($context);
$PAGE->set_pagelayout('report');
$PAGE->set_pagetype('course-view-' . $course->format);
$PAGE->navbar->add(get_string('pluginname', 'block_manage_events'), new moodle_url('/blocks/manage_events/event.php', array('courseid' => $courseid, 'instanceid' => $instanceid)));
$PAGE->set_url($pageurl);
$PAGE->set_title(get_string('pagetitle', 'block_manage_events', $course->shortname));
$PAGE->set_heading($course->fullname);

// Load libraries.
require_once('event_form.php');

// Load calculate params from form, request or set default values.
$mform = new manage_event_block_selection_form($pageurl, null, 'get');
if ($mform->is_submitted()) {
    // Params from form post.
    $formdata = $mform->get_data();
    $mintime = $formdata->mintime;
    $maxtime = $formdata->maxtime;
    $userid = $formdata->userid;
    $eventname = $formdata->eventname;
    $action = "add";
} else {
    // Params from request or default values.
    $mintime = optional_param('mintime', $course->startdate, PARAM_INT);
    $maxtime = optional_param('maxtime', time(), PARAM_INT);
    $userid = optional_param('userid', 0, PARAM_INT);
    $eventname = optional_param('eventname', '', PARAM_ALPHANUMEXT);
    $mform->set_data(array('mintime' => $mintime, 'maxtime' => $maxtime, 'userid' => $userid, 'eventname' => $eventname));
}

// Url with params for links inside tables.
$pageurl->params(array(
    'mintime' => $mintime,
    'maxtime' => $maxtime,
    'userid' => $userid
));

if ($action == "add") {

    $data = new stdClass();

    //insert data
    $data->name = $eventname;
    $data->description = '';
    $data->format = 1;
    $data->courseid = 0;
    $data->groupid = 0;
    $data->userid = $userid;
    $data->repeatid = 0;
    $data->modulename = 0;
    $data->instance = 0;
    $data->type = 0;
    $data->eventtype = 'user';
    $data->timestart = $mintime;
    $data->timeduration = $maxtime - $mintime;
    $data->timesort = NULL;
    $data->visible = 1;
    $data->uuid = '';
    $data->sequence = 1;
    $data->timemodified = $mintime;
    $data->subcriptionid = NULL;
    $data->priority = NULL;

    $insert = $DB->insert_record('event', $data);

}

// START PAGE: layout, headers, title, boxes...
echo $OUTPUT->header();

// Form.
$mform->display();

echo $OUTPUT->box_start();

if ($insert) {
    echo '<div class="prom-box prom-box-info">
        <h3>Evento insertado</h3>
        <p>Evento insertado correctamente</p>
    </div>';
}

// END PAGE.
echo $OUTPUT->box_end();
echo $OUTPUT->footer();
