<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('../../config.php');
require_once("$CFG->libdir/formslib.php");
require_once('testimonial_form.php');
require_once(dirname(__FILE__) . '/../../TestimonialClass.php');


require_login();
global $DB, $OUTPUT, $PAGE, $CFG, $USER;
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('base');
$PAGE->set_title(get_string("pluginname", 'block_testimonial'));
$PAGE->set_url('/blocks/testimonial/view.php');
$PAGE->navbar->ignore_active();
$PAGE->navbar->add(get_string("pluginname", 'block_testimonial'));


$filemanageropts = array('subdirs' => 0, 'maxbytes' => '1024', 'accepted_types' => '.jpg', '.png', 'maxfiles' => 1, 'context' => $context);
$customdata = array('filemanageropts' => $filemanageropts);

$mform = new new_testimonial_form(null, $customdata);


// ---------
// CONFIGURE FILE MANAGER
// ---------
// From http://docs.moodle.org/dev/Using_the_File_API_in_Moodle_forms#filemanager
//$itemid = 0; // This is used to distinguish between multiple file areas, e.g. different student's assignment submissions, or attachments to different forum posts, in this case we use '0' as there is no relevant id to use
// Fetches the file manager draft area, called 'attachments' 
//$draftitemid = file_get_submitted_draft_itemid('attachments');
// Copy all the files from the 'real' area, into the draft area
//file_prepare_draft_area($draftitemid, $context->id, 'testimonial', 'attachment', $itemid, $filemanageropts);
// Prepare the data to pass into the form - normally we would load this from a database, but, here, we have no 'real' record to load
//$entry = new stdClass();
//$entry->attachments = $draftitemid; // Add the draftitemid to the form, so that 'file_get_submitted_draft_itemid' can retrieve it
// --------- 
// Set form data
// This will load the file manager with your previous files
//$mform->set_data($entry);



echo $OUTPUT->header();

$redirecturl = new moodle_url('/blocks/testimonial/view.php', ['redirect' => 0]);

$id = $_GET['id'];

//deleting
if($_GET['op'] == "delete"){
    Testimonial::delete_testimonial($id);
    //redirect
    redirect($redirecturl, "Volviendo al listado de reseñas");
}

// ----------
// Form Submit Status
// ----------
if ($mform->is_cancelled()) {
    // CANCELLED
    echo '<h1>Cancelado</h1>';
} else if ($data = $mform->get_data()) {
    
    switch ($data->op){
        case 'new':
            Testimonial::insert_testimonial($data->name, $data->surname, $data->testimonial['text'], $data->course);
            break;        
        case 'edit':
            Testimonial::update_testimonial($data->id, $data->name, $data->surname, $data->testimonial['text'], $data->course);
            break;
    }
    
    //redirect
    redirect($redirecturl, "Volviendo al listado de reseñas");
/*
    // Save the files submitted
    file_save_draft_area_files($draftitemid, $context->id, 'testimonial', 'attachment', $itemid, $filemanageropts);


    $fs = get_file_storage();
    if ($files = $fs->get_area_files($context->id, 'testimonial', 'attachment', '0', 'sortorder', false)) {

        // Look through each file being managed
        foreach ($files as $file) {
            // Build the File URL. Long process! But extremely accurate.
            $fileurl = moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $file->get_filename());

            // Display the image
            echo "<br /><img src='$fileurl' />";
        }
    } else {
        echo '<p>Please upload an image first</p>';
    }*/
} else {
    // FAIL / DEFAULT
    $mform->display();
}

echo $OUTPUT->footer();
