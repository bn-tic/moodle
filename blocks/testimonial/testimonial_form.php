<?php

/*
 * FORM TO REGISTER TESTIMONIAL
 */

defined('MOODLE_INTERNAL') || die();

require_once(dirname(__FILE__) . '/../../config.php');
require_once(dirname(__FILE__) . '/../../CustomClass.php');
require_once($CFG->libdir.'/formslib.php');

class new_testimonial_form extends moodleform {

    //Add elements to form
    public function definition() {
        global $CFG;
        $id = $_GET['id'];
        
        
        $mform = $this->_form; // Don't forget the underscore! 
        //content form
        $mform->addElement('hidden', 'op');
        $mform->setDefault('op', $_GET['op']);
        
        $mform->addElement('hidden', 'id');
        $mform->setDefault('id', $_GET['id']);
        
        $mform->addElement('text', 'name', get_string('name', 'block_testimonial'));
        $mform->addElement('text', 'surname', get_string('surname', 'block_testimonial'));

        $mform->addElement('editor', 'testimonial', get_string('testimonial_field', 'block_testimonial'));
        $mform->setType('testimonial', PARAM_RAW);

        $courses = Custom::get_all_courses();
        $coursenames = array();
        foreach ($courses as $course) {
            $coursenames[$course->id] = $course->fullname;
        }
        $options = array(
            'multiple' => false,
            'noselectionstring' => get_string('allcourses', 'block_testimonial'),
        );
        
        /*$filemanageropts = $this->_customdata['filemanageropts'];
        $mform->addElement('filemanager', 'attachments',get_string('attachment', 'block_testimonial'), null, $filemanageropts);*/

        
        $mform->addElement('autocomplete', 'course', get_string('course', 'block_testimonial'), $coursenames, $options);
        
        if($_GET['op'] == 'edit'){
            $testimonial = Testimonial::get_testimonial_byid($id);
            $mform->setDefault('name', $testimonial->name);
            $mform->setDefault('surname', $testimonial->surname);
            $mform->setDefault('testimonial', array('text'=>$testimonial->testimonial, 'format'=>FORMAT_HTML));
            $mform->setDefault('course', $testimonial->courseid);
        }
        
        
        //$mform->addElement('button', 'submit', get_string('submit_recalc','block_iomad_progress'));
        $this->add_action_buttons(false, get_string('submit_testimonial','block_testimonial'));
        
    }

}

// ===============
//
//	Plugin File
//
// ===============
// I M P O R T A N T
// 
// This is the most confusing part. For each plugin using a file manager will automatically
// look for this function. It always ends with _pluginfile. Depending on where you build
// your plugin, the name will change. In case, it is a local plugin called file manager.

/*function local_filemanager_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options=array()) {
    global $DB;

    if ($context->contextlevel != CONTEXT_SYSTEM) {
        return false;
    }

    require_login();

    if ($filearea != 'attachment') {
        return false;
    }

    $itemid = (int)array_shift($args);

    if ($itemid != 0) {
        return false;
    }

    $fs = get_file_storage();

    $filename = array_pop($args);
    if (empty($args)) {
        $filepath = '/';
    } else {
        $filepath = '/'.implode('/', $args).'/';
    }

    $file = $fs->get_file($context->id, 'local_filemanager', $filearea, $itemid, $filepath, $filename);
    if (!$file) {
        return false;
    }

    // finally send the file
    send_stored_file($file, 0, 0, true, $options); // download MUST be forced - security!
}*/
