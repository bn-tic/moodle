<?php
class block_testimonial extends block_base {
        function init() {
        $this->title = get_string('pluginname', 'block_testimonial');
    }

    function has_config() {
        return false;
    }

    function applicable_formats() {
        return array('all' => true);
    }

    function specialization() {
        $this->title = isset($this->config->title) ? format_string($this->config->title) : format_string(get_string('configtitle', 'block_testimonial'));
    }

    function instance_allow_multiple() {
        return true;
    }
}