<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Block to display enrolled, completed, inprogress and undefined courses according to course completion criteria named 'grade' based on login user.
 *
 * @package    block_search_course
 * @copyright  3i Logic<lms@3ilogic.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */
require_once('../../config.php');
require_once(dirname(__FILE__) . '/../../TestimonialClass.php');



require_login();
global $DB, $OUTPUT, $PAGE, $CFG, $USER;
 $context = context_system::instance();
  $PAGE->set_context($context);
  $PAGE->set_pagelayout('base');
  $PAGE->set_title(get_string("pluginname", 'block_testimonial'));
  $pageurl = '/blocks/testimonial/view.php';
  $PAGE->set_url($pageurl);
  $PAGE->navbar->ignore_active();
  $PAGE->navbar->add(get_string("pluginname", 'block_testimonial'));
 
echo $OUTPUT->header();

$testimonials = Testimonial::get_testimonials();

$table = '<div class="container">';
    $table .= '<div class="row">';
        $table .= '<div class="col-md-12">';
            $table .= '<div>';
                $table .= '<a style="margin-bottom: 30px;" class="btn btn-primary" href="edit-testimonial.php?op=new"><i class="fa fa-plus-square"></i> Añadir reseña</a>';
            $table .= '</div>';
            $table .= '<table class="table table-responsive table-striped">';
                $table .= '<tr class="info">';
                    $table .= '<td><b>Editar</b></td>';
                    $table .= '<td><b>Alumno</b></td>';
                    $table .= '<td><b>Curso</b></td>';
                    $table .= '<td><b>Reseña</b></td>';
                    $table .= '<td><b>Fecha</b></td>';
                $table .= '</tr>';
                    foreach ($testimonials as $testimonial){
                        $table .= '<tr style="background-color:#e1e0e0">';
                        $table .= '<td style="width: 105px">'
                                . '<a class="btn btn-primary" style="margin-right:5px;" href="edit-testimonial.php?op=edit&id='.$testimonial->id.'"><i class="fa fa-pencil"></i></a>'
                                . '<a class="btn btn-danger" href="edit-testimonial.php?op=delete&id='.$testimonial->id.'"><i class="fa fa-trash-o"></i></a>'
                                . '</td>';
                        $table .= '<td>'.$testimonial->name. ' ' . $testimonial->surname.'</td>';
                        $table .= '<td>'. Testimonial::get_coursebyid($testimonial->courseid).'</td>';
                        $table .= '<td>'.$testimonial->testimonial.'</td>';
                        $table .= '<td>'.date('d/m/Y',$testimonial->date).'</td>';
                        $table .= '</tr>';
                    }
                
            $table .= '</table>';
        $table .= '</div>';
    $table .= '</div>';
$table .= '</div>';



echo $table;
echo $OUTPUT->footer();
