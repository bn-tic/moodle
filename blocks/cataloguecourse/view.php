<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Block to display enrolled, completed, inprogress and undefined courses according to course completion criteria named 'grade' based on login user.
 *
 * @package    block_cataloguecourse
 * @copyright  3i Logic<lms@3ilogic.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */
require_once('../../config.php');
require_once($CFG->libdir . '/coursecatlib.php');
require_once($CFG->libdir . '/formslib.php');
require_login();
global $OUTPUT, $PAGE, $CFG, $USER;
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('base');
$PAGE->set_title(get_string("pluginname", 'block_cataloguecourse'));
$pageurl = '/blocks/cataloguecourse/view.php';
$PAGE->set_url($pageurl);
$PAGE->navbar->ignore_active();
$PAGE->navbar->add(get_string("pluginname", 'block_cataloguecourse'));

class company_courses_form extends moodleform {

    protected $context = null;
    protected $selectedcompany = 0;
    protected $departmentid = 0;
    protected $subhierarchieslist = null;
    protected $companydepartment = 0;
    protected $course = 0;

    public function __construct($actionurl, $course, $context, $companyid, $departmentid, $parentlevel) {
        global $USER;
        $this->selectedcompany = $companyid;
        $this->context = $context;
        $this->departmentid = $departmentid;
        $this->course = $course;

        $options = array('context' => $this->context,
            'companyid' => $this->selectedcompany,
            'departmentid' => $this->departmentid,
            'subdepartments' => $this->subhierarchieslist,
            'course' => $this->course,
            'parentdepartmentid' => $parentlevel,
            'shared' => false,
            'licenses' => true,
            'partialshared' => true);

        parent::__construct($actionurl);
    }

    public function definition() {
        $this->_form->addElement('hidden', 'companyid', $this->selectedcompany);
        $this->_form->setType('companyid', PARAM_INT);
    }

    public function definition_after_data() {
        $mform = & $this->_form;

        // Adding the elements in the definition_after_data function rather than in the
        // definition function  so that when the currentcourses or potentialcourses get changed
        // in the process function, the changes get displayed, rather than the lists as they
        // are before processing.

        $context = context_system::instance();
        $company = new company($this->selectedcompany);
        $mform->addElement('hidden', 'departmentid', $this->departmentid);
        $mform->setType('departmentid', PARAM_INT);
        
        $mform->addElement('hidden', 'course', $this->course);
        $mform->setType('course', PARAM_INT);

        $mform->addElement('html', '
                      <input name="add" id="add" type="submit" value="&#x25C4;&nbsp;' .
                get_string('add') . '" title="Add" />');
    }

    public function process($course) {

        global $DB;
        $context = context_system::instance();
        // Process incoming assignments.
        if (optional_param('add', false, PARAM_BOOL) && confirm_sesskey()) {
            if (!empty($course)) {
                $company = new company($this->selectedcompany);

                // Check if its a shared course.
                if ($DB->get_record_sql("SELECT id FROM {iomad_courses}
                                         WHERE courseid=$course
                                         AND shared != 0")) {
                    $sharingrecord = new stdclass();
                    $sharingrecord->courseid = $course;
                    $sharingrecord->companyid = $this->selectedcompany;
                    
                    $DB->insert_record('company_shared_courses', $sharingrecord);
                    
                    $company_course = new stdClass();
                    $company_course->companyid = $this->selectedcompany;
                    $company_course->courseid = $course;
                    $company_course->departmentid = $this->departmentid;
                    $company_course->autoenrol = 0;
                    
                    $DB->insert_record('company_course', $company_course);
                }
            }
        }
        
        redirect(new moodle_url('/blocks/cataloguecourse/view.php'), "Curso añadido correctamente");
    }
}

//get department and company logged user
$user_info = "SELECT * FROM mco_company_users WHERE mco_company_users.userid = " . $USER->id;
$data = $DB->get_record_sql($user_info, null);

$companyid = $USER->id == 2 ? 11 : $data->companyid;


//get shared courses
$shared_sql = "SELECT mco_course.* FROM mco_course INNER JOIN mco_company_shared_courses ON mco_company_shared_courses.courseid = mco_course.id
WHERE mco_company_shared_courses.companyid = $companyid";

$shared_courses = $DB->get_records_sql($shared_sql, null);

//get cep courses to sharing
$sql = "SELECT mco_course.id, mco_course.fullname, mco_course.imgcatalogue 
FROM mco_course 
INNER JOIN mco_iomad_courses ON mco_iomad_courses.courseid = mco_course.id 
WHERE mco_course.category = 17 AND mco_iomad_courses.shared != 0";

if($companyid != 11)
    $sql .= " AND mco_course.id NOT IN (SELECT mco_company_shared_courses.courseid FROM mco_company_shared_courses WHERE mco_company_shared_courses.companyid = $companyid)";

$courses = $DB->get_records_sql($sql, null);

// Set the companyid
$parentlevel = company::get_company_parentnode($companyid);
$companydepartment = $parentlevel->id;
$syscontext = context_system::instance();
$company = new company($companyid);


echo $OUTPUT->header();

?>


<div class="row-fluid">
        <div class="span6">
            <h3>Cursos a&ntilde;adidos</h3>
<?php

foreach ($shared_courses as $shared_course) {
    ?>
        <div class="coursebox clearfix even">
            <div class="info">
                    <div class="span3">
            <?php
            if ($shared_course->imgcatalogue != '') {
                echo "<img src='images/$shared_course->imgcatalogue' alt='course' class='img-responsive' style='max-width: 75%'>";
            } else {
                ?>
                            <img src="images/generic.jpg" alt="course" class="img-responsive" style="max-width: 75%">
                        <?php } ?>
                    </div>
                    <div class="span8">
                        <h3><?php echo $shared_course->fullname; ?></h3>
                    </div>
                </div>
            </div>
    <?php }
?>
    </div>
    
    <div class="span6">
        <h3>Cursos disponibles</h3>
<?php

foreach ($courses as $course) {
$mform = new company_courses_form($PAGE->url, $course->id, $context, $companyid, $companydepartment, $parentlevel);     
if(array_key_exists('add',$_POST)){
    $mform->process($_POST['course']);
    break;
}

    ?>
        <div class="coursebox clearfix even">
            <div class="info">
                    <div class="span3">
            <?php
            if ($course->imgcatalogue != '') {
                echo "<img src='images/$course->imgcatalogue' alt='course' class='img-responsive' style='max-width: 75%'>";
            } else {
                ?>
                            <img src="images/generic.jpg" alt="course" class="img-responsive" style="max-width: 75%">
                        <?php } ?>
                    </div>
                    <div class="span8">
                        <h3><?php echo $course->fullname; ?></h3>
                        <?php if ($data) { $mform->display(); }?>
                    </div>
                </div>
            </div>
    <?php }
?>
    </div>
</div>
<?php
echo $OUTPUT->footer();