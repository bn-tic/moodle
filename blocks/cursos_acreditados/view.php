<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Block to display cursos acreditados.
 *
 * @package    block_cursos_acreditados
 * @copyright  3i Logic<lms@3ilogic.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */
require_once('../../config.php');
require_once($CFG->libdir . '/formslib.php');
require_once(dirname(__FILE__) . '/../../CustomClass.php');

require_login();
global $OUTPUT, $PAGE, $CFG, $USER;
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('base');
$PAGE->set_title(get_string("pluginname", 'block_cursos_acreditados'));
$pageurl = '/blocks/cursos_acreditados/view.php';
$PAGE->set_url($pageurl);
$PAGE->navbar->ignore_active();
$PAGE->navbar->add(get_string("pluginname", 'block_cursos_acreditados'));

//all passed
$quizspassed = 0;

/* courses */
$courses = Custom::get_accredited_course();

echo $OUTPUT->header();

echo "<h5>Listado de cursos acreditados</h5>";
echo html_writer::start_tag('div', array('class' => 'row'));
echo html_writer::start_tag('div', array('class' => 'span12'));

/* course table */
echo html_writer::start_tag('table', array('class' => 'table'));

echo html_writer::start_tag('tr', array('class' => ''));
echo html_writer::start_tag('td', array('class' => ''));
echo "ID";
echo html_writer::end_tag('td');
echo html_writer::start_tag('td', array('class' => ''));
echo "CURSO";
echo html_writer::end_tag('td');
echo html_writer::end_tag('tr');
foreach ($courses as $course) {
    $students = Custom::get_accredited_students_by_course($course->id);

    echo html_writer::start_tag('tr', array('class' => 'info'));
    echo html_writer::start_tag('td', array('class' => 'info'));
    echo $course->id;
    echo html_writer::end_tag('td');
    echo html_writer::start_tag('td', array('class' => 'info', 'data-toggle' => 'collapse', 'data-target' => '#' . $course->id, 'aria-expanded' => 'true', 'aria-controls' => '#' . $course->id));
    echo $course->fullname;

    //start courses
    echo html_writer::start_tag('table', array('class' => 'table collapse show', 'data-parent' => '#accordion', 'id' => $course->id));
    echo html_writer::start_tag('tr');
    echo html_writer::start_tag('td');
    echo "ID";
    echo html_writer::end_tag('td');
    echo html_writer::start_tag('td');
    echo "Nombre";
    echo html_writer::end_tag('td');
    echo html_writer::start_tag('td');
    echo "Email";
    echo html_writer::end_tag('td');
    echo html_writer::end_tag('tr');
    echo html_writer::start_tag('tr');

    //students
    foreach ($students as $student) {
        $quizs = Custom::get_tests_by_course($course->id);
        echo html_writer::start_tag('tr');
        echo html_writer::start_tag('td');
        echo $student->id;
        echo html_writer::end_tag('td');
        echo html_writer::start_tag('td');
        echo $student->nombre;
        echo html_writer::end_tag('td');
        echo html_writer::start_tag('td');
        echo $student->email;
        //start table tests
        echo html_writer::start_tag('table', array('class' => 'table'));
        echo html_writer::start_tag('tr', array('class' => 'warning'));
        echo html_writer::start_tag('td');
        echo "ID";
        echo html_writer::end_tag('td');
        echo html_writer::start_tag('td');
        echo "NOMBRE TEST";
        echo html_writer::end_tag('td');
        echo html_writer::start_tag('td');
        echo "PUNTUACI&Oacute;N";
        echo html_writer::end_tag('td');
        echo html_writer::end_tag('tr');

        foreach ($quizs as $quiz) {
            $attempt = Custom::get_attempt_by_quiz_and_user($quiz->id, $student->id);
            $passed = ($attempt->sumgrades * 100) / $quiz->sumgrades;

            if (round($passed) >= 50) {
                $class = "success";
                $quizspassed ++;
            } else
                $class = "danger";

            echo html_writer::start_tag('tr', array('class' => $class));
            echo html_writer::start_tag('td');
            echo $quiz->id;
            echo html_writer::end_tag('td');
            echo html_writer::start_tag('td');
            echo $quiz->name;
            echo html_writer::end_tag('td');
            echo html_writer::start_tag('td');
            echo round($attempt->sumgrades) . " / " . round($quiz->sumgrades);
            echo html_writer::end_tag('td');

            echo html_writer::end_tag('tr');
        }

        echo html_writer::end_tag('table');
        //end table test

        echo html_writer::end_tag('td');
        echo html_writer::end_tag('tr');

        //actualizamos los que aprueban el curso
        if (count($quizs) == $quizspassed) {
            Custom::update_acreditado($course->id, $student->id);
        }
        //a 0 para el siguiente student
        $quizspassed = 0;
    }

    echo html_writer::end_tag('tr');
    echo html_writer::end_tag('table');
    //end table student

    echo html_writer::end_tag('td');
    echo html_writer::end_tag('tr');
}

echo html_writer::end_tag('table');

/* end row and div */
echo html_writer::end_tag('div');
echo html_writer::end_tag('div');
?>
<form action="" method="post">
    <input type="submit" value="Send details to embassy" />
    <input type="hidden" name="button_pressed" value="1" />
</form>

<?php

if(isset($_POST['button_pressed']))
{
    $to = 'cristofer.quesada@bn-tic.es';
    $subject = 'the subject';
    $message = 'hello';
    $headers = 'From: cristofer.quesada@bn-tic.es' . "\r\n" .
        'Reply-To: cristofer.quesada@bn-tic.es' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

    mail($to, $subject, $message, $headers);

    echo 'Email Sent.';
}

echo $OUTPUT->footer();
