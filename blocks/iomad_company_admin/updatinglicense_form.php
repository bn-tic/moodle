<?php

/*
 * FORM TO UPDATING LICENSE DATE
 */

require_once(dirname(__FILE__) . '/../../config.php');

class updating_license_form extends moodleform {

    //Add elements to form
    public function definition() {
        global $CFG;

        $userid = required_param('userid', PARAM_INT);
        $enrolid = required_param('enrolid', PARAM_INT);

        $enrol = Custom::get_enrol_by_user_and_enrolid($userid, $enrolid);

        $mform = $this->_form; // Don't forget the underscore!
        $mform->addElement('html', '<ul class="thumbnails"><li class="span4"><div class="thumbnail">');

        if ($enrol->imgcatalogue)
            $mform->addElement('html', "<img src='../cataloguecourse/images/$enrol->imgcatalogue' alt='course' class='catalogue'>");
        else
            $mform->addElement('html', "<img src='../cataloguecourse/images/generic.jpg' alt='course' class='catalogue'>");

        $mform->addElement('html', "<h3>{$enrol->fullname}</h3>");
        $mform->addElement('html', "<div class='alert alert-info'>Licencia $enrol->name ($enrol->timeendformated)</div>");

        $mform->addElement('html', '<hr>');
        $mform->addElement('date_time_selector', 'timeend', 'Nueva fecha');
        $mform->setDefault('timeend', $enrol->timeend);

        $mform->addElement('hidden', 'enrolid', $enrolid);

        $mform->addElement('html', '<hr>');


        $mform->addElement('hidden', 'userid', $userid);

        $this->add_action_buttons(false, 'Actualizar licencia');
        $mform->addElement('html', '</ul></li></div>');
    }

}
