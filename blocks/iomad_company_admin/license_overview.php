<?php

// Include required files.
require_once(dirname(__FILE__) . '/../../config.php');
require_once(dirname(__FILE__) . '/../../CustomClass.php');

require_once('updatinglicense_form.php');
require_once('searchlicense_form.php');

// Start page output.
echo $OUTPUT->header();

echo html_writer::start_tag('div', array('class' => 'row-fluid'));
echo html_writer::start_tag('div', array('class' => 'span12'));

$enrols = Custom::get_enrols_by_user($_GET['userid']);

$searchilicense_form = new course_license_search_form();
$search = optional_param('search', null, PARAM_RAW);
$toform['search'] = $search;
$searchilicense_form->set_data($toform);

//render
$searchilicense_form->display();

//init form
$fromform = $searchilicense_form->get_data();

if (!$_GET['enrolid']) {
    if ($fromform = $searchilicense_form->get_data())
        echo getCourses($enrols);
    else
        echo getCourses($enrols);
} else{
    $updating_form = new updating_license_form(new moodle_url('/blocks/iomad_company_admin/updatinglicense.php'), null, 'get');

    //render form
    $updating_form->display();
}


echo html_writer::end_tag('div');
//end row
echo html_writer::end_tag('div');

echo $OUTPUT->footer();
