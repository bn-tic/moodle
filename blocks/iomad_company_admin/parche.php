<?php

	function count_slots($companyid, $courseid){
		global $DB;
		$slots = $DB->get_record_sql('SELECT SUM(slots) c FROM mco_company_course_slots WHERE companyid = :coid and courseid = :cid', array('coid'=>$companyid, 'cid'=>$courseid));
		
		//obtenemos los usuarios asignados al curso:
		$used = $DB->get_record_sql('SELECT COUNT(1) c FROM mco_companylicense_users clu join mco_companylicense cl on cl.id = clu.licenseid WHERE clu.licensecourseid = :cid and cl.companyid = :coid', array('cid'=>$courseid, 'coid' => $companyid));
		$used = (int)$used->c;
		
		$slots->c = $slots->c-$used;
		return (int)$slots->c;
	}

	function get_course_slots($licenseid, $courseid, $tutorized){
		global $DB;
		$opts = array(
			'licenseid'=>$licenseid,
			'courseid'=>$courseid
		);
		if(isset($tutorized)){ $opts['tutorized'] = (int)$tutorized; }
		$row = $DB->get_record('company_course_slots', $opts);
		
		//obtenemos los usuarios asignados al curso:
		$used = $DB->get_record_sql('SELECT COUNT(1) c FROM mco_companylicense_users WHERE licenseid = :l AND licensecourseid = :c',array('l'=>$licenseid, 'c'=>$courseid));
		$used = (int)$used->c;
		$row->slots -= $used;
		
		return $row;
	}

	function filter_courses($licenseid, $courseid, $tutorized){
		global $DB;
		$opts = array(
			'licenseid'=>$licenseid,
			'courseid'=>$courseid
		);
		if($tutorized === 1){ $opts['tutorized'] = 1; }
		$row = $DB->get_record('company_course_slots', $opts);
		if($row->slots > 0){ return $courseid; }
		return null;
	}
?>