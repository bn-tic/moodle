<?php

// Include required files.
require_once(dirname(__FILE__) . '/../../config.php');
require_once(dirname(__FILE__) . '/../../CustomClass.php');


echo $OUTPUT->header();

$userid = $_GET['userid'];
$enrolid = $_GET['enrolid'];
$newdatelicense = $_GET['timeend']['year'] . "-" . $_GET['timeend']['month'] . "-" . $_GET['timeend']['day'] . $_GET['timeend']['hour'] . ':' .$_GET['timeend']['minute'];
$tounixdatelicense = strtotime($newdatelicense);

$redirecturl = new moodle_url('/blocks/iomad_company_admin/license_overview.php?userid=' . $userid, ['redirect' => 0]);

$updating = Custom::updating_license_enrolment($tounixdatelicense, $userid, $enrolid);
redirect($redirecturl, "<div class='alert alert-info'>Licencia actualizada correctamente</div>");

echo $OUTPUT->footer();
