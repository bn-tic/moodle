<?php
$string['composetutor'] = '<i class="fa fa-graduation-cap"></i> Mensaje a tutor';
$string['compose'] = '<i class="fa fa-pencil-square-o"></i> Redactar';
$string['courses'] = '<h3 class="sectionname"><span style="color:#0574b9"><i class="fa fa-book"></i> Cursos</span></h3>';
$string['drafts'] = '<i class="fa fa-eraser"></i> Borradores';
$string['inbox'] = '<i class="fa fa-envelope-open-o"></i> Entrada';
$string['labels'] = 'Etiquetas';
$string['pluginname'] = 'Mi correo';
$string['sentmail'] = '<i class="fa fa-paper-plane-o"></i> Enviados';
$string['starred'] = '<i class="fa fa-star-o"></i> Destacados';
$string['trash'] = '<i class="fa fa-trash-o"></i> Papelera';
$string['mymail:addinstance'] = 'Añadir un nuevo bloque Mi correo';
$string['mymail:myaddinstance'] = 'Añadir un nuevo bloque Mi correo a Mi página inicial';
