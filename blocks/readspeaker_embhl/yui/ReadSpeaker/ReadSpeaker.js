YUI.add("moodle-block_readspeaker_embhl-ReadSpeaker", function(){

	M.block_RS = M.block_RS || {};
	M.block_RS.ReadSpeaker = {
		init: function() {
			if (!window.rsConf) window.rsConf = {};

			// Check params
			var dr = (window.rsDocReaderConf && window.rsDocReaderConf.cid) ? '&dload=DocReader.Moodle.AutoAdd' : '';
			var customerid = (window.rsConf && window.rsConf.moodle && window.rsConf.moodle.customerid) ? window.rsConf.moodle.customerid : 'default';

			var scriptSrc = 'https://cdn1.readspeaker.com/script/%customerid%/webReaderForEducation/moodle/v3.3/webReader.js',
				scriptParams = '?pids=embhl' + dr;
			scriptSrc = scriptSrc.replace("%customerid%", customerid);

			window.rsConf.params = scriptSrc + scriptParams;

			var head = document.getElementsByTagName('HEAD').item(0);
			var scriptTag = document.createElement("script");
			scriptTag.setAttribute("type", "text/javascript");
			scriptTag.src = scriptSrc;

			var callback = function() {
				ReadSpeaker.init();
			};

			scriptTag.onreadystatechange = scriptTag.onload = function() {
		        var state = scriptTag.readyState;
		        if (!callback.done && (!state || /loaded|complete/.test(state))) {
		            callback.done = true;
		            callback();
		        }
		    };

		    // use body if available. more safe in IE
		    (document.body || head).appendChild(scriptTag);
		}
	};
}, "@VERSION@", {
	requires: ["node"]
});
